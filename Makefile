SHELL := /bin/bash
PROJECTDIR=/home/roman/projects/pyramid/accordbank
VENV_DIR = /home/roman/.virtualenvs
CURR_ENV_NAME = pyramid34


pysetup:
	# setup package
	python setup.py develop

install: pysetup
	pip install -r requirements.txt

runW:
	pserve development.ini --reload

runG:
	gunicorn --paste development.ini --reload
	
runN:
	$(PROJECTDIR)/gunicorn_start.sh
	
start:
	$(PROJECTDIR)/run.sh
	
runtests:
	python $(PROJECTDIR)/setup.py test --test-suite accordbank.tests
	
runwtests:
	python $(PROJECTDIR)/setup.py test --test-suite accordbank.widgets.tests
	
runtestsH:
	python $(PROJECTDIR)/setup.py test --test-suite accordbank.assets.handlers.tests

killproc:
	killall gunicorn
	
restart: killproc start
