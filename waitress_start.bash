#!/bin/bash
 
NAME="accordbank" # Name of the application
PROJECT_PATH=/home/roman/projects/pyramid/accordbank # Project absolute path
PROJECT_DIR=accordbank

SOCKET_DIR=run
SOCKET_FILE=waitress.sock
SOCKET_PATH=$PROJECT_PATH/$PROJECT_DIR/$SOCKET_DIR/$SOCKET_FILE # we will communicte using this unix socket

VIRTUALENV_PATH=/home/roman/.virtualenvs/pyramid34

USER=roman # the user to run as
GROUP=roman # the group to run as
NUM_WORKERS=5 # how many worker processes should Gunicorn spawn
TIMEOUT=600
# DJANGO_SETTINGS_MODULE=fcbank.settings # which settings file should Django use
# DJANGO_WSGI_MODULE=fcbank.wsgi # WSGI module name
 
echo "Starting $NAME as `whoami`"
 
# Activate the virtual environment
cd $PROJECT_PATH
source $VIRTUALENV_PATH/bin/activate
# export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
# export PYTHONPATH=$PROJECT_PATH:$PYTHONPATH
 
# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKET_PATH)
test -d $RUNDIR || mkdir -p $RUNDIR
 
# Start your Waitress
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec $VIRTUALENV_PATH/bin/pserve development.ini \
--reload
