/*
 * Обробка подій у "Топ продуктів"
 * */
$(function(){
    var items = $('.top-products-item');
    var items_link = $('.top-products-item a');
    var activeBackGrColor = '#e5e5e5';
    
    
    function setItemBackground(item){
        item.addClass('top-products-item-active');
        };
        
    function unsetItemBackground(item){
        item.removeClass('top-products-item-active');
        };
        
    items_link.mouseover(function(){ setItemBackground($(this).parent())});
    items_link.mouseleave(function(){ unsetItemBackground($(this).parent())});
});
