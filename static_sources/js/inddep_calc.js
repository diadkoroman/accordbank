$(function(){

    var option = 'inddepcalc';
    
    //
    function sendAjax(){
        $.ajax({
            type: 'GET',
            url: '/ajax.json/',
            data: {
                'option': option,
                'depid': $('input[name=depid]').val(),
                'amount': $('input[name=amount]').val(),
                'terms': $('select[name=terms] option:selected').val(),
                'iptypes': $('select[name=iptypes] option:selected').val(),
                'currencies': $('select[name=currencies] option:selected').val(),
            }, 
            success: function(data){
                var data = $.parseJSON(data);
                $('.calc-res-block').hide();
                $('.errors-wr').hide();
                $('.errors-wr .error').hide();
                if(data['err']){
                    for(prop in data['err']){
                        $('#' + prop).show();
                        for(a=0;a<data['err'][prop].length;a++){
                            $('.' + data['err'][prop][a]).show();
                        };
                    };
                }else if(data['profit']){
                    for(prop in data){
                        $('*[ident=' + prop + ']').html(data[prop]);
                    };
                    $('.calc-res-block').show();
                };
                //alert(data);
            }
        });
    };
    
    //==========
    $('#calc-button').click(function(){
        sendAjax();
    });
    //sendAjax();
});
