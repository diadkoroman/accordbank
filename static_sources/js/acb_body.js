$(function(){
    /*
        Makes correction of cotent block height.
    */
    var contentblock = $('.content-block');
    var sidenav = $('#sidenav');
    var screenHeight = null;
    var contentHeight = null;
    var sidenavHeight = null;
    
    function setHeights(){
        // setting element heights 
        screenHeight = screen.availHeight;
        contentHeight = contentblock.innerHeight();
        sidenavHeight = sidenav.innerHeight();
    };
    
    function setContentBlockHeight(){
        // set contentblock height
        if(contentHeight < sidenavHeight){
            contentblock.innerHeight(sidenavHeight);
        };
    };
    
    
    // init section
    function init(){
        setHeights();
        setContentBlockHeight();
        //alert(screenHeight);
        //alert(contentHeight);
        //alert(sidenavHeight);
    };
    
    init();
});