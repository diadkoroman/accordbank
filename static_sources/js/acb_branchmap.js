var center_point = {'lat': 50.447193, 'lng': 30.521670}
var type_switch = $('input[name=branchtype_id]');
var prop_switch = $('input[name=branchprop_mnemo]');
var city_switch = $('#city-switch');
var mapwrapper = $('#map-wr');
var mcOptions = {imagePath: '/images/m'};
var optionPanel = $('#branchview-manager-wr');
var avH = screen.availHeight;

// список точок за умовчанням
function setDefaultBranchList(){
    return branches_raw;
    };
    
// відфільтрувати точки за типом
function filterMapBranchType(branches, branchtype_id){
    for(b=0;b<branches.length;b++){
        if(branches[b] != ''){
            if(branchtype_id ==2){
                //костиль для відображення відділень з банкоматами
                if(branches[b].info.branchtype_id != parseInt(branchtype_id) && branches[b].info.properties.indexOf('has_atm') == -1){
                    branches[b] = '';
                };
            }else{
                if(branches[b].info.branchtype_id != parseInt(branchtype_id)){
                    branches[b] = '';
                };
            };
        };
    };
    return branches;
};
    
// відфільтрувати точки за додатковими властивостями
function filterMapBranchProperty(branches, property_mnemo){
    for(c=0;c<branches.length;c++){
        if(branches[c] != ''){
            if(branches[c].info.properties.indexOf(property_mnemo) == -1){
                branches[c] = '';
                };
            };
        };
    return branches;
    };
    
// відфільтрувати точки за містом
function filterMapCity(branches, city_id){
    var city = parseInt(city_id);
    if(city != 0){
        for(d=0;d<branches.length;d++){
            if(typeof(branches[d]) != undefined){
                if(branches[d].info.city_id != city_id){
                    branches[d] = '';
                };
            };
        };
    };
    return branches;
};

// додавання інфовікна
function addInfoWindow(m, branch){
    var winfo = '<div class="row">';
    winfo += '<div class="column small-12"><strong>'+ branch.title +'</strong></div>';
    winfo += '<hr/>';
    winfo += '<div class="column small-12">'+ branch.info.address +'<br/>';
    winfo += branch.info.worktime + '<br/>'
    winfo += branch.info.telns +'</div>';
    winfo += '</div>';
    var infowindow = new google.maps.InfoWindow({
        content: winfo,
        maxWidth: 450,
        });
    new google.maps.event.addListener(m, 'click', function() {infowindow.open(m.get('map'),m);});
    };
    
// приховування керуючої панелі залежно від позиції скролу
function manageOptionPanel(){
    /*
     * Панель ховається тільки якщо активна вкладка "Мапа"*/
    $(window).scroll(null, function(){
        if($('#map-wr').is(':visible')){
            var top = $(document).scrollTop();
            if(top >= avH*0.3){
                optionPanel.fadeOut(500);
                }else{optionPanel.fadeIn(500);};
            
        };
    });
};

//ініціалізатор мапи
function initMap() {
    // створюємо порожній масив
    var branches = [];
    // заповнюємо його даними
    for(a=0;a<branches_raw.length;a++){
        branches.push(branches_raw[a]);
        };
        
    // Якщо натиснуто селектор "Місто"...
    if($('#city-switch').find('option:selected').length > 0){
        branches = filterMapCity(branches, $('#city-switch').find('option:selected').val());
    };
    

    // якщо натиснуто селектор "Тип точки" - фільтруємо
    if($('input[name=branchtype_id]:checked').length > 0){
        branches = filterMapBranchType(branches, $('input[name=branchtype_id]:checked').val());
        };

    // якщо вибрано один або більше селекторів з додатковими фільтрами...
    if($('input[name=branchprop_mnemo]:checked').length > 0){
        $('input[name=branchprop_mnemo]:checked').each(function(){
            branches = filterMapBranchProperty(branches, $(this).val());
            });
        };

    //створюємо мапу
    var map = new google.maps.Map(document.getElementById('map-wr'), {
        center: center_point,
        zoom: 10,
        maxZoom:15,
        styles: [{
        featureType: 'poi',
        stylers: [{ visibility: 'off' }]  // Turn off points of interest.
        }, 
        {
        featureType: 'transit.station',
        stylers: [{ visibility: 'off' }]  // Turn off bus stations, train stations, etc.
        }],
        disableDoubleClickZoom: true,
        scrollwheel: false,
        });
    
    // створюємо точки
    var pointBounds = new google.maps.LatLngBounds();
    // фільтруємо непорожні
    var not_empties = [];
    for(e=0;e<branches.length;e++){
        if(branches[e] != ''){ not_empties.push(branches[e]) };
    };
    // ініціалізуємо карту
    if(not_empties.length > 0){
        var markers = [];
        for(i=0;i<not_empties.length;i++){
            if(not_empties[i] != ''){
                var pointPos = new google.maps.LatLng(not_empties[i].coords.lat,not_empties[i].coords.lng);
                pointBounds.extend(pointPos);
                var marker = new google.maps.Marker({
                    position: pointPos,
                    map: map,
                    title: not_empties[i].title
                    });
                addInfoWindow(marker, not_empties[i]);
                markers.push(marker);
            };
        };
        map.setCenter(pointBounds.getCenter(), map.fitBounds(pointBounds));
        var mapClusterer = new MarkerClusterer(map, markers, mcOptions);
    }else{
        map.panTo(center_point)
        };
    //map.setCenter(pointBounds.getCenter(), map.fitBounds(pointBounds));
};


// реакція на кліки селекторів
//-----------------------------
/*
 * Задум у тому, що ці функції спрацьовують лише у випадку, 
 * коли контейнер із мапою видимий(активна вкладка)*/
 
     // перемикання за типом
    city_switch.change(function(){
        if($('#map-wr').is(':visible')){
            initMap();
            };
        });

    // перемикання за типом
    type_switch.change(function(){
        if($('#map-wr').is(':visible')){
            initMap();
            };
        });
    // перемикання за додатковими властивостями
    prop_switch.change(function(){
        if($('#map-wr').is(':visible')){
            initMap();
            };
        });

manageOptionPanel();
