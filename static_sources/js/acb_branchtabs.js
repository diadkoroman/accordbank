$(function(){
    var tab = $('.branches-tab');
    var active_id = 'map-wr';
    var mapwr_id = '#map-wr';
    var container = $('#branches-wr');
    var view_switch = $('.view-switch');
    var type_switch = $('input[name=branchtype_id]');
    var prop_switch = $('input[name=branchprop_mnemo]');
    var branchinfo = $('.branchinfo');
    var city_switch = $('#city-switch');
    
    /*
     * Показати активний таб 
     * */
    function showActiveTab(){
        tab.hide();
        $('#'+active_id).fadeIn();
        };
     
    /*
     * Змінити активний таб
     * */
    function toggleActiveTab(active_id){
        tab.hide();
        $('#'+active_id).fadeIn();
        };
        
    /*
     * Коригування висоти контейнера
     * */
    function setContainerHeight(){
        var h = screen.availHeight;
        var ch = container.innerHeight();
        container.css({'min-height':h});
        $(mapwr_id).innerHeight(h);
        /*if(ch < h){
            $(mapwr_id).innerHeight(h);
            };*/
        
        };
        
    /*
     * Фільтр виводу "Відділення/Банкомат(branchtype)"*/
    function filterBranchType(active_id){
        branchinfo.hide();
        branchinfo.each(function(){
            if(active_id == 2){
                if($(this).find('input[name=branchtype_id]').val() != active_id && $(this).find('input[name=has_atm]').length != 0){
                    $(this).show();
                };
            }else{
                if($(this).find('input[name=branchtype_id]').val() == active_id){
                    $(this).show();
                };
            };
        });
    };
        
    /*
     * Фільтр виводу точок за особливими ознаками(branchprop)
     * */
    function filterBranchProp(active_mnemo){
        branchinfo.each(function(){
            if($(this).find('input[name='+ active_mnemo +']').length == 0){
                $(this).hide();
                };
            });
        };
        
    /*
     * Фільтр виводу точок для певного міста
     * */
    function filterCity(active_id){
        branchinfo.hide();
        branchinfo.each(function(){
            if($(this).find('input[name=city_id]').val() == active_id){
                $(this).show();
                };
            });
        if(active_id == 0){branchinfo.show()};
        };

    // ініт
    showActiveTab();
    setContainerHeight();
    //клік на перемикачі типу способу відображення
    view_switch.click(function(){
        toggleActiveTab($(this).attr('ident'));
        });
    // клік на чекбоксах з типом точок
    type_switch.change(function(){
        if($('input[name=branchtype_id]:checked').length > 0){
            filterBranchType($('input[name=branchtype_id]:checked').val());
            }else{
                branchinfo.fadeIn();
                };
        });
    // клік на чекбоксах з фільтрами
    prop_switch.change(function(){
        // в циклі, тому що треба опрацювати всі властивості по черзі
        if($('input[name=branchprop_mnemo]:checked').length > 0){
            $('input[name=branchprop_mnemo]:checked').each(function(){
                filterBranchProp($(this).val());
                });
            }else{
                if($('input[name=branchtype_id]:checked').length > 0){
                    filterBranchType($('input[name=branchtype_id]:checked').val());
                    }else{
                        branchinfo.fadeIn();
                        };
                };
        });
    // зміни в селекторі міст
    city_switch.change(function(){
        var selected = $(this).find('option:selected');
        var selected_val = selected.val();
        if(selected.length > 0){
            filterCity(selected_val);
            };
        });
    });
