# -*- coding: utf-8 -*-
from pyramid_cms import admin
from pyramid_cms.assets.widgets import ProjectWidget


class ACB_NotFoundMessage(ProjectWidget):
    """
    Віджет з повідомленням по те, що сторінку не знайдено.
    """
    default_temp_dir_index = 1
    wdata_body = ('statuscode', 'message', 'message2')
    

    def get_message(self):
        self.wdata['body']['statuscode'] = '404'
        self.wdata['body']['message'] = \
            self.kw['slots']['strings']['404message']\
            .localized(self.req.curr_lang.id)
        self.wdata['body']['message2'] = \
            self.kw['slots']['strings']['404message2']\
            .localized(self.req.curr_lang.id)

    def dispatch(self):
        self.get_message()
admin.admin.widgets.append(ACB_NotFoundMessage)
