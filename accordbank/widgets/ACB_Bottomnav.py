# -*- coding: utf-8 -*-
from sqlalchemy.sql.expression import asc,desc
from pyramid_beaker import cache

from pyramid_cms import admin
from pyramid_cms.assets.widgets import ProjectWidget
from pyramid_cms.assets.db.sqlalchemy.connection import Q
from pyramid_cms.apps.navs import dbmodels as navsdb

class ACB_Bottomnav(ProjectWidget):
    """
    Top navigator  widget for project side
    """
    default_temp_dir_index = 1
    items = navsdb.NavItem
    slots = navsdb.NavSlot
    
    """ Поки що прикриваємо кеш, до з’ясування проблем з ним на
    pythonanywhere """
    #s@cache.cache_region('long_term')
    def get_items(self):
        itemslist = []
        items = Q(self.items)\
            .join(self.slots)\
            .filter(
                self.slots.mnemo == self.mnemo,
                self.items.active ==True)\
            .order_by(asc(self.items.rank))
        def _process_item(item):
            # обробка кожного елемента меню спеціальною функцією
            i = \
                {
                    'title': item.localized(self.req.curr_lang.id),
                    'path': '/{0}{1}'.format(
                        self.req.curr_lang.mnemo,
                        item.node.get_path()
                        if item.node
                        else item.link)
                }
            return i
        # повертаємо дані для меню у вигляді опрацьованого списку з даними
        return list(map(_process_item, items))

    def dispatch(self):
        """
        Dispatcher method to retrieve data for top navigator.
        """
        # get instances for current widget
        self.wdata['body']['itemslist'] = self.get_items()
admin.admin.widgets.append(ACB_Bottomnav)
