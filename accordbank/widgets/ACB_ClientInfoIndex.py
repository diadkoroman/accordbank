# -*- coding: utf-8 -*-
from pyramid_cms import admin
from pyramid_cms.assets.widgets import ProjectWidget


class ACB_ClientInfoIndex(ProjectWidget):
    """
    Віджет, що відображає блок "Інформація для клієнтів"
    на першій сторінці сайту
    """
    default_temp_dir_index = 1

admin.admin.widgets.append(ACB_ClientInfoIndex)
