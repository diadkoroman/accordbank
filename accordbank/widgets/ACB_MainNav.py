# -*- coding: utf-8 -*-
from sqlalchemy.sql.expression import asc,desc
from pyramid_beaker import cache

from pyramid_cms import admin
from pyramid_cms.assets.widgets import ProjectWidget
from pyramid_cms.assets.db.sqlalchemy.connection import Q
from pyramid_cms.apps.navs import dbmodels as navsdb

class ACB_MainNav(ProjectWidget):
    """
    Site mainmenu
    """
    default_temp_dir_index = 1
    items = navsdb.NavItem
    slots = navsdb.NavSlot
    
    def get_tree(self, item):
        i = {'mnemo': '', 'title':'', 'path': '', 'children': []}
        i['mnemo'] = item.mnemo
        i['title'] = item.localized(self.req.curr_lang.id)
        i['path'] = '/{0}{1}'.format(
            self.req.curr_lang.mnemo,
            item.node.get_path()
            if item.node
            else item.link)
        i['children'] = list(map(self.get_tree,item.children.filter_by(active=True)))
        return i
        
    """
    @cache.cache_region('long_term')
    def get_items(self):
        
        return Q(self.items)\
            .join(self.slots)\
            .filter(
                self.slots.mnemo == self.mnemo,
                self.items.parent_id == None,
                self.items.active ==True)\
            .order_by(asc(self.items.rank))
    """

    def dispatch(self):
        """
        Dispatcher method to retrieve data for main navigator.
        """
        # get instances for current widget
        items = Q(self.items)\
            .join(self.slots)\
            .filter(
                self.slots.mnemo == self.mnemo,
                self.items.parent_id == None,
                self.items.active ==True)\
            .order_by(asc(self.items.rank))
        # instances = self.get_items()
        # self.wdata['body'] = instances
        self.wdata['body']['itemslist'] = list(map(self.get_tree, items))
admin.admin.widgets.append(ACB_MainNav)
