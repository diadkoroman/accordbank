# -*- coding: utf-8 -*-
import transaction
from sqlalchemy.sql.expression import asc
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.widgets import ProjectWidget
from pyramid_cms.apps.roots import dbmodels as rootsdb


class ACB_CrossSales(ProjectWidget):
    """
    Віджет, що забезпечує вивід пов’язаних тегами розділів
    у текстову частину розділу
    """
    default_temp_dir_index = 1

    wdata_body = ['products','nodes']

    """
    Перелік продуктів для крос-продажу.
    Метод, що опрацьовує продукти, бере значення з цього списку
    і намагається отримати одноіменний атрибут із класу TreeNode.
    Напр., для "deposit" метод намагатиметься отримати атрибут
    TreeNode.deposit.
    """
    cross_sales = ['deposit', 'cardproduct']
    
    related_products = []
    related_nodes = []
    
    def reset_widget_attrs(self):
        """ Встановлюємо/перезаписуємо додаткові атрибути віджета """
        self.included_ids = []
        self.related_nodes = []
        self.related_products = []
        
    def reset_stack_attrs(self):
        """ Встановлюємо/перезаписуємо додаткові атрибути стека """
        self.wdata['body']['nodes'] = []
        self.wdata['body']['products'] = []

    def dispatch(self):
        """ Диспетчер даних для віджета """
        self.get_node_tags()
        self.get_related_nodes()
        self.set_related_products()
        self.set_related_nodes()
        self.wdata['body']['test'] = self.included_ids

    def get_node_tags(self):
        """
        Отримуємо теги поточної ноди
        """
        self.node_tags = \
            [tag.id for tag in self.kw['node'].tags]

    def get_related_nodes(self):
        """
        Отримуємо всі ноди, що мають хоча б один тег із поточної ноди
        """
        try:
            self.related_nodes = Q(rootsdb.TreeNode)\
                .filter(rootsdb.TreeNode.tags
                        .any(rootsdb.TreeNodeTag.id.in_(self.node_tags)))\
                .filter(rootsdb.TreeNode.id != self.kw['node'].id)\
                .filter(rootsdb.TreeNode.active == True)\
                .order_by(asc(rootsdb.TreeNode.rank))\
                .all()
        except:
            pass
        

    def set_related_products(self):
        """
        Метод, що намагається отримати з нод продукти для крос-продажу
        як атрибути класу TreeNode
        Для цього метод обходить у циклі перелік нод і для кожної ноди
        обходить в циклі перелік продуктів з self.cross_sales
        Отримане заряджається у стек для виводу
        """
        for node in self.related_nodes:
            for product in self.cross_sales:
                try:
                    p = getattr(node, product)
                    if p is not None:
                        self.related_products\
                                .append(p)
                        self.included_ids.append(node.id)
                except:
                    pass
        self.related_products = sorted(set(self.related_products), key=lambda i: i.rank)
        self.wdata['body']['products'] = self.related_products
        
    
    def set_related_nodes(self):
        """
        Метод, що отримує пов’язані ноди - непродуктові
        """
        
        for node in self.related_nodes:
            if node.id not in self.included_ids:
                self.wdata['body']['nodes'].append(node)

admin.admin.widgets.append(ACB_CrossSales)
