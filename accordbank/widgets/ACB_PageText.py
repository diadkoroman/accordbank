# -*- coding: utf-8 -*-
import re
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.widgets import ProjectWidget
from pyramid_cms.apps.roots import dbmodels as rootsdb

from accordbank.assets import widgets

class ACB_PageText(widgets.SearchEnabledWidget):
    """
    Текстовий блок для простої текстової сторінки
    """
    default_temp_dir_index = 1
    wdata_body = ('header','text','test')
    
    def reset_stack_attrs(self):
        self.wdata['body']['header'] = None
        self.wdata['body']['text'] = None
        self.wdata['body']['test'] = None

    def dispatch(self):
        """ Отримуємо дані для виводу у віджет """

        node = self.kw['node']
        self.clean_user_input()
        try:
            #header = node.titles\
            #.filter_by(lang_id=self.req.curr_lang.id).first().content
            text = node.texts\
            .filter_by(lang_id=self.req.curr_lang.id).first().content
            self.wdata['body']['header'] = self.data['pagetitle']
            self.wdata['body']['text'] = self.mark_search_res(self._cleaned_user_input, text)
            self.wdata['body']['test'] = self._cleaned_user_input
        except:
            pass
admin.admin.widgets.append(ACB_PageText)
