# -*- coding: utf-8 -*-
from sqlalchemy.sql.expression import asc,desc
from pyramid_beaker import cache

from pyramid_cms import admin
from pyramid_cms.assets.widgets import ProjectWidget
from pyramid_cms.assets.db.sqlalchemy.connection import Q

from accordbank.apps.deposits import dbmodels as deposdb

class ACB_TopProducts(ProjectWidget):
    """ Топ банківських продуктів - презентація на першій сторінці"""
    default_temp_dir_index = 1
    
    deposits = deposdb.Deposit
    
    wdata_body = ['deposits', 'cards']
    
    def dispatch(self):
        """
        Отримати дані для віджета
        У цей віджет теоретично зможуть потрапляти найрізноманітніші продукти,
        тому в ньому ми не маємо єдиного атрибута  items як у інших віджетах,
        а оперуємо специфічними тільки для цього віджета атрибутами
        """
        self.wdata['body']['deposits'] = []
        
        # отримати депозити
        # self._getproducts_deposits()
        self.products_init()

    def products_init(self):
        """
        Ініціалізатор для запуску різних продуктів.
        Проходиться по списку self.wdata_body, і запускає усі методи в’юхи,
        що відповідають шаблону ('_getproducts_' + поточний елемент)
        Метод, що запускається, приймає аргументом назву атрибута.
        """
        for attrname in self.wdata_body:
            try:
                getattr(self, '_getproducts_' + attrname)(attrname)
                r += 1
            except:
                pass

    @cache.cache_region('long_term')
    def _getitems_deposits(self):
        return Q(self.deposits)\
            .filter(self.deposits.in_promo == True)\
            .filter(self.deposits.active == True)\
            .order_by(asc(self.deposits.rank))

    def _getproducts_deposits(self, attrname):
        """
        Отримати депозити, позначені для виводу у топ продуктів.
        Назва атрибута, що приймається аргументом, є основою для запуску
        спеціального метода, який здійснює спеціальний запит до БД
        і отримує перелік тих чи інших продуктів.
        Наприклад, для продукту "Депозити" назвою атрибута є 'deposits',
        викликається метод "_getitems_deposits", котрий повертає
        перелік депозитів.
        """
        #try:
        deposits = getattr(self, '_getitems_' + attrname)()
        self.wdata['body'][attrname] = \
            {
                'title':\
                self.kw['slots']['strings']['our_deposits']\
                .localized(self.req.curr_lang.id),
                'itemslist': [d for d in deposits]
            }
            #self.wdata['body'][attrname].extend([d for d in deposits])
        #except:
        #    pass

admin.admin.widgets.append(ACB_TopProducts)
