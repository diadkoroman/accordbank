# -*- coding: utf-8 -*-
from sqlalchemy.sql.expression import asc,desc
from pyramid_beaker import cache

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.widgets import ProjectWidget
from pyramid_cms.apps.roots import dbmodels as rootsdb

class ACB_NewsIndex(ProjectWidget):
    """
    Віджет, що відображає заголовки новин на першій сторінці сайту
    """
    default_temp_dir_index = 1
    # Кількість заголовків у віджеті
    headers_num = 2
    # Припустима довжина заголовка
    header_length = 100
    # Припустима довжина анотації
    annot_length = 200
    # Елементи в даному випадку - ноди дерева сайту
    items = rootsdb.TreeNode
    pagetypes = rootsdb.PageType
    root = None
    
    #@cache.cache_region('short_term')
    def get_widget_root(self):
        return Q(self.items)\
            .join(self.pagetypes)\
            .filter(self.pagetypes.mnemo == self.mnemo)\
            .first()
    
    
    def widget_root(self):
        """
        Отримуємо кореневий розділ новин.
        """
        self.root = self.get_widget_root()
        self.wdata['body']['header'] = self.root.title_localized(lang_id=self.req.curr_lang.id)
            
        self.widget_root_url()
            
    def widget_root_url(self):
        """ Метод формує посилання на батьківський розділ новин """
        if self.root:
            self.wdata['body']['newsindex_url'] = self.root.get_path()

    def get_headers_list(self):
        """
        Get news headers list.
        """
        self.wdata['body']['newsitems'] = []
        for i in self.root.children\
                    .filter_by(active=True)\
                    .order_by(desc(self.items.date_visible))[:self.headers_num]:
            nh = []
            # adding date
            nh.append(i.date_visible.strftime('%d.%m.%Y'))
            
            """
            The little but clever:) method to add header.
            Only if header length is bigger than parameter set,
            it divides header by words and format it with addition
            of '...'
            """
            header = i.title_localized(lang_id=self.req.curr_lang.id)
            if len(header) > self.header_length:
                header_mod = ''
                words = list(header.split(' '))
                c = 0
                while len(header_mod) < self.header_length:
                    header_mod += words[c] + ' '
                    c += 1
                header_mod += '...'
                header = header_mod
            nh.append(header)
            
            # add path to news item
            nh.append(i.get_path())
            
            # the same clever stuff to add annotations
            annot = i.descriptions\
                .filter_by(lang_id=self.req.curr_lang.id)\
                .first().content
            if len(annot) > self.annot_length:
                annot_mod = ''
                words = annot.split(' ')
                c = 0
                while len(annot_mod) < self.annot_length:
                    annot_mod += words[c] + ' '
                    c += 1
                annot_mod += '...'
                annot = annot_mod
            nh.append(annot)
            self.wdata['body']['newsitems'].append(nh)
            
        

    def dispatch(self):
        self.wdata['body']['header'] = None
        self.wdata['body']['newsitems'] = None
        self.wdata['body']['newsindex_url'] = None
        """
        Отримання заголовків новин для виводу у віджет на першій сторінці
        """
        # Спочатку отримуємо кореневий розділ
        self.widget_root()
        # Потім отримуємо список заголовків новин. Кількість виставляємо
        # у змінній класу headers_num
        self.get_headers_list()
        """
        self.wdata['body']['newsitems'] = [
            (i.created.strftime('%d.%m.%Y'), i.titles\
            .filter_by(lang_id=self.req.curr_lang.id)\
            .first().content, i.get_path()) \
            for i in self.root.children\
        .filter_by(active=True)\
        .order_by(desc(self.items.created))[:5]]
        """

admin.admin.widgets.append(ACB_NewsIndex)
