# -*- coding: utf-8 -*-
import re
from sqlalchemy.sql.expression import asc, desc
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.widgets import ProjectWidget
from pyramid_cms.apps.roots import dbmodels as rootsdb

class ACB_SearchText(ProjectWidget):

    """
    Текстовий блок для сторінки з результатами пошуку.
    """
    default_temp_dir_index = 1
    
    wdata_body = ('header', 'text', 'userinput', 'searchitems')

    # назва змінної GET користувацького вводу
    user_input_var = "trg"
    _raw_user_input = None
    _cleaned_user_input = None
    _forbidden_input='[><:;`~@#$%\^\+\=\/\*\.,\]\[}{]+'
    
    # мінімальна довжина пошукового терміну
    _min_clean_length = 2
    # результати пошуку
    search_res = []

    def clean_user_input(self):
        """
        Очищення користувацького вводу.
        _raw_user_input - НЕОБРОБЛЕНІ(!) дані
        _cleaned_user_input - дані після обробки
        
        1. якщо знаходимо змінну вводу - отримуємо її значення із GET
        і вносимо до _raw_user_input.
        
        2. якщо _raw_user_input має значення не-None, то здійснюємо обробку
        і зневадження даних.
        """

        self._raw_user_input = None
        self._cleaned_user_input = None

        # 1.
        if self.user_input_var in self.req.GET:
            self._raw_user_input = self.req.GET.get(self.user_input_var)

        # 2.
        if self._raw_user_input:
            # очищаємо від спецсимволів
            clean = \
            re.sub(self._forbidden_input, '', self._raw_user_input, re.I)
            # розбиваємо на слова
            clean = re.split("\s+", clean, re.I)
            self._cleaned_user_input = \
            [i for i in clean if len(i) > self._min_clean_length]
            
    def process_items(self):
        """
        Метод-обгортка для опрацювання даних пошуку.
        Включає в себе кілька методів пошуку:
        1. пошук у заголовках розділів
        2. пошук у анотаціях
        3. пошук у текстах
        4. пошук у тегах
        """
        if self._cleaned_user_input:
            items = \
            Q(rootsdb.TreeNode)\
            .join(rootsdb.PageTitle)\
            .join(rootsdb.PageDescription)\
            .join(rootsdb.PageText)\
                .filter(rootsdb.TreeNode.active == True)
            self.get_search_res(items=items)
            self.search_res = sorted(set(self.search_res), key=lambda i: i.inner_name)
        
    def get_search_res(self, items=None):
        """ Отримання результатів пошуку """
        if items:
            for search_term in self._cleaned_user_input:
                st = '%{0}%'.format(search_term)
                st1 = '{0}%'.format(search_term)
                st2 = '%{0}'.format(search_term)
                self.search_res\
                .extend(items\
                    .filter((rootsdb.PageTitle.content\
                                .ilike(st)) | \
                            (rootsdb.PageDescription.content\
                                .ilike(st)) | \
                            (rootsdb.PageText.content\
                                .ilike(st)) | \
                            (rootsdb.PageTitle.content\
                                .ilike(st1)) | \
                            (rootsdb.PageDescription.content\
                                .ilike(st1)) | \
                            (rootsdb.PageText.content\
                                .ilike(st1)) | \
                            (rootsdb.PageTitle.content\
                                .ilike(st2)) | \
                            (rootsdb.PageDescription.content\
                                .ilike(st2)) | \
                            (rootsdb.PageText.content\
                                .ilike(st2))
                        ))

    def dispatch(self):
        """ Формуємо дані для виводу у віджет """
        self.search_res = []
        self.wdata['body']['header'] = None
        self.wdata['body']['text'] = None
        node = self.kw['node']
        try:
            self.wdata['body']['header'] = node.titles\
            .filter_by(lang_id=self.req.curr_lang.id).first().content
        except:
            pass

    def get(self):
        """
        Перелік дій при отриманні віджетом запиту GET
        """
        self.clean_user_input()
        self.process_items()
        if self._cleaned_user_input:
            self.wdata['body']['userinput'] = '+'.join(self._cleaned_user_input)
        self.wdata['body']['searchitems'] = self.search_res
        

admin.admin.widgets.append(ACB_SearchText)
