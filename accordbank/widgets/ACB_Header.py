# -*- coding: utf-8 -*-
from pyramid_cms import admin
from pyramid_cms.assets.widgets import ProjectWidget


class ACB_Header(ProjectWidget):
    """
    Header  widget for project side
    """
    default_temp_dir_index = 1
admin.admin.widgets.append(ACB_Header)
