# -*- coding: utf-8 -*-
from sqlalchemy.sql import func
from sqlalchemy.sql import expression

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.widgets import ProjectWidget

from accordbank.apps.currencies import dbmodels as currsdb


class ACB_CurrenciesInner(ProjectWidget):
    """
    Віджет для відображення курсів валют на першій сторінці сайту
    """
    default_temp_dir_index = 1

    # дефолтний клас (валюти)
    _default_class = currsdb.Currency
    # клас категорій
    _categs_class = currsdb.CurrencyRateCateg
    # клас ставок
    _rates_class = currsdb.CurrencyRate
    currencies = None

    def get_header(self):
        """ Отримати заголовок віджета """
        self.wdata['body']['header'] = None
        try:
            self.wdata['body']['header'] = \
                self.kw['node'].title_localized(self.req.curr_lang.id)
        except:
            pass

    def get_items(self):
        """ Отримуємо перелік валют, що мають відображатися в цьому віджеті """
        self.wdata['body']['itemslist'] = {}
        try:
            currencies = Q(self._default_class)\
                .filter(
                    self._default_class.mncode != 'UAH',
                    self._default_class.active == True)\
                .order_by(expression.asc(self._default_class.rank))
            for categ in self.categs:
                self.wdata['body']['itemslist'][categ.mnemo] = []
                for currency in currencies:
                    c = {
                        'currency': currency,
                        'rates': currency.get_lastrate_by_categ(categ)
                        }
                    self.wdata['body']['itemslist'][categ.mnemo].append(c)
        except:
            pass

    def get_lastupdate_data(self):
        try:
            return Q(func.max(self._rates_class.updated))\
                .scalar().strftime('%d.%m.%Y')
        except:
            return None

    def get_categs(self):
        """ Отримуємо категорії для розгортання вкладок у віджеті """
        self.categs = \
            Q(self._categs_class)\
            .filter(
                self._categs_class.in_widget == True,
                self._categs_class.active == True
                )\
            .order_by(expression.asc(self._categs_class.rank))

    def dispatch(self):
        """ Отримуємо дані для віджета """
        self.get_header()
        self.get_categs()
        self.get_items()
        self.wdata['body']['categs'] = self.categs
        # self.wdata['body']['itemslist'] = self.currencies
        self.wdata['body']['lastupdate'] = self.get_lastupdate_data()

admin.admin.widgets.append(ACB_CurrenciesInner)
