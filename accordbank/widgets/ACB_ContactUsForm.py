# -*- coding: utf-8 -*-
import re
import transaction
import smtplib
from pyramid.httpexceptions import HTTPFound
from pyramid_mailer import get_mailer
from pyramid_mailer.message import Message
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.widgets import ProjectWidget
from accordbank.apps.messages import dbmodels as mesgdb
from accordbank.forms import MessagesForm


class ACB_ContactUsForm(ProjectWidget):
    """
    Блок з формою для надсилання листа
    """
    default_temp_dir_index = 1

    def dispatch(self):
        """
        Формування виводу для віджета
        """
        self.wdata['body']['header'] = None
        self.wdata['body']['form'] = None
        self.wdata['body']['form_valid'] = None
        self.wdata['meta']['switch'] = None
        node = self.kw['node']
        self.path = '/{0}{1}'.format(self.req.curr_lang.mnemo, node.get_path())
        try:
            self.wdata['body']['header'] = node.titles\
                .filter_by(lang_id=self.req.curr_lang.id).first().content
        except:
            pass
        self.message_topics = \
            Q(mesgdb.MessageTopic)\
            .filter(mesgdb.MessageTopic.active == True)
            
        """
        Тут ми отримуємо змінну з GET, котра дає вказівку шаблону, який саме
        під-шаблон йому відображати
        """
        self.wdata['meta']['switch'] = self._get_switch_variable()

    def get(self):
        form = MessagesForm(meta={'csrf_context': self.req.session})
        form.topics.choices = \
            [
                (str(i.id), i.localized(self.req.curr_lang.id))
                for i in self.message_topics
            ]
        self.wdata['body']['form'] = form

    def post(self):
        form = MessagesForm(
            self.req.POST, meta={'csrf_context': self.req.session})
        form.topics.choices = \
            [
                (str(i.id), i.localized(self.req.curr_lang.id))
                for i in self.message_topics
            ]
        if form.validate_and_add():
            self.wdata['body']['form_valid'] = 'VALID'
            fdata = \
                {
                    'topic_id': form.topics.data,
                    'body': form.content.data,
                    'sender_name': form.sender_name.data,
                    'sender_email': form.sender_email.data,
                    'sender_teln': str(form.sender_teln.data) if form.sender_teln.data else ''
                }
            """
            Якщо форма провалідувалася, то відпраьовує метод для відправки
            пошти. Залежно від результату, поверненого методом, URL 
            модифікується відповідною змінною.
            Від змінної залежить, який саме під-шаблон відобразиться на 
            сторінці.
            """
            if self.send_email(fdata=fdata):
                self.path += '?success'
            else:
                self.path += '?fail'
            raise HTTPFound(location=self.path)
        else:
            self.wdata['body']['form_valid'] = 'NOT VALID'
        self.wdata['body']['form'] = form

    def send_email_old(self, fdata={}):
        """
        Надсилаємо повідомлення електронною поштою
        fdata - дані з форми, необхідні для прийняття рішення про надсилання
        поштового повідомлення.
        """
        if fdata:
            try:
                # створюємо об’єкт мейлера
                mailer = get_mailer(self.req)
                # якщо є дані для прийняття рішення - аналізуємо їх.
                concerned_emails = \
                    self._get_concerned_emails(fdata.get('topic_id', 0))
                message_data = \
                    {
                        'subject': 'test',
                        'sender': 'noreply@localhost',
                        'recipients': concerned_emails,
                        'body': 'asdasdasd'
                    }
                message = Message(**message_data)
                mailer.send(message)
                return True
            except Exception as e:
                raise e
            # transaction.commit()

    def send_email(self, fdata={}):
        if fdata:
            try:
                """
                Дані про відправника збираємо в рядок, щоб потім додати до теми листа
                """
                sender_info = \
                              '{0}, e-mail: {1}, тел.: {2}'.format(
                                  fdata.get('sender_name', 'Anonimous'),
                                  fdata.get('sender_email', ''),
                                  fdata.get('sender_teln', '')
                              )
                topic_data = \
                    self._get_topic_data(fdata.get('topic_id', 0))
                concerned_emails = topic_data.related_emails.split(',')
                #
                mailer = smtplib.SMTP(
                    admin.project.config.smtpserver,
                    admin.project.config.smtpport)
                mailer.ehlo()
                mailer.starttls()
                mailer.ehlo()

                headers = \
                    "From: {0}\r\nTo: {1}\r\nSubject: {2} {3}\r\n"\
                    .format(admin.project.config.mailheader, topic_data.related_emails, sender_info, topic_data)
                message = headers.encode('UTF-8') + fdata['body'].encode('UTF-8')
                mailer.login(admin.project.config.smtpuser, admin.project.config.smtppassw)
                mailer.sendmail(admin.project.config.from_adress, concerned_emails, message)
                mailer.close()
                return True
            except Exception as e:
                raise e
                #return False
        return False

    def _get_topic_data(self, topic_id):
        """
        Повертає Тему повідомлення, яка включає в себе назву теми і список отримувачів
        листів за цією темою.
        """
        
        #res = ['ramon1@ukr.net']
        res = None
        try:
            res = \
                  Q(mesgdb.MessageTopic).get(topic_id)
        except:
            pass
        return res
        
    def _get_switch_variable(self):
        # отримуємо змінну з GET для перемикання шаблонів
        if 'success' in self.req.GET:
            return 'success'
        if 'fail' in self.req.GET:
            return 'fail'
        return None

admin.admin.widgets.append(ACB_ContactUsForm)
