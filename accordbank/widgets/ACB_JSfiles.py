# -*- coding: utf-8 -*-
from sqlalchemy.sql.expression import asc,desc

from pyramid_cms import admin
from pyramid_cms.assets.widgets import ProjectWidget
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.apps.staticfiles import dbmodels as statsdb

class ACB_JSfiles(ProjectWidget):
    """
    Top navigator  widget for project side
    """
    default_temp_dir_index = 1
    items = statsdb.JSFile
    slots = statsdb.JSFilesSlot
    jsfiles = None
   
    
    def _get_jsfile_slots(self):
        """
        Отримуємо слоти для файлів і створюємо 
        відповідні записи у стеку для виводу
        1. стеки даних
        2. стеки з назвами відповідних файлів-макросів(для шаблонізатора)
        """
        # стек для назв файлів-макросів
        self.wdata['meta']['macros'] = {}
        
        slots = Q(self.slots).filter_by(active=True)
        for slot in slots:
            self.wdata['body'][slot.mnemo] = None
            # створюємо назву для файла з макросом
            self.wdata['meta']['macros'][slot.mnemo] = \
                'acb_jsfiles_{0}_macro.pt'.format(slot.mnemo)
            
    def _slot_fullfiller(self, mnemo, jsfiles):
        """
        Метод, що заповнює слот даними.
        перший агрумент - цільовий стек(мнемо-назва)
        другий агрумент - дані для внесення 
        """
        self.wdata['body'][mnemo] = [
                '/{0}{1}'.format('assets', i.fileitem)
                for i in jsfiles
            ]
    

    def dispatch(self):
        """
        Диспетчер даних для віджета
        1. отримуємо файлові слоти
        2. отримуємо загальний перелік файлів
        3. фільтруємо і розподіляємо файли по слотах
        """
        # 1
        self._get_jsfile_slots()

        # 2
        self.get_items_query = \
            Q(self.items).join(self.slots)\
                .filter(
                    self.items.site_id == self.req.curr_site.id,
                    self.items.active == True)\
                .order_by(asc(self.items.rank))

        # 3
        for mnemo in self.wdata['body']:
            jsfiles = \
                self.get_items(return_values=True)\
                    .filter(self.slots.mnemo == mnemo)
            self._slot_fullfiller(mnemo, jsfiles)
        """
        try:
            jsfiles = self.items\
                          .filter(
                              self.items.site_id == self.req.curr_site.id,
                              self.items.active == True)\
                          .order_by(asc(self.items.rank))
            self.wdata['body'] = [
                '/{0}{1}'.format('assets', i.fileitem)
                for i in jsfiles
            ]
        except:
            pass
        """
admin.admin.widgets.append(ACB_JSfiles)
