# -*- coding: utf-8 -*-
import os
from sqlalchemy.sql.expression import asc, desc

from pyramid_cms import admin
from pyramid_cms.assets.widgets import ProjectWidget
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.apps.staticfiles import dbmodels as statsdb


class ACB_CSSfiles(ProjectWidget):
    """
    Top navigator  widget for project side
    """
    default_temp_dir_index = 1
    items = statsdb.CSSFile

    def dispatch(self):
        """ Dispatcher method to get cssfiles for current site """
        try:
            cssfiles = self.items\
                           .filter(
                               self.items.site_id == self.req.curr_site.id,
                               self.items.active == True)\
                           .order_by(asc(self.items.rank))
            self.wdata['body'] = [
                '/{0}{1}'.format('assets', i.fileitem)
                for i in cssfiles
            ]
        except:
            pass
admin.admin.widgets.append(ACB_CSSfiles)
