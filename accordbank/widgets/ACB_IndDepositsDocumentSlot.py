# -*- coding: utf-8 -*-

"""
Widget implements documents (.pdf etc.) slot for
individuals/deposits tree node.
"""

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.widgets import ProjectWidget
from pyramid_cms.apps.files import dbmodels as filesdb

class ACB_IndDepositsDocumentSlot(ProjectWidget):
    """ Base class for widget implementation """
    
    # changing default template dir index
    default_temp_dir_index = 1
    
    wdata_body = ['slot']
    

    
    def dispatch(self):
        """ Method for widget`s data dispatching """
        self.get_items_query = \
            Q(filesdb.FileSlot)\
            .filter(filesdb.FileSlot.mnemo == self.__class__.__name__.lower())\
            .first()
        self.wdata['body']['slot'] = self.get_items(return_values=True)


# register widget in pcms engine stack
admin.admin.widgets.append(ACB_IndDepositsDocumentSlot)
