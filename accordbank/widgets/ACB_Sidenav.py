# -*- coding: utf-8 -*-

""" ...multiple lined comment..."""
# from sqlalchemy.sql.expression import asc, desc

from pyramid_cms import admin
from pyramid_cms.assets.widgets import ProjectWidget
from pyramid_cms.assets.db.sqlalchemy.connection import Q


class ACB_Sidenav(ProjectWidget):
    """
    Side navigator  widget for project side
    """
    default_temp_dir_index = 1
    pagetype_mnemo = None
    # сирі дані, що потребують обробки перед виводом у навігатор
    _raw_data = None

    def _get_pagetype_mnemo(self):
        """ Метод, що отримує значення pagetype.mnemo з поточної ноди """
        try:
            self.node = self.kw['node']
            self.pagetype_mnemo = self.node.pagetype.mnemo
        except:
            self.pagetype_mnemo = None

    def _get_data_for_current_node(self):
        """
        Отримуємо дані навігатора,
        актуальні для поточного типу сторінок.
        Попередньо має бути отримане значення self.pagetype_mnemo(тип сторінки)
        Якщо тип визначено, то метод виконує відповідний атрибут віджета,
        отриманий за маскою
        """
        # очищаємо стек для сирих даних
        self._raw_data = None
        # намагаємося виконати потрібний метод отримання даних
        if self.pagetype_mnemo:
            try:
                getattr(
                    self,
                    '_{0}_data'.format(self.pagetype_mnemo.lower()))()
            except:
                getattr(self, '_default_data')()

    def _default_data(self):
        """
        Значення для віджета, що отримуються,
        якщо не вказано/не знайдено додаткових опцій.
        Поки що за умовчанням використовується алгоритм виводу для звичайних
        текстових сторінок.
        """
        self._acb_textpage_data()

    def _acb_inddepositsindex_data(self):
        """ Дані для сторінки з переліком депозитів для фіз.осіб """
        from accordbank.apps.deposits import dbmodels as deposdb
        self.deposits = Q(deposdb.DepositCateg)\
            .filter(deposdb.DepositCateg.mnemo == 'individuals')\
            .first()\
            .deposits\
            .filter_by(active=True)\
            .order_by('rank')
        self._raw_data = self.deposits

    def _acb_inddepositpage_data(self):
        """ Дані для сторінки з інформацією про депозит для фіз.осіб """
        from accordbank.apps.deposits import dbmodels as deposdb
        self.deposits = Q(deposdb.DepositCateg)\
            .filter(deposdb.DepositCateg.mnemo == 'individuals')\
            .first()\
            .deposits\
            .filter_by(active=True)\
            .order_by('rank')
        self._raw_data = self.deposits

    def _acb_textpage_data(self):
        """
        Дані для бокового менюзвичайних текстових сторінок.
        ПРинцип отримання:
        Беремо розділ і отримуємо
        всі children. Виняток становлять:
        а) розділи адмін-частини
        б) розділи, позначені як "невидимі" (мають спеціальну властивість
        invisible_node, виставлену в True). Зазвичай це технічні розділи,
        які немає потреби відображати на клієнті - наприклад, сторінка  API
        для генерування відповідей на запити через ajax.
        """
        if self.node.children.count() > 0:
            self._raw_data = \
                self.node.children.filter_by(
                    admin_node=False,
                    invisible_node=False,
                    active=True)\
                .order_by('rank')
        else:
            self._raw_data = \
                self.node.parent.children.filter_by(
                    admin_node=False,
                    invisible_node=False,
                    active=True)\
                .order_by('rank')

    def _prepare_data(self):
        """ Підготовка даних для виводу їх у навігатор """
        self.wdata['body']['itemslist'] = None
        if self._raw_data:
            try:
                self.wdata['body']['itemslist'] = \
                    [
                        (
                            i.node.get_path(),
                            i.node.title_localized(self.req.curr_lang.id),
                            True if i.node.mnemo == self.node.mnemo else False
                        )
                        for i in self._raw_data
                    ]
            except AttributeError:
                self.wdata['body']['itemslist'] = \
                    [
                        (
                            i.get_path(),
                            i.title_localized(self.req.curr_lang.id),
                            True if i.mnemo == self.node.mnemo else False
                        )
                        for i in self._raw_data
                    ]
            except:
                pass

    def dispatch(self):
        """ Dispatcher method to retrieve data for side navigator. """

        """ Спочатку отримуємо тип сторінки """
        self._get_pagetype_mnemo()
        """ Потім виконуємо метод, що отримує потрібні нам дані """
        self._get_data_for_current_node()
        """ І готуємо ці дані для виводу у навігатор """
        self._prepare_data()

admin.admin.widgets.append(ACB_Sidenav)
