# -*- coding: utf-8 -*-
from pyramid_beaker import cache

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.widgets import ProjectWidget
from pyramid_cms.apps.roots import dbmodels as rootsdb


class ACB_Sitemap(ProjectWidget):
    """
    Віджет, що відображає мапу сайту.
    """
    default_temp_dir_index = 1

    wdata_body = ('header', 'itemslist')
    
    @cache.cache_region('long_term')
    def get_tree(self, itemslist, root):
        def _get_tree(stack, item):
            i = {'title':'', 'link': '', 'children': []}
            i['title'] = item.title_localized(self.req.curr_lang.id)
            i['path'] = item.get_path()
            for child in item.children\
            .filter_by(
            admin_node=False,
            invisible_node=False,
            active=True):
                _get_tree(i['children'], child)
            stack.append(i)
        _get_tree(itemslist, root)
        return itemslist

    def dispatch(self):
        itemslist = []
        root = Q(rootsdb.TreeNode).filter(rootsdb.TreeNode.parent_id == None).first()
        self.wdata['body']['header'] = self.data['pagetitle']
        self.wdata['body']['itemslist'] = self.get_tree(itemslist, root)
admin.admin.widgets.append(ACB_Sitemap)
