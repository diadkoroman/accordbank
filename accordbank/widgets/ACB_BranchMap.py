# -*- coding: utf-8 -*-
from sqlalchemy.sql.expression import asc, desc
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import DBSession, Q
from pyramid_cms.assets.widgets import ProjectWidget

from accordbank.apps.branches import dbmodels as brndb
from accordbank.apps.locations import dbmodels as locdb


class ACB_BranchMap(ProjectWidget):
    """
    Віджет, що відображає інформацію про відділення.
    Нехважаючи на те, що він називається branchmap, він
    призначений для відображення як мапи так і списку.
    Але головним режимом роботи віджета є відображення мапи відділень
    """
    default_temp_dir_index = 1

    _default_class = brndb.Branch
    _branchtype_class = brndb.BranchType
    _branchprop_class = brndb.BranchProperty
    _branchcity_class = locdb.City

    def get_header(self):
        """ Отримати заголовок сторінки """
        self.wdata['body']['header'] = None
        node = self.kw['node']
        try:
            self.wdata['body']['header'] = node.titles\
                .filter_by(lang_id=self.req.curr_lang.id).first().content
        except:
            pass

    def get_items(self):
        """ Отримуємо перелік відділень і банкоматів """
        self.wdata['body']['itemslist'] = {}
        self.items = Q(self._default_class)\
            .filter(self._default_class.active == True)\
            .order_by(asc(self._default_class.rank))

    def get_branches(self):
        """ Відсіяти із загального переліку тільки відділення """
        self.branches = self.items.filter_by(branchtype_id=1)
        self.wdata['body']['itemslist']['branches'] = self.branches

    def get_atms(self):
        """ Відсіяти із загального переліку тільки банкомати """
        # self.atms = self.items.filter_by(branchtype_id=2)
        # self.wdata['body']['itemslist']['atms'] = self.atms
        self.wdata['body']['itemslist']['atms'] = []

    def get_branchtypes(self):
        """ Отримати типи точок """
        try:
            self.wdata['body']['itemslist']['branchtypes'] = \
                Q(self._branchtype_class)\
                .filter(self._branchtype_class.active == True)
        except:
            self.wdata['body']['itemslist']['branchtypes'] = None

    def get_branchprops(self):
        """
        Отримати додаткові атрибути точок (фільтри вибірки)
        Тут додав костиль, щоб властивість has_atm лишалася активною
        але не відображалась.
        UP
        прибрав костиль, зробив через колонку в базі
        alter table pyramid_cms_branchprops add column 
        in_widget tinyint(1) default 1;
        """
        try:
            self.wdata['body']['itemslist']['branchprops'] = \
                Q(self._branchprop_class)\
                .filter(
                    self._branchprop_class.in_widget == True,
                    self._branchprop_class.active == True)
        except:
            self.wdata['body']['itemslist']['branchprops'] = None

    def get_branchcities(self):
        """ Отримати міста точок """
        try:
            self.wdata['body']['itemslist']['cities'] = \
                Q(self._branchcity_class)\
                    .filter(self._branchcity_class.active == True)\
                    .order_by(asc(self._branchcity_class.rank))
        except:
            self.wdata['body']['itemslist']['cities'] = None

    def get_branchlist_map_json(self):
        """ Отримуємо список точок, адаптований як json """
        self.wdata['body']['itemslist']['map_json'] = \
            self._default_class.get_branches_json(
                self.items, 
                self.req.curr_lang.id
                )

    def dispatch(self):
        """ Отримати дані для віджета """
        self.get_header()
        self.get_items()
        self.get_branchtypes()
        self.get_branchprops()
        self.get_branchcities()
        self.get_branches()
        self.get_atms()
        self.get_branchlist_map_json()

admin.admin.widgets.append(ACB_BranchMap)
