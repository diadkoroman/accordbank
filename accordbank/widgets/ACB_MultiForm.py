# -*- coding: utf-8 -*-
import re
import transaction
import smtplib
from pyramid.httpexceptions import HTTPFound
from pyramid_mailer import get_mailer
from pyramid_mailer.message import Message
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.widgets import ProjectWidget
from accordbank.apps.messages import dbmodels as mesgdb
from accordbank.forms import CallbackForm, QuestionForm


class ACB_MultiForm(ProjectWidget):
    """
    Віджет мультиформа для відображення різних форм, залежно від
    отриманої змінної
    """
    default_temp_dir_index = 1
    
    def dispatch(self):
        """
        Метод-диспетчер для виводу форм
        """
        self.wdata['body']['header'] = None
        self.wdata['body']['form'] = None
        self.wdata['body']['form_valid'] = None
        self.wdata['meta']['switch'] = None
        node = self.kw['node']
        self.wdata['meta']['curr_node'] = self.kw['node']
        self.path = '/{0}{1}'.format(self.req.curr_lang.mnemo, node.get_path())
        self.lang_id = self.req.curr_lang.id
        try:
            #self.wdata['body']['header'] = node.titles\
            #    .filter_by(lang_id=self.lang_id).first().content
            self.wdata['body']['header'] = self.data['header']
        except:
            pass
        try:
            #self.message_topics = \
            #    Q(mesgdb.MessageTopicCateg)\
            #    .filter(mesgdb.MessageTopicCateg.mnemo == node.mnemo)\
            #    .first().get_active_topics()
            self.message_topics = self.data['message_topics']
        except:
            self.message_topics = None
            
        """
        Тут ми отримуємо змінну з GET, котра дає вказівку шаблону, який саме
        під-шаблон йому відображати
        """
        self.wdata['meta']['switch'] = self._get_switch_variable()
        
    def get(self):
        """
        Залежно від мнемо-назви ноди обирається відповідний метод віджета,
        що відображає ту чи іншу форму.
        """
        try:
            getattr(self, 'get_{0}_form'.format(self.kw['node'].mnemo))()
        except AttributeError as ae:
            raise ae
            
    def post(self):
        """
        Залежно від мнемо-назви ноди обирається відповідний метод віджета,
        що відображає ту чи іншу форму.
        """
        try:
            getattr(self, 'get_{0}_form'.format(self.kw['node'].mnemo))()
        except AttributeError as ae:
            raise ae

    def get_callback_form(self):
        """
        ФОРМА ДЛЯ ЗАМОВЛЕННЯ ДЗВІНКА
        Отримуємо форму для розгортання форми для замовлення дзвінка
        -------------------------------------------------------------
        Є відмінності при розгортанні форми при POST-запитах - зокрема,
        ми повинні передавати ПОСТ у форму. Тому розділяємо дві гілки - 
        при ПОСТі працює одна, в інших випадках (GET майже виключно) - 
        інша.
        """
        if self.req.method == 'POST':
            # опрацювання POST
            form = CallbackForm(self.req.POST,
                                meta={'csrf_context': self.req.session})
            # виводимо список доступних для цієї форми тематик
            # окремо опрацьовуємо дефолтне значення
            form.topics.choices = \
                [
                    (
                        '',
                        self.kw['slots']['strings']['choose_theme']\
                        .localized(self.lang_id) 
                        if self.kw['slots']['strings'].get('choose_theme', False) 
                        else '----'
                    )
                ] + \
                [
                    (str(i.id), i.localized(self.lang_id))
                    for i in self.message_topics
                ]
            # Валідація і опрацювання отриманих даних
            if form.validate():
                # отримуємо тему із нею - список адресатів
                topic_data = self._get_topic_data(form.topics.data)
                # формуємо тіло листа
                body = \
                """
                Ім’я: {0}, Номер телефону: {1}, 
                Тематика звернення: {2}
                """.format(
                    form.sender_name.data,
                    str(form.sender_teln.data) \
                    if form.sender_teln.data else '',
                    topic_data.localized(self.lang_id)
                    )
                fdata = \
                {
                    'topic': self.kw['slots']['strings']['callback_order']\
                    .localized(self.lang_id) \
                    if self.kw['slots']['strings']\
                    .get('callback_order', False) \
                    else 'callback order',
                    
                    'body': body,
                    
                    'concerned_emails': topic_data.related_emails.split(',')
                    
                }

                if self.send_email(fdata=fdata):
                    # форма провалідувалася, лист надіслано
                    self.path += '?success'
                    # додаємо запис в БД
                    form.add_validated()
                else:
                    # форма провалідувалася, лист не надіслано
                    self.path += '?fail'
                raise HTTPFound(location=self.path)
        else:
            # опрацювання GET
            form = CallbackForm(meta={'csrf_context': self.req.session})
            # виводимо список доступних для цієї форми тематик
            # окремо опрацьовуємо дефолтне значення
            form.topics.choices = \
                [
                    (
                        '',
                        self.kw['slots']['strings']['choose_theme']\
                        .localized(self.lang_id) 
                        if self.kw['slots']['strings'].get('choose_theme', False) 
                        else '----'
                    )
                ] + \
                [
                    (str(i.id), i.localized(self.lang_id))
                    for i in self.message_topics
                ]
        self.wdata['body']['form'] = form
        
    def get_question_form(self):
        """
        ФОРМА ДЛЯ ЗАПИТАНЬ КЛІЄНТІВ
        Отримуємо форму для розгортання форми для замовлення дзвінка
        -------------------------------------------------------------
        Є відмінності при розгортанні форми при POST-запитах - зокрема,
        ми повинні передавати ПОСТ у форму. Тому розділяємо дві гілки - 
        при ПОСТі працює одна, в інших випадках (GET майже виключно) - 
        інша.
        """
        if self.req.method == 'POST':
            # опрацювання POST
            form = QuestionForm(
                self.req.POST,
                meta={'csrf_context': self.req.session})
            if form.validate():
                # отримуємо тему із нею - список адресатів
                topic_data = self._get_topic_data(form.topic_id.data)
                # формуємо тіло листа
                body = \
                """
                {0}
                ------------------------
                {5}
                Автор: {1},
                E-mail: {2}, 
                Текст повідомлення: {3},
                Телефон: {4}
                """.format(
                    self.kw['slots']['strings']['info_message_header']\
                    .localized(self.lang_id) \
                    if self.kw['slots']['strings']\
                    .get('info_message_header', False) \
                    else 'info_message_header',
                    form.sender_name.data,
                    form.sender_email.data,
                    form.content.data,
                    str(form.sender_teln.data) \
                    if form.sender_teln.data else '',
                    topic_data.localized(self.lang_id)
                    )
                fdata = \
                {
                    'topic': self.kw['slots']['strings']['question_order']\
                    .localized(self.lang_id) \
                    if self.kw['slots']['strings']\
                    .get('question_order', False) \
                    else 'question order',
                    
                    'body': body,
                    
                    'concerned_emails': topic_data.related_emails.split(',')
                    
                }
                if self.send_email(fdata=fdata):
                    # форма провалідувалася, лист надіслано
                    self.path += '?success'
                    # додаємо запис в БД
                    form.add_validated()
                else:
                    # форма провалідувалася, лист не надіслано
                    self.path += '?fail'
                raise HTTPFound(location=self.path)
        else:
            # опрацювання GET
            form = QuestionForm(meta={'csrf_context': self.req.session})
            # ідентифікатор теми у приховане поле
            form.topic_id.data = self.message_topics.first().id
        self.wdata['body']['form'] = form
            
            
    def send_email(self, fdata={}):
        if fdata:
            try:
                
                #
                mailer = smtplib.SMTP(
                    admin.project.config.smtpserver,
                    admin.project.config.smtpport)
                mailer.ehlo()
                mailer.starttls()
                mailer.ehlo()

                headers = \
                    "From: {0}\r\nTo: {1}\r\nSubject: {2}\r\n"\
                    .format(
                    admin.project.config.mailheader, 
                    ', '.join(fdata['concerned_emails']),
                    fdata['topic']
                    )
                message = \
                    headers.encode('UTF-8') + fdata['body'].encode('UTF-8')
                mailer.login(
                    admin.project.config.smtpuser,
                    admin.project.config.smtppassw)
                mailer.sendmail(
                    admin.project.config.from_adress,
                    fdata['concerned_emails'], message)
                mailer.close()
                return True
            except Exception as e:
                raise e
                #return False
        return False
        
    def _get_topic_data(self, topic_id):
        """
        Повертає Тему повідомлення, яка включає в себе назву теми і список отримувачів
        листів за цією темою.
        """
        
        #res = ['ramon1@ukr.net']
        res = None
        try:
            res = \
                  Q(mesgdb.MessageTopic).get(topic_id)
        except:
            pass
        return res
        
    def _get_switch_variable(self):
        # отримуємо змінну з GET для перемикання шаблонів
        if 'success' in self.req.GET:
            return 'success'
        if 'fail' in self.req.GET:
            return 'fail'
        return None
admin.admin.widgets.append(ACB_MultiForm)
