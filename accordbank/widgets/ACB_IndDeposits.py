# -*- coding: utf-8 -*-
from sqlalchemy.sql.expression import asc

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy.connection import Q
from pyramid_cms.assets.widgets import ProjectWidget

from accordbank.apps.deposits import dbmodels as deposdb


class ACB_IndDeposits(ProjectWidget):
    """
    Віджет, що виводить або дані для загальної сторінки
    депозитів фіз.осіб, або дані про окремий депозит.
    """
    default_temp_dir_index = 1

    items = deposdb.Deposit
    categs = deposdb.DepositCateg
    # поточні категорії депозитів для виводу
    category_mnemos = None
    # список активних депозитів у категорії(-ях)
    active_deposits = None
    # поточний депозит(якщо це сторінка окремого депозиту)
    current_deposit = None
    # процентні ставки для поточного депозиту
    current_deposit_rates = None
    #калькулятор для поточного депозиту
    current_deposit_calc = None

    def get_category_mnemos(self):
        """
        Отримати мнемо-назву категорії депозитів.
        Для цього використовуємо ноду(або її парент)
        """
        # node = self.kw['node']
        self.category_mnemos = [categ[0] for categ in Q(self.categs.mnemo)
                                .filter(
                                    self.categs.mnemo.in_(self.kw['tail']))]


    def get_active_deposits(self):
        """
        Отримуємо всі депозити, що входять до поточної категорії
        """
        self.active_deposits = Q(self.items)\
            .filter(
                self.items.categs
                .any(self.categs.mnemo.in_(self.category_mnemos)))\
            .filter(self.items.active == True)\
            .order_by(asc(self.items.rank))

    def get_current_deposit(self):
        """
        Пробуємо отримати поточний депозит.
        Від успіху роботи цієї функції залежить, що саме буде відображати
        віджет. Якщо депозит отримано - віджет відображатиметься в режимі
        "Сторінка депозиту". Якщо депозиту немає - відображатиметься
        список депозитів.
        """

        self.current_deposit = self.active_deposits\
            .filter_by(mnemo=self.kw['node'].mnemo).first()

    def get_current_deposit_rates(self):
        """
        Отримати відсоткові ставки депозиту у підготовленому вигляді
        для формування таблиці.
        """
        if self.current_deposit and not self.current_deposit.manual_mode:
            self.current_deposit_rates = self.current_deposit.get_rates_table()
            
    def get_current_deposit_calc(self):
        """
        Намагаємося отримати калькулятор для
        """
        # якщо є поточний депозит...
        if self.current_deposit:
            # і в цього депозита є калькулятор...
            if getattr(self.current_deposit, 'calculator', False):
                # отримуємо дані для калькулятора
                try:
                    return self.get_current_deposit_calc_formdata()
                except:
                    pass
        return False
        
    def get_current_deposit_calc_formdata(self):
        """
        Отримати деякі дані для форми калькулятора депозиту
        """
        data = {}
        # отримуємо ідентифікатор поточного депозиту
        try:
            data['depid'] = self.current_deposit.id
        except Exception as e:
            raise e
        # отримуємо валюти, в яких можна відкривати цей депозит
        try:
            data['currencies'] = self.current_deposit.get_currencies()
        except Exception as e:
            raise e
        # отримуємо терміни, на які можна відкривати цей депозит
        try:
            data['terms'] = self.current_deposit.get_terms()
        except Exception as e:
            raise e
        # отримуємо типи виплати відсотків, які пропонуються за цим депозитом
        try:
            data['iptypes'] = self.current_deposit.get_iptypes2()
        except Exception as e:
            raise e
        return data

    def dispatch(self):
        """ Отримуємо дані для віджета """
        self.get_category_mnemos()
        self.get_active_deposits()
        self.get_current_deposit()
        self.get_current_deposit_rates()
        self.wdata['body']['test'] = self.data['pagetitle']
        self.wdata['body']['header'] = self.data['pagetitle']
        self.wdata['body']['itemslist'] = self.active_deposits
        self.wdata['body']['curr_item'] = self.current_deposit
        self.wdata['body']['curr_item_calc'] = self.get_current_deposit_calc()
        self.wdata['body']['curr_item_rates'] = self.current_deposit_rates
admin.admin.widgets.append(ACB_IndDeposits)
