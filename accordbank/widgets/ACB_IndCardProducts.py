# -*- coding: utf-8 -*-
from sqlalchemy.sql.expression import asc

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy.connection import Q
from pyramid_cms.assets.widgets import ProjectWidget

from accordbank.apps.cards import dbmodels as cardsdb


class ACB_IndCardProducts(ProjectWidget):
    """
    Віджет, що виводить або дані для загальної сторінки
    карткових продуктів фіз.осіб, або дані про окремий картковий продукт.
    """
    default_temp_dir_index = 1

    items = cardsdb.CardProduct
    categs = cardsdb.CardProductCateg
    # поточні категорії карткових продуктів для виводу
    category_mnemos = None
    # список активних продуктів у категорії(-ях)
    active_products = None
    # поточний продукт(якщо це сторінка окремого продукту)
    current_product = None
    # процентні ставки для поточного депозиту
    current_deposit_rates = None

    def get_category_mnemos(self):
        """
        Отримати мнемо-назву категорії.
        Для цього використовуємо ноду(або її парент)
        """
        # node = self.kw['node']
        self.category_mnemos = [categ[0] for categ in Q(self.categs.mnemo)
                                .filter(
                                    self.categs.mnemo.in_(self.kw['tail']))]

    def get_active_products(self):
        """
        Отримуємо всі продукти, що входять до поточної категорії
        """
        self.active_products = Q(self.items)\
            .filter(
                self.items.categs
                .any(self.categs.mnemo.in_(self.category_mnemos)))\
            .filter(self.items.active == True)\
            .order_by(asc(self.items.rank))

    def get_current_product(self):
        """
        Пробуємо отримати поточний продукт.
        Від успіху роботи цієї функції залежить, що саме буде відображати
        віджет. Якщо продукт отримано - віджет відображатиметься в режимі
        "Сторінка продукту". Якщо продукту немає - відображатиметься
        список продуктів.
        """

        self.current_product = self.active_products\
            .filter_by(mnemo=self.kw['node'].mnemo).first()


    def dispatch(self):
        """ Отримуємо дані для віджета """
        self.get_category_mnemos()
        self.get_active_products()
        self.get_current_product()
        self.wdata['body']['test'] = self.data['pagetitle']
        self.wdata['body']['header'] = self.data['pagetitle']
        self.wdata['body']['itemslist'] = self.active_products
        self.wdata['body']['curr_item'] = self.current_product
admin.admin.widgets.append(ACB_IndCardProducts)
