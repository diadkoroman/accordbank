# -*- coding: utf-8 -*-
from pyramid_cms import admin
from pyramid_cms.assets.widgets import ProjectWidget

class ACB_Metas(ProjectWidget):
    """
    Top navigator  widget for project side
    """
    default_temp_dir_index = 1
    
    def get_description(self):
        # get node description
        self.description = \
        self.kw['node'].description_localized(self.req.curr_lang.id)
        
    def get_keywords(self):
        # get keywords
        kw = self.kw['node'].get_kwords()
        if isinstance(kw, (list, )):
            self.keywords = ','.join([i.content for i in kw])
        else:
            self.keywords = kw
    
    def dispatch(self):
        # data dispatcher
        self.get_description()
        self.wdata['body']['description'] = self.description
        self.get_keywords()
        self.wdata['body']['keywords'] = self.keywords
admin.admin.widgets.append(ACB_Metas)
