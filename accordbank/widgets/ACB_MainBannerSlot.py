# -*- coding: utf-8 -*-
from sqlalchemy.sql.expression import asc,desc
from pyramid_beaker import cache


from pyramid_cms import admin
from pyramid_cms.assets.widgets import ProjectWidget
from pyramid_cms.assets.db.sqlalchemy.connection import Q
from pyramid_cms.apps.banners import dbmodels as bannersdb

class ACB_MainBannerSlot(ProjectWidget):
    """
    Site mainmenu
    """
    default_temp_dir_index = 1
    items = bannersdb.Banner
    slots = bannersdb.BannerSlot
    
    wdata_body = ('itemslist',)
    
    """
    @cache.cache_region('long_term')
    def old_get_items(self):
        return Q(self.items, self.slots.scrlint)\
                            .join(self.slots)\
                            .filter(
                                self.slots.mnemo == self.mnemo,
                                self.items.active ==True)\
                            .order_by(asc(self.items.rank))
    """
    
    @cache.cache_region('long_term')
    def get_items(self, item):
        """ Отримуємо дані для банерів і відразу опрацьовуємо їх """
        loc = item[0].localized(self.req.curr_lang.id)
        i = \
            {
            'ident': item[0].id,
            'scrlint': item[1],
            'image': loc.image,
            'link': loc.link or item[0].link
            }
        return i

    def dispatch(self):
        items = Q(self.items, self.slots.scrlint)\
                .join(self.slots)\
                .filter(
                        self.slots.mnemo == self.mnemo,
                        self.items.active ==True)\
                .order_by(asc(self.items.rank))
        #try:
        #    instances = self.get_items()
        #    self.wdata['body'] = instances
        #except:
        #    pass
        self.wdata['body']['itemslist'] = list(map(self.get_items, items))
admin.admin.widgets.append(ACB_MainBannerSlot)
