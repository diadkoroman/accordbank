# -*- coding: utf-8 -*-

"""
Widget implements documents (.pdf etc.) slot for
individuals/deposits tree node.
"""
from sqlalchemy.sql.expression import asc, desc

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.widgets import ProjectWidget
from pyramid_cms.apps.files import dbmodels as filesdb

class ACB_UnifiedDocumentSlot(ProjectWidget):
    """ Base class for widget implementation """

    # changing default template dir index
    default_temp_dir_index = 1
    
    sort_criteria = None
    # additional wdata components
    wdata_body = ['output']
    wdata_meta = ['sort_criteria']

    def get_sorting_parameters(self):
        """
        Get parameters for sorting output
        Default sorting criteria is 'years'
        """
        # sorting by years is default option too
        if not self.req.GET or 'years' in self.req.GET:
            self.get_years()

    def get_years(self):
        self.sort_criteria = Q(filesdb.FileCategory)\
            .filter(filesdb.FileCategory.mnemo.like('%20%'))\
            .order_by(desc(filesdb.FileCategory.mnemo))
        self.wdata['meta']['sort_criteria'] = self.sort_criteria

    def dispatch(self):
        """ Method for widget`s data dispatching """
        self.years = None
        # getting slot mnemo
        self.get_slot_mnemo(1)
        # getting categories
        self.get_categories(1)
        self.get_sorting_parameters()
        self.get_items_query = \
            Q(filesdb.UploadedFile)\
            .filter(filesdb.UploadedFile.slots\
                .any(filesdb.FileSlot.mnemo.in_(self.slot_mnemos)))\
            .filter(filesdb.UploadedFile.categs\
                .any(filesdb.FileCategory.mnemo.in_(self.slot_categs)))\
            .order_by(desc(filesdb.UploadedFile.rank))
        self.manage_query()
        #self.wdata['body']['output'] = self.get_items(return_values=True)

    def manage_query(self):
        if self.sort_criteria:
            try:
                self.wdata['body']['output'] = []
                for i in self.sort_criteria:
                    d = \
                    {
                        'name': i,
                        'data': self.get_items_query\
                        .filter(filesdb.UploadedFile.categs\
                            .any(filesdb.FileCategory.mnemo == i.mnemo))
                    }
                    if d['data'].count() > 0:
                        self.wdata['body']['output'].append(d)
                """ 
                Вивід без розбивки на категорії
                """
                if not self.wdata['body']['output']:
                    self.wdata['meta']['sort_criteria'] = None
                    self.wdata['body']['output'] = self.get_items(return_values=True)
            except:
                self.wdata['body']['output'] = self.get_items(return_values=True)
        else:
            self.wdata['body']['output'] = self.get_items(return_values=True)

    def get_slot_mnemo(self, index):
        # getting slot mnemo from URL
        #self.slot_mnemos = self.kw['tail'][index:len(self.kw['tail'])]
        self.slot_mnemos = self.kw['tail'][0:index]
        # raise Exception(self.slot_mnemos)
        
    def get_categories(self, index):
        """
        Getting category of documents to output
        """
        # self.slot_categs = self.kw['tail'][index:len(self.kw['tail'])]
        self.slot_categs = self.kw['tail'][-1],


# register widget in pcms engine stack
admin.admin.widgets.append(ACB_UnifiedDocumentSlot)
