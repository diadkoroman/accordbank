import unittest
from pyramid import testing
from pyramid_cms.assets.db.sqlalchemy.connection import DBSession, Base, Q
from pyramid_cms.apps.navs import dbmodels as navsdb

###############################################################################
"""
Тести для навігаційних віджетів
"""
class NavsTest(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        from sqlalchemy import create_engine
        engine = create_engine('mysql+pymysql://acb_site:p!ssw4acbDB@localhost/acb_sitedb?charset=utf8&use_unicode=1&unix_socket=/var/run/mysql/mysql.sock')
        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        
    def test_ACB_Topnav(self):
        from sqlalchemy.sql.expression import asc,desc
        """ тестування роботи горішнього меню """
        self.items = navsdb.NavItem
        self.slots = navsdb.NavSlot
        self.mnemo = 'acb_topnav'
        q = Q(self.items)\
            .join(self.slots)\
            .filter(
                self.slots.mnemo == self.mnemo,
                self.items.active ==True)\
            .order_by(asc(self.items.rank))
        print(q)

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()
###############################################################################
