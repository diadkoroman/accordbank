# -*- coding: utf-8 -*-
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.widgets import ProjectWidget
from pyramid_cms.apps.roots import dbmodels as rootsdb

class ACB_NewsText(ProjectWidget):
    """
    Текстовий блок для сторінки з новиною.
    Від звичайного текстового блоку відрізняється тим, що додатково
    виводить дату публікації новини.
    """
    default_temp_dir_index = 1
    
    def dispatch(self):
        """ Отримуємо дані для виводу у віджет """
        self.wdata['body']['header'] = None
        self.wdata['body']['published'] = None
        self.wdata['body']['text'] = None
        node = self.kw['node']
        try:
            self.wdata['body']['header'] = node.titles\
            .filter_by(lang_id=self.req.curr_lang.id).first().content
            self.wdata['body']['published'] = node.date_visible.strftime('%d.%m.%Y')
            self.wdata['body']['text'] = node.texts\
            .filter_by(lang_id=self.req.curr_lang.id).first().content
        except:
            pass
admin.admin.widgets.append(ACB_NewsText)
