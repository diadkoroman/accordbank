# -*- coding: utf-8 -*-
from pyramid_cms import admin
from pyramid_cms.assets.widgets import ProjectWidget


class ACB_Breadcrumbs(ProjectWidget):
    """
    "Хлібні крихти" для використання на боці проекту.
    """
    default_temp_dir_index = 1

    def dispatch(self):
        if self.kw.get('node', None):
            try:
                self.wdata['body'] = self.kw['node'].get_bcrs()
            except:
                pass

admin.admin.widgets.append(ACB_Breadcrumbs)
