# -*- coding: utf-8 -*-
"""
Віджет, що відображає інформацію про відділення
1. Загальна текстова інформація
2. Дані  для карти, котра відображатиме розташування відділення.
"""

from sqlalchemy.sql.expression import asc, desc
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import DBSession, Q
from pyramid_cms.assets.widgets import ProjectWidget

from accordbank.apps.branches import dbmodels as brndb
from accordbank.apps.locations import dbmodels as locdb

class ACB_BranchInformation(ProjectWidget):
    """ Безпосередньо клас віджета """

    default_temp_dir_index = 1
    default_class = brndb.Branch
    
    wdata_body = 'item',

    def dispatch(self):
        """
        Диспетчер даних віджета
        """
        #try:
        self.wdata['body']['item'] = \
            self.kw['node'].branch.get_branchinfo(self.req.curr_lang.id)
        #except:
        #    pass
admin.admin.widgets.append(ACB_BranchInformation)
