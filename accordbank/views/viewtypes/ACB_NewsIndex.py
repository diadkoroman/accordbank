# -*- coding: utf-8 -*-
#from lxml import etree
from pyramid.httpexceptions import HTTPFound
from pyramid.response import Response

from pyramid_cms import admin
from pyramid_cms.assets.views import ProjectViewType
from pyramid_cms.config.scripts.init_scenarios import start_init

class ACB_NewsIndex(ProjectViewType):

    #template = "admin_home.pt"
    #template = "project_home.xsl"
    #test = True
    #test_xml_document = True
    #widgets = ["topnav","welcome_teaser"]

    def prepare_data(self):
        pass

admin.admin.viewtypes.append(ACB_NewsIndex)
