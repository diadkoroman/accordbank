# -*- coding: utf-8 -*-
#from lxml import etree
import decimal
from pyramid.httpexceptions import HTTPFound
from pyramid.response import Response
from pyramid.view import view_config

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.views import AjaxViewType
from pyramid_cms.config.scripts.init_scenarios import start_init

class ProjectAjaxPage(AjaxViewType):
    """  """
    renderer = 'json'
    
    
    def inddepcalc_option(self):
        from accordbank.apps.deposits import dbmodels as deposdb
        from accordbank.apps.currencies import dbmodels as currsdb
        from accordbank.forms import CalculatorForm
        try:
            deposit = Q(deposdb.Deposit).get(self.data['depid'])
            form = CalculatorForm(meta={'csrf_context': self.req.session})
            for k, v in self.data.items():
                try:
                    setattr(getattr(form, k), 'data', v)
                except:
                    pass
            """
            Вносимо в поле можливих виборів тільки ті валюти, 
            в яких можна відкривати обраний депозит
            """
            form.currencies.choices = \
                [
                    (str(i.id),'') 
                    for i in deposit.get_currencies()
                ]
            """
            Вносимо у поле можливих тільки ті терміни, 
            на які можна відкривати поточний депозит. 
            """
            form.terms.choices = \
                [
                    (str(i.id),'') 
                    for i in deposit.get_terms()
                ]
            """
            Вносимо у поле можливих типі виплати відсотків
            тільки ті, які пропонуються за умовами поточного депозиту
            iptypes2 - тому що iptypes повертає лише текстову інформацію
            """
            form.iptypes.choices = \
                [
                    (str(i.id),'') 
                    for i in deposit.get_iptypes2()
                ]
            if form.validate():
                """
                Опрацювання запиту депозитного калькулятора
                для розділу "Фізичним особам"
                
                РОБОТА КАЛЬКУЛЯТОРА
                --------------------
                1. отримати екземпляр тарифа, у якого співпадуть:
                    - ідентифікатор депозиту
                    - ідентифікатор терміну
                    - ідентифікатор валюти !!!!!
                    - ідентифікатор способу виплати відсотків
                і який є діючим
                2. провести обрахування доходу без врахування податків
                на основі введеної суми і відсоткової ставки тарифу
                3. сформувати словник даних для відповіді
                """
                #1
                tariff = \
                    Q(deposdb.DepositTariff)\
                        .filter(
                            deposdb.DepositTariff.item_id == 
                            self.data['depid'],
                            deposdb.DepositTariff.currency_id == 
                            self.data['currencies'],
                            deposdb.DepositTariff.term_id == 
                            self.data['terms'],
                            deposdb.DepositTariff.ipt_id == 
                            self.data['iptypes'],
                            deposdb.DepositTariff.active == 
                            True
                            )\
                        .first()

                #2
                #загальна сума відсотків за рік
                ti_amount = decimal.Decimal(self.data['amount']) * tariff.rate.value

                if tariff.ipt.mnemo.endswith('_cap'):
                    # для тарифа з капіталізацією відсотків дещо інша формула
                    ti_income = 0
                    for x in range(int(tariff.term.value)):
                        # відсотки, що набіжать за місяць
                        # збільшуються щомісяця, бо зростає сума
                        ti_amount_permonth = ti_amount / decimal.Decimal(12)
                        # сума збільшується на суму нарахованих за місяць відсотків
                        ti_amount += ti_amount_permonth
                        # оскільки сума щомісяця змінюється, в кожному циклі додаємо
                        ti_income += ti_amount_permonth
                else:
                    # відсотки, що набіжать за місяць
                    ti_amount_permonth = ti_amount / decimal.Decimal(12)
                    # дохід - відсотки за місяць, помножені на кількість місяців
                    ti_income = ti_amount_permonth * tariff.term.value
                # оподаткований дохід - дохід після оподаткування
                tax = ti_income * tariff.tax.value
                ti_profit = ti_income - tax
                
                #3
                self.resp = \
                    {
                        'rate': tariff.rate.inner_name,
                        'income': str(round(ti_income, 2)),
                        'tax': str(round(tax, 2)),
                        'profit': str(round(ti_profit, 2)),
                        'currmncode': tariff.currency.mncode.upper(),
                    }
            else:
                self.resp = {'err': form.errors}
        except Exception as e:
            self.resp = {'err': e}

    def get(self):
        return super().get(self.renderer)

    def post(self):
        return super().post(self.renderer)

admin.admin.viewtypes.append(ProjectAjaxPage)
