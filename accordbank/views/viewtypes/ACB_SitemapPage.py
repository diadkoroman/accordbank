# -*- coding: utf-8 -*-
#from lxml import etree
from pyramid.httpexceptions import HTTPFound
from pyramid.response import Response

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.views import ProjectViewType
from pyramid_cms.config.scripts.init_scenarios import start_init

from pyramid_cms.apps.roots import dbmodels as rootsdb

class ACB_SitemapPage(ProjectViewType):
    """
    В’юха для сторінки з картою сайту
    """
    def prepare_data(self):
        pass

admin.admin.viewtypes.append(ACB_SitemapPage)
