# -*- coding: utf-8 -*-
#from lxml import etree
from pyramid.httpexceptions import HTTPFound
from pyramid.response import Response

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.views import ProjectViewType
from accordbank.apps.messages import dbmodels as mesgdb
from pyramid_cms.config.scripts.init_scenarios import start_init

class ACB_MultiFormPage(ProjectViewType):

    #template = "admin_home.pt"
    #template = "project_home.xsl"
    #test = True
    #test_xml_document = True
    #widgets = ["topnav","welcome_teaser"]

    def prepare_data(self):
        self.vdata['header'] = \
            self.node.titles\
                .filter_by(lang_id=self.req.curr_lang.id).first().content
        # отримуємо теми повідомлень
        self.vdata['message_topics'] = Q(mesgdb.MessageTopicCateg)\
                .filter(mesgdb.MessageTopicCateg.mnemo == self.node.mnemo)\
                .first().get_active_topics()

admin.admin.viewtypes.append(ACB_MultiFormPage)
