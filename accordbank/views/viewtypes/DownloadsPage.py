# -*- coding: utf-8 -*-

"""
Put __doc__ info here.
"""

from pyramid_cms import admin
from pyramid_cms.assets.views import DownloadsViewType
from pyramid_cms.config.scripts.init_scenarios import start_init

class DownloadsPage(DownloadsViewType):
    
    root_dir = admin.project.config.files
    daily_file_downloads_limit = 30
    daily_total_downloads_limit = 200

admin.admin.viewtypes.append(DownloadsPage)
