from pyramid.config import Configurator
from sqlalchemy import engine_from_config
from pyramid_beaker import set_cache_regions_from_settings

from .settings import admin
from pyramid_cms.assets.db.sqlalchemy.connection import DBSession, Base
from pyramid_cms import admin_init

def add_static_routes(config):
    """ Adding static routes """
    config.add_static_view("foundation",
                           "{0}:static/f6"
                           .format(admin.project.config.packagename),
                           cache_max_age=3600)
    # files
    config.add_static_view("assets",
                           "{0}:files"
                           .format(admin.project.config.packagename),
                           cache_max_age=3600)
    # path for images
    config.add_static_view("images",
                           "{0}:files/images"
                           .format(admin.project.config.packagename),
                           cache_max_age=3600)

    # path for pdf-downloads
    # config.add_static_view("downloads/pdf",
    #                        "{0}:files/pdf"
    #                        .format(admin.project.config.packagename),
    #                        cache_max_age=3600)
    

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    set_cache_regions_from_settings(settings)
    config = Configurator(settings=settings)
    # static routes
    add_static_routes(config)
    
    
    config.include('pyramid_chameleon')
    config.include('pyramid_mailer')
    config.include('pyramid_cms')
    config.include('pggeo')
    

    config.scan()
    
    """
    Ініціалізація адмін-частини.
    Під час неї створюються таблиці для всіх типів контенту,
    перевіряються і створюються розділи для типів контенту і розділи
    для плагінів.
    """
    admin_init(engine, config)

    return config.make_wsgi_app()
