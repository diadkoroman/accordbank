# -*- coding: utf-8 -*-
"""
Внутрішня конфігураційна інформація для плагіна "News"
"""
newsindex_pagetype_mnemo = 'acb_newsindex'
newspage_pagetype_mnemo = 'acb_newspage'
default_news_tag = 'news'
