# -*- coding: utf-8 -*-

import time

"""
Форми для плагіна "Новини".
Практично повністю повторюють код форм
pyramid_cms.plugins.tree.ui.forms
за винятком деяких автоматизованих процедур, зокрема:
- автоматичне призначення типу сторінки;
"""

from wtforms.fields import (
    DateTimeField,
    SelectMultipleField,
    SelectField,
    FormField,
    FieldList,
    FileField,
    HiddenField,
    TextField,
    TextAreaField
    )
from wtforms import widgets
from wtforms import validators
from wtforms import form
from wtforms_sqlalchemy.fields import (
    QuerySelectField,
    QuerySelectMultipleField
)

from wtforms_alchemy import ModelForm

from pyramid_cms.assets.db.sqlalchemy.connection import DBSession, Q
from pyramid_cms.assets.forms import UIForm
from pyramid_cms.assets.forms import FormInit
from pyramid_cms.apps.roots import dbmodels as rootsdb
from pyramid_cms.apps.auth import dbmodels as authdb

""" Внутрішні конфігураційні дані плагіна """
from accordbank.plugins.news import local_settings as ls



class TitleForm(ModelForm):
    """
    Additional form to create node titles
    """
    class Meta:
        model = rootsdb.PageTitle
        exclude = ['active']
    lang_id = HiddenField()

class DescrForm(ModelForm):
    """
    Additional form to create node titles
    """
    class Meta:
        model = rootsdb.PageDescription
        exclude = ['active']
    lang_id = HiddenField()

class TextForm(ModelForm):
    """
    Additional form to create node titles
    """
    class Meta:
        model = rootsdb.PageText
        exclude = ['active']
    lang_id = HiddenField()
    
    
class NewsNodeForm(UIForm):
    """ News node form  """
    exclude = (
        'id',
        #'created',
        'mnemo',
        'updated',
        'site_id',
        'pagetype_id',
        'parent_id',
        'sibling_id',
        'redirect_id',
    )
    errors_tracker = None

    class Meta:
        model = rootsdb.TreeNode

    site = QuerySelectField(allow_blank=True, blank_text='----')
    #pagetype = QuerySelectField(allow_blank=True, blank_text='----')
    #parent = QuerySelectField(allow_blank=True, blank_text='----')
    redirect = QuerySelectField(allow_blank=True, blank_text='----')
    tags = QuerySelectMultipleField()
    date_visible = DateTimeField()
    created = DateTimeField()

    def validate_and_add(self):
        """ Validate and add item """
        columns = [c for c in self.Meta.model.columns_names()
                   if not c in self.exclude]
        if self.validate():
            obj = rootsdb.TreeNode()
            obj.titles = []
            obj.descriptions = []
            obj.texts = []
            obj.tags = []
            for attr in columns:
                setattr(obj,attr, getattr(self, attr).data)

            for t in self.titles:
                # titles
                title = rootsdb.PageTitle()
                title.lang_id = t.lang_id.data
                title.content = t.content.data
                obj.titles.append(title)
            for d in self.descriptions:
                # descriptions
                description = rootsdb.PageDescription()
                description.lang_id = d.lang_id.data
                description.content = d.content.data
                obj.descriptions.append(description)
            for t in self.texts:
                # texts
                text = rootsdb.PageText()
                text.lang_id = t.lang_id.data
                text.content = t.content.data
                obj.texts.append(text)
            # inner_name
            obj.inner_name = self.titles[0].content.data
            # site
            obj.site = self.site.data
            
            # mnemo
            obj.mnemo = str(int(time.time()))

            """
            Тип сторінки автоматично втсановлюється як "Сторінка новини".
            Отримується за мнемо-назвою.Мнемо-назва, в свою чергу,
            зазначаєтьсяу файлі local_settings.py
            """
            obj.pagetype = \
            Q(rootsdb.PageType)\
            .filter(rootsdb.PageType.mnemo == ls.newspage_pagetype_mnemo)\
            .first()

            """
            атьківський розідл автоматично встановлюється як 
            "Перша сторінка новин".
            Отримується за мнемо-назвою.Мнемо-назва, в свою чергу,
            зазначаєтьсяу файлі local_settings.py
            """
            # parent
            obj.parent_id = \
            Q(rootsdb.TreeNode).join(rootsdb.PageType)\
            .filter(rootsdb.PageType.mnemo == ls.newsindex_pagetype_mnemo)\
            .first().id
            
            # redirect
            if self.redirect.data not in (0, None, 'None'):
                obj.redirect = self.redirect.data

            # permissions
            obj.permissions = [i for i in Q(authdb.Permission)\
                .filter(authdb.Permission.name == 'view')]

            """
            Якщо при створенні новинного розділу не зазначено пошукових тегів,
            то автоматиом виставляється тег "новини".
            Дефолтнй тег "новини" зазначається у файлі local_settings.py
            """
            if self.tags.data not in (0, None, 'None', []):
                obj.tags = self.tags.data
            else:
                obj.tags = \
                Q(rootsdb.TreeNodeTag)\
                .filter(rootsdb.TreeNodeTag.content == ls.default_news_tag)\
                .all()

            obj.save()
            return True
        return False

    def validate_and_edit(self):
        """ Validate and change item """
        columns = [c for c in self.Meta.model.columns_names()
                   if not c in self.exclude]
        if self.validate():
            obj = self._obj
            for attr in columns:
                setattr(obj,attr, getattr(self, attr).data)

            for t in self.titles:
                # titles
                #try:
                title = obj.titles\
                           .filter_by(lang_id=t.lang_id.data)\
                           .first()
                #except:
                if not title:
                    title = rootsdb.PageTitle()
                title.lang_id = t.lang_id.data
                title.content = t.content.data
                obj.titles.append(title)
            for d in self.descriptions:
                # descriptions
                #try:
                description = obj.descriptions\
                                 .filter_by(lang_id=d.lang_id.data)\
                                 .first()
                #except:
                if not description:
                    description = rootsdb.PageDescription()
                description.lang_id = d.lang_id.data
                description.content = d.content.data
                obj.descriptions.append(description)
            for t in self.texts:
                # texts
                #try:
                text = obj.texts\
                          .filter_by(lang_id=t.lang_id.data)\
                          .first()
                #except:
                if not text:
                    text = rootsdb.PageText()
                text.lang_id = t.lang_id.data
                text.content = t.content.data
                obj.texts.append(text)

            # inner_name
            obj.inner_name = self.titles[0].content.data
            # site
            obj.site = self.site.data
            
            # mnemo
            if self.mnemo.data in ('', None, 0):
                obj.mnemo = str(int(time.time()))

            """
            Поля "Тип сторінки", "Батьківський розділ" оновленню не підлягають,
            оскільки опція зміни типології і розташування сторінки новин 
            не передбачена.
            """
            # redirect
            if self.redirect.data not in (0, None, 'None'):
                obj.redirect = self.redirect.data

            # permissions
            obj.permissions = [i for i in Q(authdb.Permission)\
                .filter(authdb.Permission.name == 'view')]

            """
            Якщо при створенні новинного розділу не зазначено пошукових тегів,
            то автоматиом виставляється тег "новини".
            Дефолтнй тег "новини" зазначається у файлі local_settings.py
            """
            if self.tags.data not in (0, None, 'None', []):
                obj.tags = self.tags.data
            else:
                obj.tags = \
                Q(rootsdb.TreeNodeTag)\
                .filter(rootsdb.TreeNodeTag.content == ls.default_news_tag)\
                .all()

            # created data
            obj.created = self.created.data
            obj.save()
            return True
        return False

def modify_form(entries):
    """ Append additional props to edit item form """
    class_obj = NewsNodeForm
    setattr(class_obj,
            'titles',
            FieldList(FormField(TitleForm),
                      min_entries=entries))
    setattr(class_obj,
            'descriptions',
            FieldList(FormField(DescrForm),
                      min_entries=entries))
    setattr(class_obj,
            'texts',
            FieldList(FormField(TextForm),
                      min_entries=entries))
    return class_obj


class NewsNodeFormInit(FormInit):
    """ Form init for edit item form """

    def set_form_vars(self):
        self.f.site.query = Q(rootsdb.Site).all()
        self.f.redirect.query = Q(rootsdb.TreeNode).all()
        self.f.tags.query = Q(rootsdb.TreeNodeTag).all()
