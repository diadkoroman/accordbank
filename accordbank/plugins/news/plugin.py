# -*- coding: utf-8 -*-
"""
Плагін для роботи з різними типами новин.
"""

import os

from sqlalchemy.sql.expression import asc,desc

#import transaction
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.plugins import Plugin

""" Цей плагін використовує як основу моделі адмінки, тому імпортуємо їх """
from pyramid_cms.apps.roots import dbmodels as rootsdb

""" Внутрішні конфігураційні дані плагіна """
from accordbank.plugins.news import local_settings as ls

""" Форми для взаємодії користувача з даними через плагін """
from .ui import forms


def langs_count():
    """
    Функція, що генерує список доступних мов.
    """
    lc = 0
    try:
        lc = Plugin.count_langs(langs=Plugin.get_langs())
    except:
        pass
    return lc
    
class News(Plugin):
    """ ...multiple lined comment..."""
    packagename = admin.project.config.packagename
    template = 'plugins/news/templates/news_widget.pt'
    
    """ Перелік мов """
    langs = Plugin.get_langs()

    """ Кількість мов """
    langs_count =  langs_count()
    
    """ Перелік категорій """
    categs = None
    """ Перелік новин (загальний) """
    items = None
    
    # основний клас даних для плагіна
    _default_class = rootsdb.TreeNode
    # допоміжні класи даних для плагіна
    _pagetype_class = rootsdb.PageType
    _tags_class = rootsdb.TreeNodeTag

    # форма для роботи з даними депозиту
    _default_form_class = forms.NewsNodeFormInit(forms.modify_form(langs_count))
    
    def __init__(self, section_class=None, parent=None):
        """
        Назва віджета, яку можна використовувати у меню адмін-частини
        замість використання назви класу.
        """
        self.inner_name = "Новини"
        """
        Мнемо-ім’я віджета. За умовчанням додається у стек widgets
        у якості назви основного віджета.
        """
        self.mnemo = self.__class__.__name__.lower()
        """
        У стек widgets додається мнемо-ім’я віджета у якості
        назви основного віджета.
        """
        self.widgets = [self.mnemo]
        super().__init__(section_class=section_class, parent=parent)

    def _load_default_forms(self):
        """
        За умовчанням у стек форми плагіна завантажується форма за умовч.
        """
        self._form_stack = self._default_form_class

    def _form(self, *args, **kw):
        """ Розгортання універсального ui для плагіна """
        f = None
        if self._form_stack:
            f = self._form_stack(*args, **kw)
        return f
        
    def get_news_categs(self):
        """
        Отримати категорії новин.
        Отримуємо лише ті категорії, що актуальні для розділів з новинами.
        """
        pagetype = Q(rootsdb.PageType)\
        .filter(rootsdb.PageType.mnemo == ls.newspage_pagetype_mnemo)\
        .first()
        
        self.categs = \
        Q(rootsdb.TreeNodeTag)\
        .filter(rootsdb.TreeNodeTag.nodes\
            .any(rootsdb.TreeNode.pagetype_id == pagetype.id))\
        .order_by(asc(rootsdb.TreeNodeTag.content))
        return self.categs
        
    def get_items(self):
        """ Отримати всі записи """
        self.items = Q(rootsdb.TreeNode).join(rootsdb.PageType)\
        .filter(rootsdb.PageType.mnemo == ls.newspage_pagetype_mnemo)
        return self.items

    def get_categ(self, stack, categ):
        """ отримати записи одної категорії """
        stack[categ.content] = \
            self.items\
                .filter(rootsdb.TreeNode.tags\
                    .any(rootsdb.TreeNodeTag.content == categ.content))\
                .order_by(desc(rootsdb.TreeNode.date_visible))

""" Реєстрація плагіна в загальному стеку """
admin.admin._plugins.append(News)
