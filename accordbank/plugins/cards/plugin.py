# -*- coding: utf-8 -*- 

"""
Плагін для взаємодії з типом контенту
"Картковий продукт" на стороні адмін-частини.
"""

import os

from sqlalchemy.sql.expression import asc,desc

#import transaction
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.plugins import Plugin

from accordbank.apps.cards import dbmodels as cardsdb

from .ui import forms

def langs_count():
    """ Function for counting langs """
    lc = 0
    try:
        lc = Plugin.count_langs(langs=Plugin.get_langs())
    except:
        pass
    return lc

class CardProducts(Plugin):
    """
    Основний клас плагіна, що забезпечує взаємодію з типом контенту "Картковий продукт"
    на боці адмін-частини.
    """
    packagename = admin.project.config.packagename
    template = 'plugins/cards/templates/cards_widget.pt'
    
    """ Перелік мов """
    langs = Plugin.get_langs()

    """ Кількість мов """
    langs_count = langs_count()
    
    _default_class = cardsdb.CardProduct

    # форма для роботи з даними карткового продукту
    _default_form_class = forms.CardProductsFormInit(forms.modify_form(langs_count))
    
    items = cardsdb.CardProduct
    categs = cardsdb.CardProductCateg
    
    def __init__(self, section_class=None, parent=None):
        """
        Назва віджета, яку можна використовувати у меню адмін-частини
        замість використання назви класу.
        """
        self.inner_name = "Карткові продукти"

        """
        Мнемо-ім’я віджета. За умовчанням додається у стек widgets
        у якості назви основного віджета.
        """
        self.mnemo = self.__class__.__name__.lower()

        """
        У стек widgets додається мнемо-ім’я віджета у якості
        назви основного віджета.
        """
        self.widgets = [self.mnemo]

        super().__init__(section_class=section_class, parent=parent)

    def _load_default_forms(self):
        """ Завантаження дефолтної форми для взаємодії з даними """
        self._form_stack = self._default_form_class

    def _form(self, *args, **kw):
        """ Розгортання універсального ui для плагіна """
        f = None
        if self._form_stack:
            f = self._form_stack(*args, **kw)
        return f
        
    def get_categs_list(self):
        """
        Отримати список категорій.
        На боці шаблона кожна категорія розгорне перелік 
        включених в неї продуктів.
        """
        categs = Q(self.categs)\
            .filter(self.categs.active == True)\
            .order_by(asc(self.categs.inner_name))
        return categs

""" Реєстрація плагіна в загальному стеку """
admin.admin._plugins.append(CardProducts)
