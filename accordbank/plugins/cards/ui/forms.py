# -*- coding: utf-8 -*- 

""" Форми для плагіна "Картковий продукт" """
import os
import transaction

from wtforms import widgets
from wtforms import validators
from wtforms import form
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from wtforms.fields import (
    SelectMultipleField,
    SelectField,
    FormField,
    FieldList,
    FileField,
    HiddenField
    )

from wtforms_alchemy import ModelForm

from pyramid_cms.assets.db.sqlalchemy.connection import DBSession, Q
from pyramid_cms.assets.forms import UIForm
from pyramid_cms.assets.forms import FormInit
from pyramid_cms.assets.forms import ProtectedForm
from pyramid_cms.apps.roots import dbmodels as rootsdb

from accordbank.apps.cards import dbmodels as cardsdb


class CardProductTitleForm(ModelForm):
    """
    Форма для додавання заголовків карткового продукту
    відповідно до кількості вказаних мов.
    """
    class Meta:
        model = cardsdb.CardProductTitle
        exclude = ['active']
    lang_id = HiddenField()

class CardProductAnnotationForm(ModelForm):
    """
    Форма для додавання анотацій карткового продукту
    відповідно до кількості вказаних мов.
    """
    class Meta:
        model = cardsdb.CardProductAnnotation
        exclude = ['active']
    lang_id = HiddenField()
    
class CardProductPromoAnnotationForm(ModelForm):
    """
    Форма для додавання анотацій карткового продукту(для промо-віджета)
    відповідно до кількості вказаних мов.
    """
    class Meta:
        model = cardsdb.CardProductPromoAnnotation
        exclude = ['active']
    lang_id = HiddenField()
    
class CardProductUsefulInfoForm(ModelForm):
    """
    Форма для додавання корисної інфи про картковий продукт
    відповідно до кількості вказаних мов.
    """
    class Meta:
        model = cardsdb.CardProductUsefulInfo
        exclude = ['active']
    lang_id = HiddenField()

class CardProductTextForm(ModelForm):
    """
    Форма для додавання текстів карткового продукту
    відповідно до кількості вказаних мов.
    """
    class Meta:
        model = cardsdb.CardProductText
        exclude = ['active']
    lang_id = HiddenField()
    
    
class CardProductsForm(UIForm):
    """
    Форма для роботи з картковими продуктами.
    """
    exclude = (
                'id',
                'created',
                'updated',
                'site_id',
                'node_id',
                'site',
                'node',
                'icon',
                )
    errors_tracker = None
    class Meta:
        model = cardsdb.CardProduct
        
    site = QuerySelectField('Сайт')
    node = QuerySelectField('Розділ у дереві сайту')
    icon = FileField()
    
    def add_obj_categs(self, obj, req):
        """ Додавання категорій до створеного карткового продукту """
        try:
            obj.categs.append(Q(cardsdb.CardProductCateg).get(req.GET.get('s',None)))
        except:
            pass
    
    def validate_and_add(self, req=None):
        """ Валідація і додавання запису """
        columns = [c for c in self.Meta.model.columns_names()
                   if c not in self.exclude]
        if self.validate():
            try:
                obj = cardsdb.CardProduct()
                obj.titles = []
                obj.annots = []
                obj.promo_annots = []
                obj.useful_infos = []
                obj.texts = []
                obj.categs = []
                for attr in columns:
                    """ Спочатку додаємо усі прості колонки """
                    setattr(obj,attr, getattr(self, attr).data)
                for t in self.titles:
                    # titles
                    title = cardsdb.CardProductTitle()
                    title.lang_id = t.lang_id.data
                    title.content = t.content.data
                    obj.titles.append(title)
                for a in self.annots:
                    # annots
                    annot = cardsdb.CardProductAnnotation()
                    annot.lang_id = a.lang_id.data
                    annot.content = a.content.data
                    obj.annots.append(annot)
                for pa in self.promo_annots:
                    # annots (promo)
                    promo_annot = cardsdb.CardProductPromoAnnotation()
                    promo_annot.lang_id = pa.lang_id.data
                    promo_annot.content = pa.content.data
                    obj.promo_annots.append(promo_annot)
                for ui in self.useful_infos:
                    # useful infos
                    useful_info = cardsdb.CardProductUsefulInfo()
                    useful_info.lang_id = ui.lang_id.data
                    useful_info.content = ui.content.data
                    obj.useful_infos.append(useful_info)
                for tx in self.texts:
                    # texts
                    text = cardsdb.CardProductText()
                    text.lang_id = tx.lang_id.data
                    text.content = tx.content.data
                    obj.texts.append(text)
                obj.site = self.site.data
                obj.node = self.node.data
                try:
                    obj.icon = self.icon.data.__dict__
                except:
                    pass
                """ Додаємо категорії, якщо отримали request з віджета """
                if req:
                    self.add_obj_categs(obj, req)
                obj.save()
                return True
            except Exception as e:
                self.errors_tracker = e
                return False
        return False
        
    def validate_and_edit(self, req=None):
        """ Валідація і оновлення запису про депозит """
        columns = [c for c in self.Meta.model.columns_names()
                if c not in self.exclude]
        
        if self.validate():
            try:
                obj = self._obj
                for attr in columns:
                    """ Спочатку додаємо усі прості колонки """
                    setattr(obj, attr, getattr(self, attr).data)
            
                for t in self.titles:
                    # titles
                    title = obj.titles\
                                .filter_by(lang_id=t.lang_id.data)\
                                .first()
                    if not title:
                        title = cardsdb.CardProductTitle()
                    title.lang_id = t.lang_id.data
                    title.content = t.content.data
                    obj.titles.append(title)
            
                for a in self.annots:
                    # annots
                    annot = obj.annots\
                                .filter_by(lang_id=a.lang_id.data)\
                                .first()
                    if not annot:
                        annot = cardsdb.CardProductAnnotation()
                    annot.lang_id = a.lang_id.data
                    annot.content = a.content.data
                    obj.annots.append(annot)
                    
                for pa in self.promo_annots:
                    # annots (promo)
                    promo_annot = obj.promo_annots\
                                .filter_by(lang_id=pa.lang_id.data)\
                                .first()
                    if not promo_annot:
                        promo_annot = cardsdb.CardProductPromoAnnotation()
                    promo_annot.lang_id = pa.lang_id.data
                    promo_annot.content = pa.content.data
                    obj.promo_annots.append(promo_annot)

                for ui in self.useful_infos:
                    # useful infos
                    useful_info = obj.useful_infos\
                                .filter_by(lang_id=ui.lang_id.data)\
                                .first()
                    if not useful_info:
                        useful_info = cardsdb.CardProductUsefulInfo()
                    useful_info.lang_id = ui.lang_id.data
                    useful_info.content = ui.content.data
                    obj.useful_infos.append(useful_info)
                    
                for tx in self.texts:
                    # texts
                    text = obj.texts\
                                .filter_by(lang_id=tx.lang_id.data)\
                                .first()
                    if not text:
                        text = cardsdb.CardProductText()
                    text.lang_id = tx.lang_id.data
                    text.content = tx.content.data
                    obj.texts.append(text)
            
                obj.site = self.site.data
                obj.node = self.node.data
                try:
                    obj.icon = self.icon.data.__dict__
                except:
                    pass
                obj.save()
                return True
            except Exception as e:
                self.errors_tracker = e
                return False
        return False

def modify_form(entries):
    """
    Додавання до форми комплекних полів у кількості, 
    визначеній змінною entries
    """
    class_obj = CardProductsForm
    """ додавання поля titles """
    setattr(class_obj,
            'titles',
            FieldList(FormField(CardProductTitleForm),
                      min_entries=entries))
    """ додавання поля annots """
    setattr(class_obj,
            'annots',
            FieldList(FormField(CardProductAnnotationForm),
                      min_entries=entries))
    """ додавання поля promo_annots (для промо-віджета) """
    setattr(class_obj,
            'promo_annots',
            FieldList(FormField(CardProductPromoAnnotationForm),
                      min_entries=entries))
    """ додавання поля useful_infos """
    setattr(class_obj,
            'useful_infos',
            FieldList(FormField(CardProductUsefulInfoForm),
                      min_entries=entries))
    """ додавання поля texts """
    setattr(class_obj,
            'texts',
            FieldList(FormField(CardProductTextForm),
                      min_entries=entries))
    return class_obj

class CardProductsFormInit(FormInit):
    """ Ініціалізатор для форми депозиту """
    def set_form_vars(self):
        """ Встановлення дефолтних та ін. значень для полів форми """
        self.f.site.query = Q(rootsdb.Site).all()
        self.f.node.query = Q(rootsdb.TreeNode).all()
