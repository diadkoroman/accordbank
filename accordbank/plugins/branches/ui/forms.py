# -*- coding: utf-8 -*-
""" Форми для плагіна "Відділення і банкомати" """
# import os
import time
# import transaction

# from wtforms import widgets
# from wtforms import validators
# from wtforms import form
from wtforms.ext.sqlalchemy.fields import (
    QuerySelectField,
    QuerySelectMultipleField
)
from wtforms.fields import (
    SelectMultipleField,
    SelectField,
    FormField,
    FieldList,
    FileField,
    HiddenField
    )

from wtforms_alchemy import ModelForm

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy.connection import DBSession, Q
from pyramid_cms.assets.forms import UIForm
from pyramid_cms.assets.forms import FormInit
from pyramid_cms.assets.forms import ProtectedForm
from pyramid_cms.apps.roots import dbmodels as rootsdb

from accordbank.apps.branches import dbmodels as brndb
from accordbank.apps.locations import dbmodels as locsdb
from accordbank.plugins.branches import local_settings as ls


class BranchNameForm(ModelForm):
    """
    Форма для додавання назв відділення
    відповідно до кількості вказаних мов.
    """
    class Meta:
        model = brndb.BranchName
        exclude = ['active']
    lang_id = HiddenField()


class BranchAddressNameForm(ModelForm):
    """
    Форма для додавання адрес відділення
    відповідно до кількості вказаних мов.
    """
    class Meta:
        model = brndb.BranchAddressName
        exclude = ['active']
    lang_id = HiddenField()


class BranchWorktimeNameForm(ModelForm):
    """
    Форма для додавання роб. годин відділення
    відповідно до кількості вказаних мов.
    """
    class Meta:
        model = brndb.BranchWorktimeName
        exclude = ['active']
    lang_id = HiddenField()


class BranchNoteNameForm(ModelForm):
    """
    Форма для додавання приміток до відділення
    відповідно до кількості вказаних мов.
    """
    class Meta:
        model = brndb.BranchNoteName
        exclude = ['active']
    lang_id = HiddenField()


class BranchesForm(UIForm):
    """
    Форма для роботи з відділеннями.
    """
    exclude = (
        'id',
        'created',
        'updated',
        'node_id',
        'branchtype_id',
        'city_id'
        )
    errors_tracker = None

    class Meta:
        model = brndb.Branch
    city = QuerySelectField()
    branchtype = QuerySelectField()
    services = QuerySelectMultipleField()
    properties = QuerySelectMultipleField()

    def create_branch_node(self, **kw):
        """
        Створюємо ноду для відділення, яке створюється.
        1. отримуємо батьківський розділ
        2. отримуємо pagetype.id для нової ноди
        3. створюємо мнемо-назву для відділення (time)
        3. безпосередньо створення ноди з необхідними параметрами
        2. зберігання реквізитів(id) ноди для прикріплення відділення.
        """

        inner_name = kw.get('inner_name', None)

        # отримуємо батьківський розділ ("Відділення і банкомати")
        branches = \
            Q(rootsdb.TreeNode).join(rootsdb.PageType)\
            .filter(rootsdb.PageType.mnemo == ls.branchlist_pagetype_mnemo)\
            .first()

        # отримуємо pagetype.id
        pagetype_id = \
            Q(rootsdb.PageType)\
            .filter(rootsdb.PageType.mnemo == ls.branchinfo_pagetype_mnemo)\
            .first().id

        # створюємо мнемо-назву
        new_node_mnemo = str(int(time.time()))

        # створюємо ноду
        node = rootsdb.TreeNode()
        node.site_id = branches.site_id
        node.inner_name = inner_name
        node.mnemo = new_node_mnemo
        node.pagetype_id = pagetype_id
        node.parent_id = branches.id
        node.save()
        return new_node_mnemo

    def get_branch_coords(self, inner_address=None):
        """
        Функціонал для отримання координат точки
        """
        coords = {}
        # за умовчанням повертаємо нулі, щоб був коретний вивід
        coords['lat'] = 0
        coords['lng'] = 0
        try:
            from pggeo import Geocoder
            geo = \
            Geocoder(
                     request_address=inner_address,
                     proxy_address=admin.project.config.pggeo_proxy_addresses)
            geo.get_data(location=True)
            coords = geo.processed_data['location']
        except:
            """
            Поки що просто ставимо заглушку.
            Надалі можна відловлювати помилки і вставляти їх, наприклад,
            у UIForm.errors_tracker
            """
            pass
        return coords

    def validate_and_add(self, req=None):
        """ Валідація і додавання запису про депозит """
        columns = [c for c in self.Meta.model.columns_names()
                   if c not in self.exclude]
        if self.validate():
            try:
                obj = brndb.Branch()
                obj.names = []
                obj.addresses = []
                obj.worktimes = []
                obj.notes = []
                obj.services = []
                obj.properties = []
                for attr in columns:
                    """ Спочатку додаємо усі прості колонки """
                    setattr(obj, attr, getattr(self, attr).data)

                for n in self.names:
                    # names
                    name = brndb.BranchName()
                    name.lang_id = n.lang_id.data
                    name.content = n.content.data
                    obj.names.append(name)

                for a in self.addresses:
                    # addresses
                    addr = brndb.BranchAddressName()
                    addr.lang_id = a.lang_id.data
                    addr.content = a.content.data
                    obj.addresses.append(addr)

                for wt in self.worktimes:
                    # worktimes
                    wkrtime = brndb.BranchWorktimeName()
                    wkrtime.lang_id = wt.lang_id.data
                    wkrtime.content = wt.content.data
                    obj.worktimes.append(wkrtime)

                for nt in self.notes:
                    # notes
                    note = brndb.BranchNoteName()
                    note.lang_id = nt.lang_id.data
                    note.content = nt.content.data
                    obj.notes.append(note)
                obj.city = self.city.data
                obj.branchtype = self.branchtype.data
                obj.node = \
                    Q(rootsdb.TreeNode)\
                    .filter(
                        rootsdb.TreeNode.mnemo ==
                        self.create_branch_node(
                            inner_name=self.inner_name.data or
                            ls.branch_inner_name_default))\
                    .first()
                obj.services = self.services.data
                obj.properties = self.properties.data
                if (int(self.lat.data) == 0 or
                self.lat.data in (0, None, 'None')) \
                and (int(self.lng.data) == 0 or
                self.lng.data in (0, None, 'None')):
                    """
                    Отримуємо координати точки.
                    Якщо координат немає (у поля внесено 0) - відпрацьовує
                    метод, що отримує координати точки через геокодер
                    """
                    coords = self.get_branch_coords(
                        inner_address=self.inner_address.data)
                    obj.lat = coords['lat']
                    obj.lng = coords['lng']
                obj.save()
                return True
            except Exception as e:
                raise Exception(e)
                self.errors_tracker = e
                return False
        return False

    def validate_and_edit(self, req=None):
        """ Валідація і оновлення запису про відділення """
        columns = [c for c in self.Meta.model.columns_names()
                if c not in self.exclude]
        if self.validate():
            try:
                obj = self._obj
                for attr in columns:
                    """ Спочатку додаємо усі прості колонки """
                    setattr(obj, attr, getattr(self, attr).data)

                for n in self.names:
                    # names
                    name = \
                        obj.names.filter_by(lang_id=n.lang_id.data).first()
                    if not name:
                        name = brndb.BranchName()
                    name.lang_id = n.lang_id.data
                    name.content = n.content.data
                    obj.names.append(name)
            
                for a in self.addresses:
                    # addresses
                    addr = \
                        obj.addresses.filter_by(lang_id=a.lang_id.data).first()
                    if not addr:
                        addr = brndb.BranchAddressName()
                    addr.lang_id = a.lang_id.data
                    addr.content = a.content.data
                    obj.addresses.append(addr)
            
                for wt in self.worktimes:
                    # worktimes
                    wrktime = \
                        obj.worktimes.filter_by(lang_id=wt.lang_id.data)\
                        .first()
                    if not wrktime:
                        wrktime = brndb.BranchWorktimeName()
                    wrktime.lang_id = wt.lang_id.data
                    wrktime.content = wt.content.data
                    obj.worktimes.append(wrktime)
                    
                for nt in self.notes:
                    # notes
                    note = \
                        obj.notes.filter_by(lang_id=nt.lang_id.data).first()
                    if not note:
                        note = brndb.BranchNoteName()
                    note.lang_id = nt.lang_id.data
                    note.content = nt.content.data
                    obj.notes.append(note)
                obj.city = self.city.data
                obj.branchtype = self.branchtype.data
                obj.services = self.services.data
                obj.properties = self.properties.data
                if (int(self.lat.data) == 0 or
                self.lat.data in (0, None, 'None')) \
                and (int(self.lng.data) == 0 or
                self.lng.data in (0, None, 'None')):
                    """
                    Отримуємо координати точки.
                    Якщо координат немає (у поля внесено 0) - відпрацьовує
                    метод, що отримує координати точки через геокодер
                    """
                    coords = \
                        self.get_branch_coords(
                            inner_address=self.inner_address.data)
                    obj.lat = coords['lat']
                    obj.lng = coords['lng']
                obj.save()
                return True
            except Exception as e:
                self.errors_tracker = e
                return False
        return False


def modify_form(entries):
    """
    Додавання до форми комплекних полів у кількості,
    визначеній змінною entries
    """
    class_obj = BranchesForm

    """ додавання поля names """
    setattr(class_obj,
            'names',
            FieldList(FormField(BranchNameForm),
                      min_entries=entries))

    """ додавання поля addresses """
    setattr(class_obj,
            'addresses',
            FieldList(FormField(BranchAddressNameForm),
                      min_entries=entries))

    """ додавання поля worktimes """
    setattr(class_obj,
            'worktimes',
            FieldList(FormField(BranchWorktimeNameForm),
                      min_entries=entries))

    """ додавання поля notes """
    setattr(class_obj,
            'notes',
            FieldList(FormField(BranchNoteNameForm),
                      min_entries=entries))
    return class_obj


class BranchesFormInit(FormInit):
    """ Ініціалізатор для форми відділень """
    def set_form_vars(self):
        """ Встановлення дефолтних та ін. значень для полів форми """
        self.f.city.query = \
            Q(locsdb.City).filter(locsdb.City.active == True)
        self.f.branchtype.query = \
            Q(brndb.BranchType).filter(brndb.BranchType.active == True)
        self.f.services.query = \
            Q(brndb.BranchService).filter(brndb.BranchService.active == True)
        self.f.properties.query = \
            Q(brndb.BranchProperty).filter(brndb.BranchProperty.active == True)
