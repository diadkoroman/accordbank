# -*- coding: utf-8 -*-
""" ...multiple lined comment..."""
import os

from pyramid.httpexceptions import HTTPFound
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import DBSession, Q
from pyramid_cms.assets.widgets import ProjectWidget

class Branches(ProjectWidget):
    """ ...multiple lined comment..."""
    template_custom_path = 'plugins/branches/templates'
    
    def dispatch(self):
        """ Отримання даних для віджета """

        # у self.kw['current'] з в’юхи передається екземпляр поточного плагіна
        self.curr = self.kw['current']

        # Назва для сторінки
        self.wdata['body']['pagetitle'] = self.curr.inner_name

        # Мови для розгортання форми
        self.wdata['body']['langs'] = self.curr.langs

        """ Шлях до плагіна """
        self.wdata['body']['path'] = self.curr.get_plugin_path(self.req)
        
        """ Перелік відділень """
        self.wdata['body']['itemslist'] = self.curr.get_items()
        
    def _add_form_widget(self):
        """ Вивід віджета, призначеного для додавання елементів """
        if self.req.method == 'GET':
            return self.kw['current']\
                .ui(meta={'csrf_context':self.req.session},
                    req=self.req.GET)
        elif self.req.method == 'POST':
            return self.kw['current']\
                .ui(self.req.POST,
                    meta={'csrf_context':self.req.session},
                    req=self.req.GET)
        
    def _edit_form_widget(self, i):
        """ Вивід віджета, призначеного для редагування елементів """
        if self.req.method == 'GET':
            return self.kw['current']\
                        .ui(meta={'csrf_context':self.req.session},
                            obj=i,
                            req=self.req.GET)
        elif self.req.method == 'POST':
            return self.kw['current']\
                    .ui(self.req.POST,
                        meta={'csrf_context':self.req.session},
                        obj=i,
                        req=self.req.GET)
        
    def _delete_form_widget(self, i):
        """ Вивід віджета, призначеного для видалення елементів """
        delete = self.req.GET.get('delete', None)
        path = self.kw['node'].get_path()
        if delete and delete == 'y':
            if i:
                i.delete()
                raise HTTPFound(location=path)
        return {
                'name': getattr(i, 'inner_name', 'noname'),
                'back_url': path,
                'frw_url': '{0}?i={1}'.format(path, str(i.id))
                }
        
    def get(self):
        """
        Дії, що повинні буть виконані при запиті типу GET
        -------------------------------------------------
        - отримуємо ui з поточного плагіна і записуємо її дані в стек для виводу
        """
        self.wdata['body']['form'] = None
        #self.wdata['body']['test'] = self.wdata['body']['langs']

        if 'add' in self.req.GET or 'i' in self.req.GET or 'delete' in self.req.GET:
            try:
                item = int(self.req.GET.get('i'))
                i = self.curr.get(item)
            except:
                item = None
                i = None
            if i:
                if 'delete' in self.req.GET:
                    # форма для видалення елемента (тільки через GET)
                    form = self._delete_form_widget(i)
                else:
                    # форма для редагування елемента (GET)
                    form = self._edit_form_widget(i)
            else:
                # форма для додавання елемента (GET)
                form = self._add_form_widget()
            self.wdata['body']['form'] = form
        
    def post(self):
        """
        Дії, що повинні бути виконані при запиті типу POST
        --------------------------------------------------
        """
        self.wdata['body']['form'] = None
        self.wdata['body']['test'] = 'test'
        if 'add' in self.req.GET or 'i' in self.req.GET or 'delete' in self.req.GET:
            try:
                item = int(self.req.GET.get('i'))
                i = self.curr.get(item)
            except:
                item = None
                i = None
            self.wdata['body']['test'] = 'test2'
            if i:
                form = self._edit_form_widget(i)
                self.wdata['body']['test'] = 'test3'
            else:
                form = self._add_form_widget()
                self.wdata['body']['test'] = 'test4'
            if 'add' in self.req.GET:
                if form.validate_and_add(req=self.req):
                    """
                    Якщо форма валідується, це означає, що через неї
                    передано валідний файл. Отже, ми викликаємо функцію,
                    що опрацьовує клік на файл.
                    """
                    redir_to = self.curr.get_plugin_path(self.req)
                    raise HTTPFound(location=redir_to)
                    # self.dispatch()
                    # self.wdata['body']['test'] = 'FORM VALID'
                else:
                    self.wdata['body']['test'] = form.errors
            if 'i' in self.req.GET:
                self.wdata['body']['test'] = 'test5'
                if form.validate_and_edit(req=self.req):
                    self.wdata['body']['test'] = 'test6'
                    """
                    Якщо форма валідується, це означає, що через неї
                    передано валідний файл. Отже, ми викликаємо функцію,
                    що опрацьовує клік на файл.
                    """
                    redir_to = self.curr.get_plugin_path(self.req)
                    raise HTTPFound(location=redir_to)
                    # self.dispatch()
                    # self.wdata['body']['test'] = 'FORM VALID'
                self.wdata['body']['test'] = form.errors_tracker
            self.wdata['body']['form'] = form

admin.admin.widgets.append(Branches)
