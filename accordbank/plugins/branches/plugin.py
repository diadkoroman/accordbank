# -*- coding: utf-8 -*-
""" ...multiple lined comment..."""

from sqlalchemy.sql.expression import asc,desc
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.plugins import Plugin

from accordbank.apps.branches import dbmodels as brndb
from .ui import forms

def langs_count():
    """ Function for counting langs """
    lc = 0
    try:
        lc = Plugin.count_langs(langs=Plugin.get_langs())
    except:
        pass
    return lc

class Branches(Plugin):
    """
    Клас плагіна, що забезпечує роботу з типом контенту "Відділення"
    на боці адмін-частини.
    """
    packagename = admin.project.config.packagename
    template = 'plugins/branches/templates/branches_widget.pt'

    # перелік мов для віджета
    langs = Plugin.get_langs()

    # кількість мов для віджета
    langs_count =  langs_count()

    # кла моделі БД за умовчанням
    _default_class = brndb.Branch

    # клас форми за умовчанням
    _default_form_class = forms.BranchesFormInit(forms.modify_form(langs_count))

    def __init__(self, section_class=None, parent=None):
        """
        Назва віджета, яку можна використовувати у меню адмін-частини
        замість використання назви класу.
        """
        self.inner_name = "Відділення"

        """
        Мнемо-ім’я віджета. За умовчанням додається у стек widgets
        у якості назви основного віджета.
        """
        self.mnemo = self.__class__.__name__.lower()

        """
        У стек widgets додається мнемо-ім’я віджета у якості
        назви основного віджета.
        """
        self.widgets = [self.mnemo]

        super().__init__(section_class=section_class, parent=parent)

    def _load_default_forms(self):
        """
        За умовчанням у стек форми плагіна завантажується форма за умовч.
        """
        self._form_stack = self._default_form_class

    def _form(self, *args, **kw):
        """ Розгортання універсального ui для плагіна """
        f = None
        if self._form_stack:
            f = self._form_stack(*args, **kw)
        return f
        
    def get_items(self):
        """ Отримати перелік елементів """
        items = Q(self._default_class)\
            .filter(self._default_class.active == True)\
            .order_by(asc(self._default_class.rank))
        return items

""" Реєстрація плагіна в загальному стеку """
admin.admin._plugins.append(Branches)
