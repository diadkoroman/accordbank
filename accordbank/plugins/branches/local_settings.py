# -*- coding: utf-8 -*-

""" Local settings for this plugin """

branchlist_pagetype_mnemo = 'acb_brancheslistpage'
branchinfo_pagetype_mnemo = 'acb_branchinfopage'
branch_inner_name_default = 'Відділення/Банкомат'
