# -*- coding: utf-8 -*- 

"""
Форми для плагіна "Депозит"
"""
import os
import transaction

from wtforms import widgets
from wtforms import validators
from wtforms import form
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from wtforms.fields import (
    SelectMultipleField,
    SelectField,
    FormField,
    FieldList,
    FileField,
    HiddenField
    )

from wtforms_alchemy import ModelForm

from pyramid_cms.assets.db.sqlalchemy.connection import DBSession, Q
from pyramid_cms.assets.forms import UIForm
from pyramid_cms.assets.forms import FormInit
from pyramid_cms.assets.forms import ProtectedForm
from pyramid_cms.apps.roots import dbmodels as rootsdb

from accordbank.apps.deposits import dbmodels as deposdb
from accordbank.apps.currencies import dbmodels as currsdb

class DepositTitleForm(ModelForm):
    """
    Форма для додавання заголовків депозиту
    відповідно до кількості вказаних мов.
    """
    class Meta:
        model = deposdb.DepositTitle
        exclude = ['active']
    lang_id = HiddenField()

class DepositAnnotationForm(ModelForm):
    """
    Форма для додавання анотацій депозиту
    відповідно до кількості вказаних мов.
    """
    class Meta:
        model = deposdb.DepositAnnotation
        exclude = ['active']
    lang_id = HiddenField()
    
class DepositPromoAnnotationForm(ModelForm):
    """
    Форма для додавання анотацій депозиту(для промо-віджета)
    відповідно до кількості вказаних мов.
    """
    class Meta:
        model = deposdb.DepositPromoAnnotation
        exclude = ['active']
    lang_id = HiddenField()
    
class DepositUsefulInfoForm(ModelForm):
    """
    Форма для додавання корисної інфи про депозит
    відповідно до кількості вказаних мов.
    """
    class Meta:
        model = deposdb.DepositUsefulInfo
        exclude = ['active']
    lang_id = HiddenField()

class DepositTextForm(ModelForm):
    """
    Форма для додавання текстів депозиту
    відповідно до кількості вказаних мов.
    """
    class Meta:
        model = deposdb.DepositText
        exclude = ['active']
    lang_id = HiddenField()
    
class DepositTariffForm(form.Form):
    """
    Комплексне поле для внесення тарифної моделі
    депозиту
    """
    item_id = HiddenField()
    currency = QuerySelectField()
    term = QuerySelectField()
    rate = QuerySelectField()
    tax = QuerySelectField()
    ecrate = QuerySelectField()
    ipt = QuerySelectField()

class DepositsForm(UIForm):
    """
    Форма для роботи з депозитами.
    """
    exclude = (
                'id',
                'created',
                'updated',
                'site_id',
                'node_id',
                'site',
                'node',
                'icon',
                'minsums',
                'tariffs'
                )
    errors_tracker = None
    class Meta:
        model = deposdb.Deposit
        
    site = QuerySelectField('Сайт')
    node = QuerySelectField('Розділ у дереві сайту')
    minsums = QuerySelectMultipleField('Мін.суми/Валюти депозиту')
    pros = QuerySelectMultipleField('Корисні якості депозиту')
    icon = FileField()
    #ip_types = QuerySelectMultipleField('Типи виплати відсотків')
    #tariffs = FormField(DepositTariffForm)
    tariffs = QuerySelectMultipleField('Тарифні моделі')
    
    def add_obj_categs(self, obj, req):
        """ Додавання категорій до створеного дкаозиту """
        try:
            obj.categs.append(Q(deposdb.DepositCateg).get(req.GET.get('s',None)))
        except:
            pass
    
    def validate_and_add(self, req=None):
        """ Валідація і додавання запису про депозит """
        columns = [c for c in self.Meta.model.columns_names()
                   if not c in self.exclude]
        if self.validate():
            try:
                obj = deposdb.Deposit()
                obj.titles = []
                obj.annots = []
                obj.promo_annots = []
                obj.useful_infos = []
                obj.texts = []
                obj.minsums = []
                obj.pros = []
                obj.tariffs = []
                obj.categs = []
                for attr in columns:
                    """ Спочатку додаємо усі прості колонки """
                    setattr(obj,attr, getattr(self, attr).data)
                for t in self.titles:
                    # titles
                    title = deposdb.DepositTitle()
                    title.lang_id = t.lang_id.data
                    title.content = t.content.data
                    obj.titles.append(title)
                for a in self.annots:
                    # annots
                    annot = deposdb.DepositAnnotation()
                    annot.lang_id = a.lang_id.data
                    annot.content = a.content.data
                    obj.annots.append(annot)
                for pa in self.promo_annots:
                    # annots (promo)
                    promo_annot = deposdb.DepositPromoAnnotation()
                    promo_annot.lang_id = pa.lang_id.data
                    promo_annot.content = pa.content.data
                    obj.promo_annots.append(promo_annot)
                for ui in self.useful_infos:
                    # useful infos
                    useful_info = deposdb.DepositUsefulInfo()
                    useful_info.lang_id = ui.lang_id.data
                    useful_info.content = ui.content.data
                    obj.useful_infos.append(useful_info)
                for tx in self.texts:
                    # texts
                    text = deposdb.DepositText()
                    text.lang_id = tx.lang_id.data
                    text.content = tx.content.data
                    obj.texts.append(text)
                obj.site = self.site.data
                obj.node = self.node.data
                try:
                    obj.icon = self.icon.data.__dict__
                except:
                    pass
                obj.minsums = self.minsums.data
                obj.pros = self.pros.data
                obj.tariffs = self.tariffs.data
                """ Додаємо категорії, якщо отримали request з віджета """
                if req:
                    self.add_obj_categs(obj, req)
                obj.save()
                return True
            except Exception as e:
                self.errors_tracker = e
                return False
        return False
        
    def validate_and_edit(self, req=None):
        """ Валідація і оновлення запису про депозит """
        columns = [c for c in self.Meta.model.columns_names()
                if not c in self.exclude]
        
        if self.validate():
            try:
                obj = self._obj
                for attr in columns:
                    """ Спочатку додаємо усі прості колонки """
                    setattr(obj, attr, getattr(self, attr).data)
            
                for t in self.titles:
                    # titles
                    title = obj.titles\
                                .filter_by(lang_id=t.lang_id.data)\
                                .first()
                    if not title:
                        title = deposdb.DepositTitle()
                    title.lang_id = t.lang_id.data
                    title.content = t.content.data
                    obj.titles.append(title)
            
                for a in self.annots:
                    # annots
                    annot = obj.annots\
                                .filter_by(lang_id=a.lang_id.data)\
                                .first()
                    if not annot:
                        annot = deposdb.DepositAnnotation()
                    annot.lang_id = a.lang_id.data
                    annot.content = a.content.data
                    obj.annots.append(annot)
                    
                for pa in self.promo_annots:
                    # annots (promo)
                    promo_annot = obj.promo_annots\
                                .filter_by(lang_id=pa.lang_id.data)\
                                .first()
                    if not promo_annot:
                        promo_annot = deposdb.DepositPromoAnnotation()
                    promo_annot.lang_id = pa.lang_id.data
                    promo_annot.content = pa.content.data
                    obj.promo_annots.append(promo_annot)

                for ui in self.useful_infos:
                    # useful infos
                    useful_info = obj.useful_infos\
                                .filter_by(lang_id=ui.lang_id.data)\
                                .first()
                    if not useful_info:
                        useful_info = deposdb.DepositUsefulInfo()
                    useful_info.lang_id = ui.lang_id.data
                    useful_info.content = ui.content.data
                    obj.useful_infos.append(useful_info)
                    
                for tx in self.texts:
                    # texts
                    text = obj.texts\
                                .filter_by(lang_id=tx.lang_id.data)\
                                .first()
                    if not text:
                        text = deposdb.DepositText()
                    text.lang_id = tx.lang_id.data
                    text.content = tx.content.data
                    obj.texts.append(text)
            
                obj.site = self.site.data
                obj.node = self.node.data
                try:
                    obj.icon = self.icon.data.__dict__
                except:
                    pass
                obj.minsums = self.minsums.data
                obj.pros = self.pros.data
                obj.tariffs = self.tariffs.data
                obj.save()
                return True
            except Exception as e:
                self.errors_tracker = e
                return False
        return False

def modify_form(entries):
    """
    Додавання до форми комплекних полів у кількості, 
    визначеній змінною entries
    """
    class_obj = DepositsForm
    """ додавання поля titles """
    setattr(class_obj,
            'titles',
            FieldList(FormField(DepositTitleForm),
                      min_entries=entries))
    """ додавання поля annots """
    setattr(class_obj,
            'annots',
            FieldList(FormField(DepositAnnotationForm),
                      min_entries=entries))
    """ додавання поля promo_annots (для промо-віджета) """
    setattr(class_obj,
            'promo_annots',
            FieldList(FormField(DepositPromoAnnotationForm),
                      min_entries=entries))
    """ додавання поля useful_infos """
    setattr(class_obj,
            'useful_infos',
            FieldList(FormField(DepositUsefulInfoForm),
                      min_entries=entries))
    """ додавання поля texts """
    setattr(class_obj,
            'texts',
            FieldList(FormField(DepositTextForm),
                      min_entries=entries))
    return class_obj

class DepositsFormInit(FormInit):
    """ Ініціалізатор для форми депозиту """
    def set_form_vars(self):
        """ Встановлення дефолтних та ін. значень для полів форми """
        self.f.site.query = Q(rootsdb.Site).all()
        self.f.node.query = Q(rootsdb.TreeNode).all()
        self.f.minsums.query = Q(deposdb.DepositMinSum).all()
        self.f.pros.query = Q(deposdb.DepositPros).all()
        #self.f.ip_types.query = Q(deposdb.DepositInterestPaymentType)
        self.f.tariffs.query = Q(deposdb.DepositTariff).all()
