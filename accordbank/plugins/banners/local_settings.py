# -*- coding: utf-8 -*-

"""
Конфігураційний файл для плагіна "Банери"
"""

# Мнемо-ім’я плагіна
plugin_mnemo = 'banners'

# Директорія з шаблонами плагіна
plugin_templates_dir = 'templates'

# Відносний шлях до директорії з шаблонами плагіна
plugin_templates_path = \
    'plugins/{0}/{1}'.format(plugin_mnemo, plugin_templates_dir)

# Шлях до основного шаблона плагіна
plugin_template = \
    '{0}/{1}_widget.pt'.format(plugin_templates_path, plugin_mnemo)
    
"""
Назви додаткових шаблонів плагіна.
Використовується для внесення інформації про додаткові під-шаблони,
котрі використовуються для внутрішніх потреб самого плагіна
"""
plugin_subtemplates = \
    {
        'listing': 'bannerslist_widget.pt',
        'form': 'bannersform_widget.pt'
    }
