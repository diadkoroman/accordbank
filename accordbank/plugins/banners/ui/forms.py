# -*- coding: utf-8 -*-

from wtforms.ext.sqlalchemy.fields import (
    QuerySelectField,
    QuerySelectMultipleField
)
from wtforms.fields import (
    SelectMultipleField,
    SelectField,
    FormField,
    FieldList,
    FileField,
    HiddenField
    )

from wtforms_alchemy import ModelForm

from pyramid_cms.assets.db.sqlalchemy.connection import DBSession, Q
from pyramid_cms.assets.forms import UIForm
from pyramid_cms.assets.forms import FormInit
from pyramid_cms.assets.forms import ProtectedForm
from pyramid_cms.apps.roots import dbmodels as rootsdb

from pyramid_cms.apps.banners import dbmodels as banndb
from .. import local_settings


class BannerImageForm(ModelForm):
    """
    Форма для додавання назв відділення
    відповідно до кількості вказаних мов.
    """
    class Meta:
        model = banndb.BannerImage
        exclude = ['active']
    lang_id = HiddenField()
    imagefile = FileField()
    
class BannerForm(UIForm):
    exclude = (
        'id',
        'created',
        'updated',
        'slot_id',
        'categ_id'
        )
    errors_tracker = None

    class Meta:
        model = banndb.Banner
    slot = QuerySelectField()
    categ = QuerySelectField(allow_blank=True, blank_text='----')
    
    def validate_and_add(self, req=None):
        """ Валідація і додавання запису про депозит """
        columns = [c for c in self.Meta.model.columns_names()
                   if c not in self.exclude]
        if self.validate():
            obj = banndb.Banner()
            obj.images = []
            for attr in columns:
                """ Спочатку додаємо усі прості колонки """
                setattr(obj, attr, getattr(self, attr).data)

            for i in self.images:
                # images
                image = banndb.BannerImage()
                image.lang_id = i.lang_id.data
                try:
                    image.image = i.imagefile.data.__dict__
                except:
                    pass
                image.link = i.link.data
                obj.images.append(image)

            obj.slot = self.slot.data
            obj.categ = self.categ.data
            obj.save()
            return True
        return False

    def validate_and_edit(self, req=None):
        """ Валідація і оновлення запису про відділення """
        columns = [c for c in self.Meta.model.columns_names()
                if c not in self.exclude]
        if self.validate():
            obj = self._obj
            for attr in columns:
                """ Спочатку додаємо усі прості колонки """
                setattr(obj, attr, getattr(self, attr).data)

            for i in self.images:
                # images
                image = \
                    obj.images.filter_by(lang_id=i.lang_id.data).first()
                if not image:
                    image = banndb.BannerImage()
                image.lang_id = i.lang_id.data
                try:
                    image.image = i.imagefile.data.__dict__
                except:
                    pass
                obj.images.append(image)

            obj.slot = self.slot.data
            obj.categ = self.categ.data
            obj.save()
            return True
        return False
    
def modify_form(entries):
    """
    Додавання до форми комплекних полів у кількості,
    визначеній змінною entries
    """
    class_obj = BannerForm

    """ додавання поля images """
    setattr(class_obj,
            'images',
            FieldList(FormField(BannerImageForm),
                      min_entries=entries))
    return class_obj
    
class BannerFormInit(FormInit):
    """ Ініціалізатор для форми """
    def set_form_vars(self):
        """ Встановлення дефолтних та ін. значень для полів форми """
        self.f.slot.query = \
            Q(banndb.BannerSlot).filter(banndb.BannerSlot.active == True)
        self.f.categ.query = \
            Q(banndb.BannerCateg).filter(banndb.BannerCateg.active == True)
