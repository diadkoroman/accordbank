# -*- coding: utf-8 -*-

"""
Плагін, що відповідає за роботу з банерами на боці адмін-частини
"""

from sqlalchemy.sql.expression import asc,desc
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.plugins import Plugin

from pyramid_cms.apps.banners import dbmodels as banndb
from . import local_settings
from .ui import forms


def langs_count():
    """ Рахуємо кількість мов """
    lc = 0
    try:
        lc = Plugin.count_langs(langs=Plugin.get_langs())
    except:
        pass
    return lc


class Banners(Plugin):
    """
    Основний клас плагіна для роботи з банерами.
    """

    packagename = admin.project.config.packagename
    template = local_settings.plugin_template

    # перелік мов для віджета
    langs = Plugin.get_langs()

    # кількість мов для віджета
    langs_count =  langs_count()

    # кла моделі БД за умовчанням
    _default_class = banndb.Banner
    
    # клас банерного слоту
    _bslot_class = banndb.BannerSlot

    # клас форми за умовчанням
    _default_form_class = forms.BannerFormInit(forms.modify_form(langs_count))

    def __init__(self, section_class=None, parent=None):
        """
        Назва віджета, яку можна використовувати у меню адмін-частини
        замість використання назви класу.
        """
        self.inner_name = "Банери"

        """
        Мнемо-ім’я віджета. За умовчанням додається у стек widgets
        у якості назви основного віджета.
        """
        self.mnemo = self.__class__.__name__.lower()

        """
        У стек widgets додається мнемо-ім’я віджета у якості
        назви основного віджета.
        """
        self.widgets = [self.mnemo]

        super().__init__(section_class=section_class, parent=parent)
        
    def _load_default_forms(self):
        """
        За умовчанням у стек форми плагіна завантажується форма за умовч.
        """
        self._form_stack = self._default_form_class

    def _form(self, *args, **kw):
        """ Розгортання універсального ui для плагіна """
        f = None
        if self._form_stack:
            f = self._form_stack(*args, **kw)
        return f
        
    def get_slots(self):
        """
        Отримуємо перелік банерних слотів.
        Метод використовується при формуванні списку слотів на першій сторінці
        плагіна в адмін-частині
        """
        try:
            slots = Q(self._bslot_class).order_by(asc(self._bslot_class.rank))
            return slots
        except:
            return None

    def get_items(self):
        """ Отримати перелік елементів """
        items = Q(self._default_class)\
            .filter(self._default_class.active == True)\
            .order_by(asc(self._default_class.id))
        return items

""" Реєстрація плагіна в загальному стеку """
admin.admin._plugins.append(Banners)
