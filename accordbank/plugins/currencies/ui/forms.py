# -*- coding: utf-8 -*-

"""
Файл з формами для роботи плагіна "Курси валют".
"""

import csv
import os

# from wtforms import widgets
# from wtforms import validators
# from wtforms import form

from wtforms.fields import (
    # SelectMultipleField,
    # SelectField,
    FormField,
    FieldList,
    FileField,
    HiddenField
    )

from wtforms_alchemy import ModelForm

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy.connection import Q
from pyramid_cms.assets.forms import UIForm
from pyramid_cms.assets.forms import FormInit
# from pyramid_cms.assets.forms import ProtectedForm
from pyramid_cms.assets.handlers.csv import WTFormsCSVHandler as CSVFileHandler
# from pyramid_cms.apps.roots import dbmodels as rootsdb

from accordbank.apps.currencies import dbmodels as currsdb
from accordbank.assets.handlers import nbu


class CurrencyRateForm(ModelForm):
    """ Форма-рядок з курсами для окремої валюти """
    class Meta:
        model = currsdb.CurrencyRate
    currency_id = HiddenField()
    categ_id = HiddenField()


class CurrencyExchangeForm(UIForm):
    """ Форма для роботи з курсами валют """
    exclude = \
        (
            'id',
            'created',
            'updated',
            'currency_id',
            'categ_id',
            'site_id',
            'pur_rate',
            'sel_rate',
            'nbu_rate',
            'change_pur',
            'change_sel',
            'change_nbu',
            'aut_rate',
        )
    errors_tracker = None

    class Meta:
        model = currsdb.CurrencyRate

    branches_rates = FileField()
    cards_rates = FileField()

    def is_csv_file(self, obj):
        # check for .csv extension
        if os.path.splitext(obj)[1] == '.csv':
            return True
        return False

    def parse_csv(self, csvdata, csvcateg):
        categ_id = \
            Q(currsdb.CurrencyRateCateg)\
            .filter(currsdb.CurrencyRateCateg.mnemo == csvcateg)\
            .scalar().id
        # parse .csv file data
        filename = os.path.join(admin.project.config.tmpdir, 'currencies.csv')
        with open(filename, 'wb') as tempf:
            tempf.write(csvdata.read())
        with open(filename, 'r') as f:
            parser = csv.DictReader(f)

            for row in parser:
                currency_id = \
                    Q(currsdb.Currency)\
                    .filter(currsdb.Currency.mncode == row['mncode'].lower())\
                    .scalar().id
                rate = currsdb.CurrencyRate()
                rate.categ_id = categ_id
                rate.currency_id = currency_id
                if row.get('pur_rate', False):
                    rate.pur_rate = row['pur_rate'].replace(',', '.')
                if row.get('sel_rate', False):
                    rate.sel_rate = row['sel_rate'].replace(',', '.')
                if row.get('nbu_rate', False):
                    rate.nbu_rate = row['nbu_rate'].replace(',', '.')
                if not rate.nbu_rate:
                    dh = nbu.NBUSingleCurrencyDataHandler(current_valcode=row['mncode'])
                    data = dh.get_data()
                    rate.nbu_rate = data['rate'] 
                if row.get('aut_rate', False):
                    rate.aut_rate = row['aut_rate'].replace(',', '.')
                rate.save()
        os.remove(filename)

    def parse_csv2(self, data, csvcateg):
        categ_id = \
            Q(currsdb.CurrencyRateCateg)\
            .filter(currsdb.CurrencyRateCateg.mnemo == csvcateg)\
            .scalar().id
        handler = CSVFileHandler(input_data=data, reader='dict')
        read_data = handler.read_data()
        for row in read_data:
            currency_id = \
                Q(currsdb.Currency)\
                .filter(currsdb.Currency.mncode == row['mncode'].lower())\
                .scalar().id
            rate = currsdb.CurrencyRate()
            rate.categ_id = categ_id
            rate.currency_id = currency_id
            if row.get('pur_rate', False):
                rate.pur_rate = row['pur_rate'].replace(',', '.')
            if row.get('sel_rate', False):
                rate.sel_rate = row['sel_rate'].replace(',', '.')
            if row.get('nbu_rate', False):
                rate.nbu_rate = row['nbu_rate'].replace(',', '.')
            if not rate.nbu_rate:
                try:
                    dh = nbu.NBUSingleCurrencyDataHandler(current_valcode=row['mncode'])
                    data = dh.get_data()
                    rate.nbu_rate = data['rate']
                except:
                    pass
            if row.get('aut_rate', False):
                rate.aut_rate = row['aut_rate'].replace(',', '.')
            rate.save()
            
    def parse_fields(self, rates):

        for rate in rates:
            r = currsdb.CurrencyRate()
            r.categ_id = rate['categ_id']
            r.currency_id = rate['currency_id']
            if rate.get('pur_rate', False):
                r.pur_rate = rate['pur_rate']
            if rate.get('sel_rate', False):
                r.sel_rate = rate['sel_rate']
            try:
                mncode = \
                    Q(currsdb.Currency)\
                    .get(rate['currency_id'])\
                    .mncode
                dh = nbu.NBUSingleCurrencyDataHandler(current_valcode=mncode)
                data = dh.get_data()
                r.nbu_rate = data['rate']
            except Exception as e:
                raise e
            if rate.get('aut_rate', False):
                r.aut_rate = rate['aut_rate']
            r.save()
        return True

    def validate_and_add(self):
        """
        Оновлення курсів валют. Працює через метод validate_and_add,
        оскільки фактично редагування даних про валюту не відбувається  - 
        відбувається лише додавання нових рядів з оновленням ставок і дати.
        1. Пробуємо отримати дані з файлу branches_rates
            - якщо отримали - здійснюємо нонвлення курсів валют з файлу
            - якщо не отримали - здійснюємо оновлення курсів валют
            з полів
        """
        if self.validate():
            try:
                # Якщо отримали файл - пробуємо парсити
                self.branches_rates.data.__dict__
                # raise Exception(data)
                # if self.is_csv_file(data['filename']):
                #    self.parse_csv(data['file'], 'branches')
                self.parse_csv2(self.branches_rates.data, 'branches')
                
                # після того як розпарсили - припиняємо роботу
                return True
            except:
                pass

            try:
                # Якщо отримали файл - пробуємо парсити
                self.cards_rates.data.__dict__
                # if self.is_csv_file(data['filename']):
                #    self.parse_csv(data['file'], 'cards')
                self.parse_csv2(self.cards_rates.data, 'cards')
                # після того як розпарсили - припиняємо роботу
                return True
            except:
                pass
                
            """
            Якщо до цього моменту метод нічого не повернув - значить, файлів
            не було завантажено.Тому приступаємо до варіанту 2 оновлення даних - 
            оновлення з вводу у поля.
            Для цього проходимо по кожному полю form.rates і записуємо дані,
            одночасно оновлюючи дату оновлення.
            """
            return self.parse_fields(self.rates.data)


def modify_form(entries):
    """
    Додавання до форми комплекних полів у кількості,
    визначеній змінною entries
    """
    class_obj = CurrencyExchangeForm

    """ додавання поля rate """
    setattr(class_obj,
            'rates',
            FieldList(FormField(CurrencyRateForm),
                      min_entries=entries))
    return class_obj


class CurrencyExchangeFormInit(FormInit):
    """ Ініціаізатор форми """
    def set_form_vars(self): pass
