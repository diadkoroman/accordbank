# -*- coding: utf-8 -*-

"""
Плагін для взаємодії з типом контенту "Валюта"
на боці адмін-частини.
"""

import os

from sqlalchemy.sql.expression import asc,desc

#import transaction
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.plugins import Plugin

from accordbank.apps.currencies import dbmodels as currsdb


from .ui import forms

def get_currencies():
    try:
        return Q(currsdb.Currency)\
        .filter(currsdb.Currency.active == True)
    except:
        return 0

def currencies_count(currencies):
    """
    Функція, що генерує список доступних валют.
    """
    c = 0
    try:
        c = \
        currencies.count()
    except:
        pass
    return c

# from .ui import forms

def langs_count():
    """ Function for counting langs """
    lc = 0
    try:
        lc = Plugin.count_langs(langs=Plugin.get_langs())
    except:
        pass
    return lc

class Currencies(Plugin):
    packagename = admin.project.config.packagename
    template = 'plugins/currencies/templates/currencies_widget.pt'
    
    # Currencies
    currencies = get_currencies()

    """ Кількість валют """
    currencies_count = currencies_count(currencies)

    _default_class = currsdb.Currency
    # клас категорій(типів) курсів валют
    _categs_class = currsdb.CurrencyRateCateg
    
    _rates_class = currsdb.CurrencyRate

    # форма для роботи з курсами валют
    _default_form_class = forms.CurrencyExchangeFormInit(forms.modify_form(currencies_count))

    
    """ Перелік мов """
    langs = Plugin.get_langs()

    """ Кількість мов """
    langs_count = 3

    _default_class = currsdb.Currency

    
    # форма для завантаження курсів валют з файлу
    _upload_form_class = None
    

    def __init__(self, section_class=None, parent=None):
        """
        Назва віджета, яку можна використовувати у меню адмін-частини
        замість використання назви класу.
        """
        self.inner_name = "Валюти"

        """
        Мнемо-ім’я віджета. За умовчанням додається у стек widgets
        у якості назви основного віджета.
        """
        self.mnemo = self.__class__.__name__.lower()

        """
        У стек widgets додається мнемо-ім’я віджета у якості
        назви основного віджета.
        """
        self.widgets = [self.mnemo]

        super().__init__(section_class=section_class, parent=parent)

    def _load_default_forms(self):
        """
        За умовчанням у стек форми плагіна завантажується форма для 
        роботи з даними депозитів.
        """
        self._form_stack = self._default_form_class

    def _form(self, *args, **kw):
        """ Розгортання універсального ui для плагіна """
        f = None
        if self._form_stack:
            f = self._form_stack(*args, **kw)
        return f

    @classmethod  
    def get_currencies(cls):
        try:
            return Q(currsdb.Currency)\
            .filter(currsdb.Currency.active == True)
        except:
            return 0

    @classmethod    
    def get_categs(cls):
        """ Отримуємо доступні типи курсів валют """
        categs = \
        Q(cls._categs_class)\
            .filter(cls._categs_class.active == True)\
            .order_by(asc(cls._categs_class.inner_name))
        return categs

""" Реєстрація плагіна в загальному стеку """
admin.admin._plugins.append(Currencies)
