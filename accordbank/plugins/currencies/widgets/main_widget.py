# -*- coding: utf-8 -*-
import os

from pyramid.httpexceptions import HTTPFound
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import DBSession, Q
from pyramid_cms.assets.widgets import ProjectWidget

class Currencies(ProjectWidget):
    """
    Головний віджет для роботи з валютами.
    По ідеї повинен могти відпрацьовувати додатковий функціонал:
    1. завантаження курсів валют з файлу
    """
    template_custom_path = 'plugins/currencies/templates'

    # встановлюємо змінні для wdata['body']
    wdata_body='main_form', 'add_currency_form', 'edit_currency_form', 'categs'
    
    def dispatch(self):
        """ Диспечер даних для віджета """

        # поточний плагін для віджета
        self.curr = self.kw['current']

        # назва, що відобажатиметься на сторінці як назва розділу
        self.wdata['body']['pagetitle'] = self.curr.inner_name

    def get(self):
        """ Обробка запиту GET """
        # отримуємо типи курсів валют
        self.wdata['body']['categs'] = self.curr.get_categs()
        self.wdata['body']['currencies'] = self.curr.get_currencies()
        self.wdata['body']['lastupdate'] = self.curr._rates_class.get_lastupdate_data()

        if 'add' in self.req.GET or 'i' in self.req.GET or 'delete' in self.req.GET:
            try:
                item = int(self.req.GET.get('i'))
                i = self.curr.get(item)
            except:
                item = None
                i = None
            if i:
                form = self.kw['current']\
                    .ui(meta={'csrf_context':self.req.session},
                        obj=i,
                        req=self.req.GET)
                self.wdata['body']['edit_currency_form'] = form
            else:
                form = self.kw['current']\
                    .ui(meta={'csrf_context':self.req.session},
                        req=self.req.GET)
                self.wdata['body']['add_currency_form'] = form
        else:
            form = self.kw['current']\
                .ui(meta={'csrf_context':self.req.session},
                    req=self.req.GET)
            self.wdata['body']['main_form'] = form
            
    def post(self):
        """ POST request processing """
        # отримуємо типи курсів валют
        self.wdata['body']['categs'] = self.curr.get_categs()
        self.wdata['body']['currencies'] = self.curr.get_currencies()
        self.wdata['body']['lastupdate'] = self.curr._default_class.get_lastupdate_data()

        form = self.kw['current']\
            .ui(self.req.POST, meta={'csrf_context':self.req.session},
            req=self.req.GET)
        if form.validate_and_add():
            """
            Якщо форма валідується, це означає, що через неї
            передано валідний файл. Отже, ми викликаємо функцію,
            що опрацьовує клік на файл.
            """
            redir_to = self.curr.get_plugin_path(self.req)
            raise HTTPFound(location=redir_to)
        else:
            self.wdata['body']['test'] = form.errors_tracker
        self.wdata['body']['main_form'] = form

admin.admin.widgets.append(Currencies)
