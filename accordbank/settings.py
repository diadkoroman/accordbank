# -*- coding: utf-8 -*-
"""
Project settings file.
"""
import os
from pyramid_cms import admin
admin.project.config.deflang = 'ua'
admin.project.config.packagename = 'accordbank'
admin.project.config.verbose_name = \
    {
        'ua': 'Акордбанк',
        'en': 'Акордбанк',
        'ru': 'Акордбанк'
    }
admin.project.config.project_id = 1
admin.project.config.basedir = os.path.dirname(__file__)
admin.project.config.files = admin.project.config.basedir + '/files'
admin.project.config.css = admin.project.config.basedir + '/static/css'
admin.project.config.jss = admin.project.config.basedir + '/static/jss'
admin.project.config.images = admin.project.config.files + '/images'
admin.project.config.templates = [
    admin.project.config.basedir + '/templates',
    admin.project.config.basedir + '/templates/components',
    admin.project.config.basedir + '/templates/pages',
]
admin.project.config.templates_default_dir = 2
admin.project.config.tmpdir = admin.project.config.basedir + '/tmp'

# Перезаписуємо значення директорії для запису файлів
admin.rewrite_setting('config', 'files', admin.project.config.files)
# Перезаписуємо шлях для запису файлів css
admin.rewrite_setting('config', 'css', admin.project.config.css)
# Перезаписуємо шлях для запису файлів js
admin.rewrite_setting('config', 'jss', admin.project.config.jss)

"""
Переписуємо налаштування дефолтної директорії для розміщення
viewtypes і стек директорій шаблонів.
"""
#admin.rewrite_setting('config','templates_default_dir', 2)
#admin.rewrite_setting('config','templates', admin.project.config.templates)

"""
Реквізити проксі-адрес для геокодера (pggeo.Geocoder)
Для прямих з’єднань лишаємо порожній dict
"""
admin.project.config.pggeo_proxy_addresses = \
    {
        #'http':'192.168.50.9:8080',
        #'https':'192.168.50.9:8080'
    }

"""
АДРЕСИ СТОРІНОК API НАЦІОНАЛЬНОГО БАНКУ ДЛЯ ОТРИМАННЯ ДАНИХ
"""
# Токен для розпізнавання параметра конфігурації (список валют)
admin.project.config.nbuapi_currs_token = 'nbuapi_currs_'
# Токен для розпізнавання параметра конфігурації (окрема валюта)
admin.project.config.nbuapi_currency_token = 'nbuapi_currency_'

# Курси валют НБУ(формат XML)
admin.project.config.nbuapi_currs_xml = \
    'http://bank.gov.ua/NBUStatService/v1/statdirectory/exchange'

# Курси валют НБУ (формат JSON)
admin.project.config.nbuapi_currs_json = \
    'http://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json'

"""
Курси валюти на певну дату отримуються за допомогою урлів, у які підставляються
необхідні значення. Тому урл в конфігу має значення рядка під форматування, де:

{0} - буквений код валюти реєстронезалежний
(напр. USD або usd, EUR або eur, UAH або uah тощо) 

{1} - дата у форматі YYYYMMDD 
(напр. 8 червня 2016 року повинно мати формат 20160608)
"""
# Курс НБУ для окремої валюти на певну дату (формат XML)
admin.project.config.nbuapi_currency_xml = \
'http://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode={0}&date={1}'

# Курс НБУ для окремої валюти на певну дату (формат JSON)
admin.project.config.nbuapi_currency_json = \
'http://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode={0}&date={1}&json'

# Робочий поштовий сервер для надсилання пошти з сайту
admin.project.config.smtpserver = 'smtp.gmail.com'
admin.project.config.smtpport = 587
admin.project.config.mailheader = 'AccordBank WebPage Mail Service'
admin.project.config.smtpuser = 'pjsc.accordbank@gmail.com'
admin.project.config.smtppassw = 'pjsc.accordbank1975'
admin.project.config.from_adress = admin.project.config.smtpuser
