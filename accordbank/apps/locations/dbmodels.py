# -*- coding: utf-8 -*-
"""
Все для розгортання географічний прив’язок:
моделі для розгортання списку країн, міст
"""

# SQLALCHEMY IMPORTS
from sqlalchemy import (
    Table,
    Column,
    Numeric,
    ForeignKey,
    Integer,
    String,
    Text
    )
from sqlalchemy.orm import relationship, backref
from sqlalchemy.sql.functions import func

from wtforms.fields import FileField
from wtforms import widgets
from wtforms import validators
from wtforms.ext.sqlalchemy.fields import (
    QuerySelectField,
    QuerySelectMultipleField
)

# APP IMPORTS
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy.connection import Base, Q, Metadata
from pyramid_cms.assets.db.sqlalchemy import fields
from pyramid_cms.assets.db.sqlalchemy.models import mixins
from pyramid_cms.assets.db.sqlalchemy.models import tables
from pyramid_cms.assets.translations import _db_ts as _
from pyramid_cms.apps.roots import dbmodels as rootsdb


class Country(mixins.RankedColumnsMixin, Base):
    """   модель "Країна"   """
    __tablename__ = tables.tablename_('countries')
    __descr__ = _('Countries model')
    verbose_name = _('Country')
    verbose_name_plural = _('Countries')

    def __str__(self):
        return self.inner_name

    names = relationship('CountryName',
                         cascade='all, delete-orphan',
                         lazy='dynamic')

    def localized(self, lang_id):
        """ Локалізована у певну мову назва """
        try:
            locale = self.names\
                .filter_by(lang_id=lang_id)\
                .first()
            if locale is not None and locale.content:
                return locale.content
            return self.inner_name
        except:
            return self.inner_name

admin.admin.content_types.append(Country)


class CountryName(mixins.StringSlot, Base):
    """ Назва точки(лок.) """
    __tablename__ = tables.tablename_('country_names')
    __descr__ = _('Localized country names')
    verbose_name = _('Country Name')
    verbose_name_plural = _('Country Names')

    def __str__(self):
        return self.content

    # мовна локалізація
    lang_id = Column(Integer, ForeignKey(tables.fk_('langs')))
    # single line comment...
    item_id = Column(Integer,
                     ForeignKey(tables.fk_('countries')))

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'lang_id',
                       'item_id'
                       )
            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(Country)
        f.lang_id.query = Q(rootsdb.Language)
        return f

admin.admin.content_types.append(CountryName)


class City(mixins.RankedColumnsMixin, Base):
    """   модель "Місто"   """
    __tablename__ = tables.tablename_('cities')
    __descr__ = _('Cities')
    verbose_name = _('City')
    verbose_name_plural = _('Cities')

    def __str__(self):
        return '{0} ({1})'.format(self.inner_name,self.country.inner_name)

    country_id = Column(Integer, ForeignKey(tables.fk_('countries')))
    country = relationship('Country',
                           backref=backref(
                                          'cities',
                                          cascade='all, delete-orphan',
                                          lazy='dynamic'),
                           uselist=False)
    names = relationship('CityName',
                         cascade='all, delete-orphan',
                         lazy='dynamic')

    def localized(self, lang_id):
        """ Локалізована у певну мову назва """
        try:
            locale = self.names\
                .filter_by(lang_id=lang_id)\
                .first()
            if locale is not None and locale.content:
                return locale.content
            return self.inner_name
        except:
            return self.inner_name

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'country_id'
                       )
            class Meta:
                model = cls
            country = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.country.data:
                        obj.country = self.country.data
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.country.data:
                        obj.country = self.country.data
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.country.query = Q(Country)
        return f

admin.admin.content_types.append(City)


class CityName(mixins.StringSlot, Base):
    """ Назва точки(лок.) """
    __tablename__ = tables.tablename_('city_names')
    __descr__ = _('Localized city names')
    verbose_name = _('City Name')
    verbose_name_plural = _('City Names')

    def __str__(self):
        return self.content

    # мовна локалізація
    lang_id = Column(Integer, ForeignKey(tables.fk_('langs')))
    # single line comment...
    item_id = Column(Integer,
                     ForeignKey(tables.fk_('cities')))

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'lang_id',
                       'item_id'
                       )
            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(City)
        f.lang_id.query = Q(rootsdb.Language)
        return f

admin.admin.content_types.append(CityName)















