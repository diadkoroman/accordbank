# -*- coding: utf-8 -*-

"""
Module documentation here...
"""
import json

# SQLALCHEMY IMPORTS
from sqlalchemy import (
    Table,
    Column,
    Numeric,
    ForeignKey,
    Integer,
    String,
    Text,
    Boolean
    )
from sqlalchemy.orm import relationship, backref

from wtforms.ext.sqlalchemy.fields import (
    QuerySelectField,
    QuerySelectMultipleField
)

# APP IMPORTS
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy.connection import Base, Q, Metadata
from pyramid_cms.assets.db.sqlalchemy.models import mixins
from pyramid_cms.assets.db.sqlalchemy.models import tables
from pyramid_cms.assets.translations import _db_ts as _

from pyramid_cms.apps.roots import dbmodels as rootsdb
from accordbank.apps.locations import dbmodels as locdb


'''
'''
branches_services = Table(
    tables.tablename_('branches_services'),
    Metadata,
    Column('branch_id', Integer, ForeignKey(tables.fk_('branches'))),
    Column('service_id', Integer, ForeignKey(tables.fk_('branch_services')))
)

branches_properties = Table(
    tables.tablename_('branches_properties'),
    Metadata,
    Column('branch_id', Integer, ForeignKey(tables.fk_('branches'))),
    Column('property_id', Integer, ForeignKey(tables.fk_('branchprops')))
)


class BranchType(mixins.StandardColumnsMixin, Base):
    """ Типи точок """
    __tablename__ = tables.tablename_('branchtypes')
    __descr__ = _('Types of branches')
    verbose_name = _('Branch type')
    verbose_name_plural = _('Branch types')

    def __str__(self):
        return self.inner_name

    names = relationship('BranchTypeName',
                         cascade='all, delete-orphan',
                         lazy='dynamic')

    def localized(self, lang_id):
        """ Локалізована у певну мову назва """
        try:
            locale = self.names\
                .filter_by(lang_id=lang_id)\
                .first()
            if locale is not None and locale.content:
                return locale.content
            return self.inner_name
        except:
            return self.inner_name

admin.admin.content_types.append(BranchType)


class BranchTypeName(mixins.StringSlot, Base):
    """ Назва точки(лок.) """
    __tablename__ = tables.tablename_('branchtype_names')
    __descr__ = _('Localized branchtype names')
    verbose_name = _('BranchType Name')
    verbose_name_plural = _('BranchType Names')

    def __str__(self):
        return self.content

    # мовна локалізація
    lang_id = Column(Integer, ForeignKey(tables.fk_('langs')))
    # single line comment...
    item_id = Column(Integer,
                     ForeignKey(tables.fk_('branchtypes')))

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'lang_id',
                       'item_id'
                       )

            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj, attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj, attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(BranchType)
        f.lang_id.query = Q(rootsdb.Language)
        return f

admin.admin.content_types.append(BranchTypeName)


class BranchProperty(mixins.RankedColumnsMixin, Base):
    """ ...multiple lined comment..."""
    __tablename__ = tables.tablename_('branchprops')
    __descr__ = _('Branch specific properties')
    verbose_name = _('Branch specific property')
    verbose_name_plural = _('Branch specific properties')

    def __str__(self):
        return self.inner_name

    mnemo = Column(String(150), nullable=True)

    names = relationship('BranchPropertyName',
                         cascade='all, delete-orphan',
                         lazy='dynamic')
    in_widget = Column(Boolean, default=True)

    def localized(self, lang_id):
        """ Локалізована у певну мову назва """
        try:
            locale = self.names\
                .filter_by(lang_id=lang_id)\
                .first()
            if locale is not None and locale.content not in ('', None):
                return locale.content
            return self.inner_name
        except:
            return self.inner_name

admin.admin.content_types.append(BranchProperty)


class BranchPropertyName(mixins.StringSlot, Base):
    """
    Додаткові властивості точки/бакномата.
    ПРикріплюється до точки/банкомата відношенням "багато-до-багатьох"
    (Branch.properties)
    """
    __tablename__ = tables.tablename_('branchprops_names')
    __descr__ = _('Localized branch property names')
    verbose_name = _('Localized branch property name')
    verbose_name_plural = _('Localized branch property names')

    def __str__(self):
        return self.content

    # мовна локалізація
    lang_id = Column(Integer, ForeignKey(tables.fk_('langs')))
    # single line comment...
    item_id = Column(Integer,
                     ForeignKey(tables.fk_('branchprops')))

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'lang_id',
                       'item_id'
                       )

            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj, attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj, attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(BranchProperty)
        f.lang_id.query = Q(rootsdb.Language)
        return f

admin.admin.content_types.append(BranchPropertyName)


class BranchName(mixins.StringSlot, Base):
    """ Назва точки(лок.) """
    __tablename__ = tables.tablename_('branch_names')
    __descr__ = _('Localized branch names')
    verbose_name = _('Branch Name')
    verbose_name_plural = _('Branch Names')

    def __str__(self):
        return self.content

    # мовна локалізація
    lang_id = Column(Integer, ForeignKey(tables.fk_('langs')))
    # single line comment...
    item_id = Column(Integer,
                     ForeignKey(tables.fk_('branches')))

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'lang_id',
                       'item_id'
                       )

            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj, attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj, attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(Branch)
        f.lang_id.query = Q(rootsdb.Language)
        return f

admin.admin.content_types.append(BranchName)


class BranchAddressName(mixins.StringSlot, Base):
    """ Адреса точки(лок.) """
    __tablename__ = tables.tablename_('branchaddress_names')
    __descr__ = _('Localized branch addresses')
    verbose_name = _('Branch Address')
    verbose_name_plural = _('Branch Addresses')

    def __str__(self):
        return self.content

    # мовна локалізація
    lang_id = Column(Integer, ForeignKey(tables.fk_('langs')))
    item_id = Column(Integer,
                     ForeignKey(tables.fk_('branches')))

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'lang_id',
                       'item_id'
                       )

            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj, attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj, attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(Branch)
        f.lang_id.query = Q(rootsdb.Language)
        return f

admin.admin.content_types.append(BranchAddressName)


class BranchWorktimeName(mixins.StringSlot, Base):
    """ Робочі години для точки(лок.) """
    __tablename__ = tables.tablename_('branchwtime_names')
    __descr__ = _('Localized branch worktimes')
    verbose_name = _('Branch Worktime')
    verbose_name_plural = _('Branch Worktimes')

    def __str__(self):
        return self.content

    # мовна локалізація
    lang_id = Column(Integer, ForeignKey(tables.fk_('langs')))
    item_id = Column(Integer,
                     ForeignKey(tables.fk_('branches')))

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'lang_id',
                       'item_id'
                       )

            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj, attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj, attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(Branch)
        f.lang_id.query = Q(rootsdb.Language)
        return f

admin.admin.content_types.append(BranchWorktimeName)


class BranchNoteName(mixins.TextSlot, Base):
    """ Додаткові примітки для точки(лок.) """
    __tablename__ = tables.tablename_('branchnote_names')
    __descr__ = _('Localized additional notes for branch')
    verbose_name = _('Branch Note')
    verbose_name_plural = _('Branch Notes')

    def __str__(self):
        if len(self.content) > 0:
            return self.content[:20]
        return 'Parent item id: {0}'.format(str(self.item_id))

    # мовна локалізація
    lang_id = Column(Integer, ForeignKey(tables.fk_('langs')))
    item_id = Column(Integer,
                     ForeignKey(tables.fk_('branches')))

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'lang_id',
                       'item_id'
                       )

            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj, attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj, attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(Branch)
        f.lang_id.query = Q(rootsdb.Language)
        return f

admin.admin.content_types.append(BranchNoteName)


class Branch(mixins.RankedColumnsMixin, Base):
    """ multiline comment... """
    __tablename__ = tables.tablename_('branches')
    __descr__ = _('Branch elements')
    verbose_name = _('Branch')
    verbose_name_plural = _('Branches')

    def __str__(self):
        return self.inner_name

    city_id = Column(Integer, ForeignKey(tables.fk_('cities')))
    city = relationship('City', uselist=False)

    """
    Прив’язка до ноди.
    Вважаю, що буде правильно оформити кожен запис про відділення/банкомат
    як розділ дерева сайту з парентом /branches
    """
    node_id = Column(Integer, ForeignKey(tables.fk_('tree_nodes')))
    branchtype_id = Column(Integer, ForeignKey(tables.fk_('branchtypes')))
    inner_address = Column(String(150), nullable=True)
    inner_worktime = Column(Text, nullable=True)
    # номери телефонів відділення
    telns = Column(String(200), nullable=True)
    # додаткові примітки(умовч.)
    inner_note = Column(Text, nullable=True)
    # географічна широта
    lat = Column(Numeric(precision=12, scale=9), default=000.000000000)
    # географічна довгота
    lng = Column(Numeric(precision=12, scale=9), default=000.000000000)

    node = relationship('TreeNode', backref=backref('branch', uselist=False), uselist=False)
    branchtype = relationship('BranchType', uselist=False)
    names = relationship('BranchName',
                         cascade='all, delete-orphan',
                         lazy='dynamic')
    addresses = relationship('BranchAddressName',
                             cascade='all, delete-orphan',
                             lazy='dynamic')
    worktimes = relationship('BranchWorktimeName',
                             cascade='all, delete-orphan',
                             lazy='dynamic')
    notes = relationship('BranchNoteName',
                         cascade='all, delete-orphan',
                         lazy='dynamic')
    services = relationship('BranchService',
                            secondary=branches_services,
                            lazy='dynamic')
    properties = relationship('BranchProperty',
                              secondary=branches_properties,
                              lazy='dynamic')

    def name_localized(self, lang_id):
        """ Локалізована у певну мову назва """
        try:
            locale = self.names\
                .filter_by(lang_id=lang_id)\
                .first()
            if locale is not None and locale.content:
                return locale.content
            return self.inner_name or self.node.inner_name
        except:
            return self.inner_name or self.node.inner_name

    def address_localized(self, lang_id):
        """ Локалізована у певну мову адреса """
        try:
            locale = self.addresses\
                .filter_by(lang_id=lang_id)\
                .first()
            if locale is not None and locale.content:
                return locale.content
            return self.inner_address
        except:
            return self.inner_address

    def worktime_localized(self, lang_id):
        """ Локалізований у певну мову режим роботи """
        try:
            locale = self.worktimes\
                .filter_by(lang_id=lang_id)\
                .first()
            if locale is not None and locale.content:
                return locale.content
            return self.inner_worktime
        except:
            return self.inner_worktime

    def note_localized(self, lang_id):
        """ Локалізована у певну мову примітка """
        try:
            locale = self.notes\
                .filter_by(lang_id=lang_id)\
                .first()
            if locale is not None and locale.content:
                return locale.content
            return self.inner_note
        except:
            return self.inner_note

    def services_localized(self, lang_id):
        """ Перелік послуг, котрі надає відділення, у потрібній локалі """
        services = []
        for srv in self.services.filter_by(active=True).order_by('inner_name'):
            services.append(srv.localized(lang_id))
        return services

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'node_id',
                       'node',
                       'branchtype_id',
                       'city_id',
                       'services',
                       )

            class Meta:
                model = cls
            node = QuerySelectField()
            branchtype_id = QuerySelectField()
            city = QuerySelectField()
            services = QuerySelectMultipleField()
            properties = QuerySelectMultipleField()

            def validate_and_add(self):
                # add new instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj, attr, getattr(self, attr).data)
                    obj.node = self.node.data
                    obj.branchtype_id = self.branchtype_id.data.id
                    obj.city = self.city.data
                    obj.services = self.services.data
                    obj.properties = self.properties.data
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        setattr(obj, attr, getattr(self, attr).data)
                    obj.node = self.node.data
                    obj.branchtype_id = self.branchtype_id.data.id
                    obj.city = self.city.data
                    obj.services = self.services.data
                    obj.properties = self.properties.data
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.node.query = Q(rootsdb.TreeNode)
        f.branchtype_id.query = Q(BranchType)
        f.city.query = Q(locdb.City)
        f.services.query = Q(BranchService)
        f.properties.query = Q(BranchProperty)
        return f

    @staticmethod
    def get_branches_json(itemslist, lang_id):
        """ ...multiple lined comment..."""
        branches = []
        for item in itemslist:
            iteminfo = {
                'branchtype_id': item.branchtype_id,
                'city_id': item.city_id,
                'address': item.address_localized(lang_id),
                'worktime': item.worktime_localized(lang_id),
                'telns': item.telns,
                'services': item.services_localized(lang_id),
                'properties': [
                                p.mnemo for p in item.properties
                                .filter_by(active=True)
                            ]
            }
            i = {
                'coords': {'lat': float(item.lat), 'lng': float(item.lng)},
                'title': item.name_localized(lang_id),
                'info': iteminfo,
            }
            branches.append(i)
        return json.dumps(branches)

    def get_branchinfo(self, lang_id):
        """
        Інформація про відділення
        """
        iteminfo = \
            {
                'branchtype_id': self.branchtype_id,
                'city_id': self.city_id,
                'address': self.address_localized(lang_id),
                'worktime': self.worktime_localized(lang_id),
                'telns': self.telns,
                'services': self.services_localized(lang_id),
                'properties': \
                    [
                        p.mnemo for p in self.properties
                        .filter_by(active=True)
                    ],
                'note': self.note_localized(lang_id),
            }
        i = \
            {
                'coords': json.dumps({'lat': float(self.lat), 'lng': float(self.lng)}),
                'title': self.name_localized(lang_id),
                'info': iteminfo,
            }
        return i


admin.admin.content_types.append(Branch)


class BranchServiceName(mixins.StringSlot, Base):
    """ Назви послуг(лок.) """
    __tablename__ = tables.tablename_('branchsrv_names')
    __descr__ = _('Localized names for services provided by branches')
    verbose_name = _('Branch Service Name')
    verbose_name_plural = _('Branch Servive Names')

    def __str__(self):
        return self.content

    # мовна локалізація
    lang_id = Column(Integer, ForeignKey(tables.fk_('langs')))
    item_id = Column(Integer,
                     ForeignKey(tables.fk_('branch_services')))

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'lang_id',
                       'item_id'
                       )

            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj, attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        setattr(obj, attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(BranchService)
        f.lang_id.query = Q(rootsdb.Language)
        return f

admin.admin.content_types.append(BranchServiceName)


class BranchService(mixins.RankedColumnsMixin, Base):
    """ Послуги точок """
    __tablename__ = tables.tablename_('branch_services')
    __descr__ = _('Services provided by branches')
    verbose_name = _('Service')
    verbose_name_plural = _('Services')
    names = relationship('BranchServiceName',
                         cascade='all, delete-orphan',
                         passive_deletes=True,
                         lazy='dynamic')

    def localized(self, lang_id):
        """ Локалізована у певну мову назва """
        try:
            locale = self.names\
                .filter_by(lang_id=lang_id)\
                .first()
            if locale is not None:
                return locale.content
            return self.inner_name
        except:
            return self.inner_name

admin.admin.content_types.append(BranchService)
