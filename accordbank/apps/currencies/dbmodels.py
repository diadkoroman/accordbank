# -*- coding: utf-8 -*-

# SQLALCHEMY IMPORTS
from sqlalchemy import (
        Table,
        Column,
        Boolean,
        Numeric,
        ForeignKey,
        Integer,
        String,
        Text,
        Numeric,
        )
from sqlalchemy.orm import relationship, backref
from sqlalchemy.sql.functions import func

from wtforms.fields import FileField
from wtforms import widgets
from wtforms import validators
from wtforms.ext.sqlalchemy.fields import (
    QuerySelectField,
    QuerySelectMultipleField
)

# APP IMPORTS
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy.connection import Base, Q
from pyramid_cms.assets.db.sqlalchemy import fields
from pyramid_cms.assets.db.sqlalchemy.models import mixins
from pyramid_cms.assets.db.sqlalchemy.models import tables
from pyramid_cms.assets.translations import _db_ts as _

from pyramid_cms.apps.roots import dbmodels as rootsdb





'''
КЛАСИ ТАБЛИЦЬ, ЩО ВИКОРИСТОВУЮТЬСЯ У РОБОТІ МОДУЛЯ "ВАЛЮТИ"
'''

class CurrencyRateCategName(mixins.StringSlot, Base):
    """ Мовна локаль для назви категорії """
    __tablename__ = tables.tablename_('crcateg_names')
    __descr__ = _('Локалі для типів курсів валют')
    verbose_name = _('Rate Categ Locale')
    verbose_name_plural = _('Rate Categ Locales')

    def __str__(self):
        return self.content
        
    # мовна локалізація
    lang_id = Column(Integer,ForeignKey(tables.fk_('langs')))
    # тип ставки
    categ_id = Column(Integer, ForeignKey(tables.fk_('currate_categs')))
    lang = relationship('Language',uselist = False)
    categ = relationship('CurrencyRateCateg', uselist=False)
    
    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'lang_id',
                       'categ_id',
                       )
            class Meta:
                model = cls
            lang = QuerySelectField()
            categ = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.lang.data:
                        obj.lang = self.lang.data
                    if self.categ.data:
                        obj.categ = self.categ.data
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.lang.data:
                        obj.lang = self.lang.data
                    if self.categ.data:
                        obj.categ = self.categ.data
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.lang.query = Q(rootsdb.Language)
        f.categ.query = Q(CurrencyRateCateg)
        return f

admin.admin.content_types.append(CurrencyRateCategName)


class CurrencyRateCateg(mixins.RankedColumnsMixin, Base):
    """
    Категорія(тип) ставки курсу валюти.
    Кастомний інтерфейс форми не потрібен, оскільки не маємо тут
    ключів на інші моделі
    """
    __tablename__ = tables.tablename_('currate_categs')
    __descr__ = _('Типи курсів валют')
    verbose_name = _('Rate Categ')
    verbose_name_plural = _('Rate Categs')
    
    # мнемо-назва
    mnemo = Column(String(150), nullable=True)
    # чи використовується на першій сторінці у віджеті
    in_widget = Column(Boolean,default = False)
    # позиція на першій сторінці у віджеті (якщо use_for_indexpage=True)
    rank_widget = Column(Integer,default = 0)
    
    names = relationship('CurrencyRateCategName',
        cascade = 'all, delete-orphan',
        passive_deletes = True,
        lazy = 'dynamic',
        )

    def localized(self, lang_id):
        """ Метод, що генерує мовну локаль для об’єкта """
        try:
            loc = self.names.filter_by(lang_id=lang_id).first()
            if loc:
                return loc.content
            return self.inner_name
        except:
            return self.inner_name

admin.admin.content_types.append(CurrencyRateCateg)


class Currency(mixins.RankedColumnsMixin, Base):
    '''
    Основний клас - валюта. До нього будуть причеплені через ключі усі інші
    властивості - назви у мовній локалі, курси внутрішні і курси НБУ тощо.
    '''
    __tablename__ = tables.tablename_('currencies') # fcbank_currencies
    __descr__ = _('Валюти')
    verbose_name = _('валюта')
    verbose_name_plural = _('валюти')
    
    def __str__(self):
        return '{0} ({1})'.format(self.inner_name, self.mncode.upper())
    
    # сайт, на якому використовується валюта
    site_id = Column(Integer,ForeignKey(tables.fk_('sites')))
    # код валюти (цифровий)
    code = Column(String(10))
    # мнемо-код (буквенне позначення валюти)
    mncode = Column(String(3))
    # за скільки одиниць валюти виставляється ціна в гривнях
    foritems = Column(Integer)
    # курс купівлі
    pur_rate = Column(Numeric(precision = 10,scale=4), default=0.0)
    # курс продажу
    sel_rate = Column(Numeric(precision = 10,scale=4), default=0.0)
    # курс НБУ
    nbu_rate = Column(Numeric(precision = 10,scale=4), default=0.0)
    '''
    зміни в курсі валюти (- чи +) з моменту останього оновлення
    change_pur - динаміка курсу купівлі
    change_sel - динаміка курсу продажу
    change_nbu - динаміка курсу НБУ
    '''
    change_pur = Column(Numeric(precision = 10,scale=4), default=0.0)
    change_sel = Column(Numeric(precision = 10,scale=4), default=0.0)
    change_nbu = Column(Numeric(precision = 10,scale=4), default=0.0)
    # чи використовується на першій сторінці у віджеті
    in_widget = Column(Boolean,default = False)
    # позиція на першій сторінці у віджеті (якщо use_for_indexpage=True)
    rank_widget = Column(Integer,default = 0)
    # чи виводити валюту у таблиці валют в розділі "Валюти"
    in_table = Column(Boolean,default = True)
    # № у списку для таблиць зі ставками депозитів
    rank_deptable = Column(Integer,default = 0)
    # посилання на сайт
    site = relationship('Site',backref = 'currencies',uselist = False)
    # назви для валюти у різних локалях
    names = relationship('CurrencyName',
        cascade = 'all, delete-orphan',
        passive_deletes = True,
        lazy = 'dynamic',
        )
    rates = relationship('CurrencyRate',
        cascade = 'all, delete-orphan',
        lazy = 'dynamic',
        )
    stats = relationship('CurrencyRateStat',
        cascade = 'all, delete-orphan',
        lazy = 'dynamic',
        )

    def localized(self, lang_id):
        try:
            loc = self.names.filter_by(lang_id=lang_id).first()
            if loc:
                return loc.content
            return self.inner_name
        except:
            return self.inner_name
            
    def get_lastrate_by_categ(self, categ):
        """ Отримує найсвіжішу ставку за категорією """
        rate = self.rates.filter_by(categ_id=categ.id).order_by('-created').first()
        return rate
    
    @classmethod
    def get_lastupdate_data(cls):
        try:
            return Q(func.max(cls.created))\
                .scalar().strftime('%d.%m.%Y')
        except:
            return None

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'site_id',
                       'site'
                       )
            class Meta:
                model = cls
            site = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    if Q(cls).filter(cls.mncode == self.mncode.data).count() == 0:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                        obj.site = self.site.data
                        obj.save(flush=True)
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    obj.site = self.site.data
                    obj.save(flush=True)
                    return True
                return False
        f = __F(*args, **kw)
        f.site.query = Q(rootsdb.Site)
        return f

admin.admin.content_types.append(Currency)


class CurrencyRate(mixins.LiteColumnsMixin, Base):
    """
    Сnавки валют винесено в окрему таблицю БД,
    тому що нам потрібно буде проводити класифікацію цих ставок
    (напр. Ставки у відділеннях, Ставки по картках тощо).
    Ставка посилається на валюту через ключ currency_id і повязана з нею
    залежністю currency.
    Ставка посилається на категорію через ключ categ_id і пов’язана з нею
    залежністю categ.
    """
    __tablename__ = tables.tablename_('curr_rates')
    __descr__ = _('Курси для валют(тільки значення)')
    verbose_name = _('курс валюти')
    verbose_name_plural = _('курси валют')
    
    def __str__(self):
        return self.currency.mncode
    
    currency_id = Column(Integer, ForeignKey(tables.fk_('currencies')))
    # rate category
    categ_id = Column(Integer, ForeignKey(tables.fk_('currate_categs')))
    # курс купівлі
    pur_rate = Column(Numeric(precision = 10,scale=4), default=0.0)
    # курс продажу
    sel_rate = Column(Numeric(precision = 10,scale=4), default=0.0)
    # курс НБУ
    nbu_rate = Column(Numeric(precision = 10,scale=4), default=0.0)
    # курс авторизацї(для платіжних карток)
    aut_rate = Column(Numeric(precision = 10,scale=4), default=0.0)
    # rel currency
    currency = relationship('Currency', uselist=False)
    # rel category
    categ = relationship('CurrencyRateCateg', uselist=False)

    @classmethod
    def get_lastupdate_data(cls):
        try:
            return Q(func.max(cls.created))\
                .scalar().strftime('%d.%m.%Y')
        except:
            return None

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'currency_id',
                       'categ_id',
                       )
            class Meta:
                model = cls
            currency = QuerySelectField()
            categ = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.currency.data:
                        obj.currency = self.currency.data
                    if self.categ.data:
                        obj.categ = self.categ.data
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.currency.data:
                        obj.currency = self.currency.data
                    if self.categ.data:
                        obj.categ = self.categ.data
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.currency.query = Q(Currency)
        f.categ.query = Q(CurrencyRateCateg)
        return f

admin.admin.content_types.append(CurrencyRate)

class CurrencyRateStat(mixins.LiteColumnsMixin, Base):
    '''
    Клас, що веде статистику по зміні курсу для кожної валюти.
    При кожному оновленні у моделі CurrencyItem створюється новий
    запис CurrencyItemRateStats
    '''
    __tablename__ = tables.tablename_('curr_stats')
    __descr__ = _('')
    verbose_name = _('зріз курсу валюти')
    verbose_name_plural = _('зрізи курсів валют')
    
    def __str__(self):
        return self.currency.mncode
        
    currency_id = Column(Integer, ForeignKey(tables.fk_('currencies')))
    # rate category
    categ_id = Column(Integer, ForeignKey(tables.fk_('currate_categs')))
    # курс купівлі
    pur_rate = Column(Numeric(precision = 10,scale=4))
    # курс продажу
    sel_rate = Column(Numeric(precision = 10,scale=4))
    # курс НБУ
    nbu_rate = Column(Numeric(precision = 10,scale=4))
    # rel currency
    currency = relationship('Currency', uselist=False)
    # rel category
    categ = relationship('CurrencyRateCateg', uselist=False)

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'currency_id',
                       'categ_id'
                       )
            class Meta:
                model = cls
            currency_id = QuerySelectField()
            categ_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.currency_id.data:
                        obj.currency_id = self.currency_id.data.id
                    if self.categ_id.data:
                        obj.categ_id = self.categ_id.data.id
                    obj.save(flush=True)
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.currency_id.data:
                        obj.currency_id = self.currency_id.data.id
                    if self.categ_id.data:
                        obj.categ_id = self.categ_id.data.id
                    obj.save(flush=True)
                    return True
                return False
        f = __F(*args, **kw)
        f.currency_id.query = Q(Currency)
        f.categ_id.query = Q(CurrencyRateCateg)
        return f

admin.admin.content_types.append(CurrencyRateStat)


class CurrencyName(mixins.StringSlot, Base):
    '''
    Назва валюти у мовній локалі.
    Валюта отримує свою назву через спеціальну властивість names, використовуючи
    спеціальний метод name_localized або name_content_localized
    '''
    __tablename__ = tables.tablename_('curr_names') # fcbank_curr_names
    __descr__ = _('Назви валют')
    verbose_name = _('назва валюти')
    verbose_name_plural = _('назви валют')
    
    def __str__(self):
        return self.content
        
    # мовна локалізація
    lang_id = Column(Integer,ForeignKey(tables.fk_('langs')))
    # валюта
    currency_id = Column(Integer, ForeignKey(tables.fk_('currencies')))
    lang = relationship('Language',uselist = False)
    
    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'currency_id',
                       'lang_id',
                       'lang'
                       )
            class Meta:
                model = cls
            currency_id = QuerySelectField()
            lang = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.currency_id.data:
                        obj.currency_id = self.currency_id.data.id
                    if self.lang.data:
                        obj.lang = self.lang.data
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.currency_id.data:
                        obj.currency_id = self.currency_id.data.id
                    if self.lang.data:
                        obj.lang = self.lang.data
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.currency_id.query = Q(Currency)
        f.lang.query = Q(rootsdb.Language)
        return f

admin.admin.content_types.append(CurrencyName)
