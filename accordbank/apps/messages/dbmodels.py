# -*- coding: utf-8 -*-
"""
Моделі для зберігання в БД повідомлень від користувачів
"""

# SQLALCHEMY IMPORTS
from sqlalchemy import (
    Table,
    Column,
    Numeric,
    ForeignKey,
    Integer,
    String,
    Text
    )
from sqlalchemy.orm import relationship, backref
from sqlalchemy.sql.functions import func

from wtforms.fields import FileField
from wtforms import widgets
from wtforms import validators
from wtforms.ext.sqlalchemy.fields import (
    QuerySelectField,
    QuerySelectMultipleField
)

# APP IMPORTS
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy.connection import Base, Q, Metadata
from pyramid_cms.assets.db.sqlalchemy import fields
from pyramid_cms.assets.db.sqlalchemy.models import mixins
from pyramid_cms.assets.db.sqlalchemy.models import tables
from pyramid_cms.assets.translations import _db_ts as _
from pyramid_cms.apps.roots import dbmodels as rootsdb

""" Many-to-Many between Message Topics and Message Topic Categories. """
mtopics_to_mtcategs = Table(
    tables.tablename_('mtopics_to_mtcategs'),
    Metadata,
    Column('mtopic_id',Integer,ForeignKey(tables.fk_('messagetopics'))),
    Column('mtcateg_id',Integer,ForeignKey(tables.fk_('message_topic_categs')))
)

class MessageTopicCateg(mixins.RankedColumnsMixin, Base):
    """
    Оскільки є кілька форм, що користуються моделлю
    Тема повідомлення, то вводимо поняття Категорія
    Теми повідомлення, пов’язане залежністю m2m із
    темами повідомлень
    """
    __tablename__ = tables.tablename_('message_topic_categs')
    __descr__ = _('Message Topic Category')
    verbose_name = _('Message Topic Category')
    verbose_name_plural = _('Message Topic Categories')

    def __str__(self):
        return self.inner_name
    mnemo = Column(String(150))
    
    def get_active_topics(self):
        """ Отримати перелік тем, що входять у категорію """
        return self.topics\
        .filter_by(active=True)\
        .order_by('rank')

admin.admin.content_types.append(MessageTopicCateg)

class MessageTopic(mixins.RankedColumnsMixin, Base):
    """   модель "Тема повідомлення"   """
    __tablename__ = tables.tablename_('messagetopics')
    __descr__ = _('Message Topic model')
    verbose_name = _('Message Topic')
    verbose_name_plural = _('Message Topics')

    def __str__(self):
        return self.inner_name
    related_emails = Column(Text, nullable=True)
    names = relationship('MessageTopicName',
                         cascade='all, delete-orphan',
                         lazy='dynamic')
    categories = relationship(
                              'MessageTopicCateg',
                              secondary=mtopics_to_mtcategs,
                              backref=backref('topics',lazy='dynamic'),
                              lazy='dynamic')

    def localized(self, lang_id):
        """ Локалізована у певну мову назва """
        try:
            locale = self.names\
                .filter_by(lang_id=lang_id)\
                .first()
            if locale is not None and locale.content:
                return locale.content
            return self.inner_name
        except:
            return self.inner_name

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'lang_id',
                       'item_id'
                       )
            class Meta:
                model = cls
            categories = QuerySelectMultipleField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.categories.data:
                        obj.categories = self.categories.data
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.categories.data:
                        obj.categories = self.categories.data
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.categories.query = Q(MessageTopicCateg)
        return f
admin.admin.content_types.append(MessageTopic)


class MessageTopicName(mixins.StringSlot, Base):
    """ Назва (лок.) """
    __tablename__ = tables.tablename_('messagetopic_names')
    __descr__ = _('Localized Message Topic names')
    verbose_name = _('Message Topic Name')
    verbose_name_plural = _('Message Topic Names')

    def __str__(self):
        return self.content

    # мовна локалізація
    lang_id = Column(Integer, ForeignKey(tables.fk_('langs')))
    # single line comment...
    item_id = Column(Integer,
                     ForeignKey(tables.fk_('messagetopics')))
    lang = relationship('Language', uselist=False)
    item = relationship('MessageTopic', uselist=False)

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'lang_id',
                       'item_id'
                       )
            class Meta:
                model = cls
            item = QuerySelectField()
            lang = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.item.data:
                        obj.item = self.item.data
                    if self.lang.data:
                        obj.lang = self.lang.data
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item = self.item.data
                    if self.lang.data:
                        obj.lang = self.lang.data
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item.query = Q(MessageTopic)
        f.lang.query = Q(rootsdb.Language)
        return f
admin.admin.content_types.append(MessageTopicName)


class Message(mixins.LiteColumnsMixin, Base):
    """ Модель "Повідомлення" """
    __tablename__ = tables.tablename_('messages')
    __descr__ = _('Message model')
    verbose_name = _('Message')
    verbose_name_plural = _('Messages')
    
    sender_name = Column(String(150))
    sender_teln = Column(String(150), nullable=True)
    sender_email = Column(String(150), nullable=True)
    topic_id = Column(Integer,
                     ForeignKey(tables.fk_('messagetopics')))
    content = Column(Text)
    topic = relationship('MessageTopic', uselist=False, backref='messages')
admin.admin.content_types.append(Message)
