# -*- coding: utf-8 -*-

# SQLALCHEMY IMPORTS
from sqlalchemy import (
        Table,
        Column,
        Date,
        Boolean,
        Numeric,
        ForeignKey,
        Integer,
        String,
        Text
        )
from sqlalchemy.orm import relationship, backref
from sqlalchemy.sql.expression import asc, desc
from sqlalchemy.sql.functions import func

from wtforms.fields import FileField
from wtforms import widgets
from wtforms import validators
from wtforms.ext.sqlalchemy.fields import (
    QuerySelectField,
    QuerySelectMultipleField
)

# APP IMPORTS
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy.connection import Base, Q, Metadata
from pyramid_cms.assets.db.sqlalchemy import fields
from pyramid_cms.assets.db.sqlalchemy.models import mixins
from pyramid_cms.assets.db.sqlalchemy.models import tables
from pyramid_cms.assets.translations import _db_ts as _

from pyramid_cms.apps.roots import dbmodels as rootsdb
###############################################################################

""" Many-to-Many between CardProduct and CardProductCateg. """
cardproducts_to_categs = Table(
    tables.tablename_('cardproducts_to_categs'),
    Metadata,
    Column('cardproduct_id',Integer,ForeignKey(tables.fk_('cardproducts'))),
    Column('categ_id',Integer,ForeignKey(tables.fk_('cardproduct_categs')))
)

###############################################################################
class CardProductCateg(mixins.StandardColumnsMixin, Base):
    """ Типи карткових продуктів """
    __tablename__ = tables.tablename_('cardproduct_categs')
    __descr__ = 'Типи карткових продуктів'
    verbose_name = 'категорія карткових продуктів'
    verbose_name_plural = 'категорії карткових продуктів'
    
    def __str__(self):
        return self.inner_name
        
    mnemo = Column('mnemo',String(100))

admin.admin.content_types.append(CardProductCateg)
###############################################################################


###############################################################################
class CardProductTitle(mixins.StringSlot, Base):
    __tablename__ = tables.tablename_('cardproduct_titles')
    __descr__ = 'Назви карткового продукту(локалі)'
    verbose_name = 'назва карткового продукту (локаль)'
    verbose_name_plural = 'назви карткового продукту (локалі)'
    
    def __str__(self):
        return self.content
        
    # мовна локалізація
    lang_id = Column(Integer,ForeignKey(tables.fk_('langs')))
    # дкартковий продукт
    item_id = Column(Integer,ForeignKey(tables.fk_('cardproducts')))
    item = relationship('CardProduct', uselist=False)

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'item_id',
                       'item',
                       'lang_id'
                       )
            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(CardProduct)
        f.lang_id.query = Q(rootsdb.Language)
        return f

admin.admin.content_types.append(CardProductTitle)
###############################################################################


###############################################################################
class CardProductText(mixins.TextSlot, Base):
    __tablename__ = tables.tablename_('cardproduct_texts')
    __descr__ = 'Тексти карткового продукту(локалі)'
    verbose_name = 'текст карткового продукту (локаль)'
    verbose_name_plural = 'тексти карткового продукту (локалі)'
    
    def __str__(self):
        return self.item.inner_name
        
    # мовна локалізація
    lang_id = Column(Integer,ForeignKey(tables.fk_('langs')))
    # картковий продукт
    item_id = Column(Integer,ForeignKey(tables.fk_('cardproducts')))
    item = relationship('CardProduct', uselist=False)

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'item_id',
                       'item',
                       'lang_id'
                       )
            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(CardProduct)
        f.lang_id.query = Q(rootsdb.Language)
        return f

admin.admin.content_types.append(CardProductText)
###############################################################################


###############################################################################
class CardProductAnnotation(mixins.TextSlot, Base):
    __tablename__ = tables.tablename_('cardproduct_annots')
    __descr__ = 'Анотації карткового продукту(локалі)'
    verbose_name = 'анотація карткового продукту (локаль)'
    verbose_name_plural = 'анотації карткового продукту (локалі)'
    
    def __str__(self):
        return self.item.inner_name
        
    # мовна локалізація
    lang_id = Column(Integer,ForeignKey(tables.fk_('langs')))
    # картковий продукт
    item_id = Column(Integer,ForeignKey(tables.fk_('cardproducts')))
    item = relationship('CardProduct', uselist=False)

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'item_id',
                       'item',
                       'lang_id'
                       )
            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(CardProduct)
        f.lang_id.query = Q(rootsdb.Language)
        return f

admin.admin.content_types.append(CardProductAnnotation)
###############################################################################

###############################################################################
class CardProductPromoAnnotation(mixins.TextSlot, Base):
    __tablename__ = tables.tablename_('cardproduct_promo_annots') 
    __descr__ = 'Анотації карткового продукту(локалі) для промо-віджета'
    verbose_name = 'анотація карткового продукту(локаль)для промо-віджета'
    verbose_name_plural = 'анотації карткового продукту(локалі) для промо-віджета'
    
    def __str__(self):
        return self.item.inner_name
        
    # мовна локалізація
    lang_id = Column(Integer,ForeignKey(tables.fk_('langs')))
    # картковий продукт
    item_id = Column(Integer,ForeignKey(tables.fk_('cardproducts')))
    item = relationship('CardProduct', uselist=False)

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'item_id',
                       'item',
                       'lang_id'
                       )
            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(CardProduct)
        f.lang_id.query = Q(rootsdb.Language)
        return f

admin.admin.content_types.append(CardProductPromoAnnotation)
###############################################################################


###############################################################################
class CardProductUsefulInfo(mixins.TextSlot, Base):
    __tablename__ = tables.tablename_('cardproduct_useful_infos') 
    __descr__ = 'Корисна інформація про картковий продукт'
    verbose_name = 'Корисна інформація про картковий продукт (локаль)'
    verbose_name_plural = 'Корисна інформація про картковий продукт (локалі)'
    
    def __str__(self):
        return self.item.inner_name
        
    # мовна локалізація
    lang_id = Column(Integer,ForeignKey(tables.fk_('langs')))
    # картковий продукт
    item_id = Column(Integer,ForeignKey(tables.fk_('cardproducts')))
    item = relationship('CardProduct', uselist=False)

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'item_id',
                       'item',
                       'lang_id'
                       )
            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = \
                    [
                        c for c in cls.columns_names() 
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = \
                    [
                        c for c in cls.columns_names() 
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(CardProduct)
        f.lang_id.query = Q(rootsdb.Language)
        return f

admin.admin.content_types.append(CardProductUsefulInfo)
###############################################################################


###############################################################################
class CardProduct(mixins.RankedColumnsMixin, Base):
    __tablename__ = tables.tablename_('cardproducts')
    __descr__ = 'Карткові продукти'
    verbose_name = 'Картковий продукт'
    verbose_name_plural = 'Карткові продукти'
    
    def __str__(self):
        return self.inner_name
    
    # сайт, на якому використовується картковий продукт
    site_id = Column(Integer,ForeignKey(tables.fk_('sites')))
    """ Нода для карткового продукту в дереві сайту """
    node_id = Column(Integer,ForeignKey(tables.fk_('tree_nodes')))
    mnemo = Column('mnemo',String(150))
    icon = Column(fields.UploadFileField(
        200,
        mimetypes = admin.admin.config.imgs_mime,
        upload_to= 'images'),
        nullable = True
    )
    in_promo = Column(Boolean, default=True)
    rank_for_promo = Column(Integer, default=0)
    manual_mode = Column(Boolean, default=False)
    site = relationship('Site', uselist=False)
    node = relationship('TreeNode',
        backref=backref('cardproduct',
            uselist=False,
            lazy='joined'),
        lazy='joined',
        uselist = False)
    categs = relationship(
        'CardProductCateg',
        backref=backref('cardproducts'),
        secondary=cardproducts_to_categs,
        lazy='dynamic',
        )
    titles = relationship('CardProductTitle',
        cascade = 'all, delete-orphan',
        passive_deletes = True,
        lazy = 'dynamic')
    annots = relationship('CardProductAnnotation',
        cascade = 'all, delete-orphan',
        passive_deletes = True,
        lazy = 'dynamic')
    promo_annots = relationship('CardProductPromoAnnotation',
        cascade = 'all, delete-orphan',
        passive_deletes = True,
        lazy = 'dynamic')
    useful_infos = relationship('CardProductUsefulInfo',
        cascade = 'all, delete-orphan',
        passive_deletes = True,
        lazy = 'dynamic')
    texts = relationship('CardProductText',
        cascade = 'all, delete-orphan',
        passive_deletes = True,
        lazy = 'dynamic')
        
    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'site_id',
                       'node_id',
                       'node',
                       'calculator',
                       'categs',
                       'titles',
                       'annots',
                       'texts',
                       )
            class Meta:
                model = cls
            site_id = QuerySelectField()
            node = QuerySelectField()
            categs = QuerySelectMultipleField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.site_id.data:
                        obj.site_id = self.site_id.data.id
                    obj.node = self.node.data
                    obj.categs = self.categs.data
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.site_id.data:
                        obj.site_id = self.site_id.data.id
                    obj.node = self.node.data
                    obj.categs = self.categs.data
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.site_id.query = Q(rootsdb.Site)
        f.node.query = \
            Q(rootsdb.TreeNode).filter(rootsdb.TreeNode.admin_node == False)
        f.categs.query = \
            Q(CardProductCateg).filter(CardProductCateg.active == True)
        return f
    
    def title_localized(self, lang_id):
        try:
            loc = self.titles.filter_by(lang_id=lang_id).first()
            if loc:
                return loc.content
            return self.inner_name
        except:
            return self.inner_name
            
    def annot_localized(self, lang_id):
        try:
            loc = self.annots.filter_by(lang_id=lang_id).first()
            if loc:
                return loc.content
            return self.inner_name
        except:
            return self.inner_name
            
    def promo_annot_localized(self, lang_id):
        try:
            loc = self.promo_annots.filter_by(lang_id=lang_id).first()
            if loc:
                return loc.content
            return self.inner_name
        except:
            return self.inner_name
        
    def useful_info_localized(self, lang_id):
        try:
            loc = self.useful_infos.filter_by(lang_id=lang_id).first()
            if loc:
                return loc.content
            return self.inner_name
        except:
            return self.inner_name
            
    def text_localized(self, lang_id):
        try:
            loc = self.texts.filter_by(lang_id=lang_id).first()
            if loc:
                return loc.content
            return self.inner_name
        except:
            return self.inner_name

admin.admin.content_types.append(CardProduct)
###############################################################################
