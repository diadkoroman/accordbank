# -*- coding: utf-8 -*-

# SQLALCHEMY IMPORTS
from sqlalchemy import (
        Table,
        Column,
        Date,
        Boolean,
        Numeric,
        ForeignKey,
        Integer,
        String,
        Text
        )
from sqlalchemy.orm import relationship, backref
from sqlalchemy.sql.expression import asc, desc
from sqlalchemy.sql.functions import func

from wtforms.fields import FileField
from wtforms import widgets
from wtforms import validators
from wtforms.ext.sqlalchemy.fields import (
    QuerySelectField,
    QuerySelectMultipleField
)

# APP IMPORTS
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy.connection import Base, Q, Metadata
from pyramid_cms.assets.db.sqlalchemy import fields
from pyramid_cms.assets.db.sqlalchemy.models import mixins
from pyramid_cms.assets.db.sqlalchemy.models import tables
from pyramid_cms.assets.translations import _db_ts as _

from pyramid_cms.apps.roots import dbmodels as rootsdb

from accordbank.apps.currencies import dbmodels as currsdb
###############################################################################

###############################################################################
""" Many-to-Many between Deposit and DepositPros. """
deposits_dproses = Table(
    tables.tablename_('deposits_dproses'), 
    Metadata,
    Column('deposit_id',Integer,ForeignKey(tables.fk_('deposits'))),
    Column('dpros_id',Integer,ForeignKey(tables.fk_('dproses')))
)

""" Many-to-Many between Deposit and DepositCateg. """
deposits_categs = Table(
    tables.tablename_('deposits_categs'), # fc_deposits_categs
    Metadata,
    Column('deposit_id',Integer,ForeignKey(tables.fk_('deposits'))),
    Column('categ_id',Integer,ForeignKey(tables.fk_('dcategs')))
)


'''
'''
deposits_minsums = Table(
    tables.tablename_('deposits_minsums'), # fc_deposits_minsums
    Metadata,
    Column('deposit_id',Integer,ForeignKey(tables.fk_('deposits'))),
    Column('minsum_id',Integer,ForeignKey(tables.fk_('deposit_minsums')))
)


'''
Таблиця відносин між депозитами та типами виплати відсотків
'''
deposits_interest_payments_types = Table(
    tables.tablename_('deposits_interest_payments_types'), # fc_deposits_interest_payments_types
    Metadata,
    Column('deposit_id',Integer,ForeignKey(tables.fk_('deposits'))),
    Column('ipaytype_id',Integer,ForeignKey(tables.fk_('deposit_interestp_types')))
)


calcs_amounts = Table(
    tables.tablename_('calcs_amounts'), # fc_calcs_amounts
    Metadata,
    Column('calc_id',Integer,ForeignKey(tables.fk_('depcalcs'))),
    Column('amount_id',Integer,ForeignKey(tables.fk_('depcalc_amounts')))
)

###############################################################################
class DepositPros(mixins.StandardColumnsMixin, Base):
    '''
    Корисна якість депозитів
    '''
    __tablename__ = tables.tablename_('dproses') 
    __descr__ = 'Корисна якість депозитів'
    verbose_name = 'Корисна якість депозиту'
    verbose_name_plural = 'Корисна якість депозитів'
    
    def __str__(self):
        return self.inner_name

    deposits = relationship(
        'Deposit',
        secondary=deposits_dproses,
        backref='pros',
        lazy='dynamic',
        )
    names = relationship('DepositProsName',
        cascade = 'all, delete-orphan',
        passive_deletes = True,
        lazy = 'dynamic')
        
    def localized(self, lang_id):
        try:
            loc = self.names.filter_by(lang_id=lang_id).first()
            if loc:
                return loc.content
            return self.inner_name
        except:
            return self.inner_name

admin.admin.content_types.append(DepositPros)
###############################################################################

###############################################################################
class DepositProsName(mixins.StringSlot, Base):
    __tablename__ = tables.tablename_('dpros_names')
    # fcbank_deposit_term_names
    __descr__ = 'Назви корисних якостей депозиту (локалі)'
    verbose_name = 'Назва корисної якості депозиту (локаль)'
    verbose_name_plural = 'Назви корисних якостей депозитів (локалі)'
    
    # мовна локалізація
    lang_id = Column(Integer,ForeignKey(tables.fk_('langs')))
    # депозит
    item_id = Column(Integer,
        ForeignKey(tables.fk_('dproses')))

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'item_id',
                       'lang_id'
                       )
            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(DepositPros)
        f.lang_id.query = Q(rootsdb.Language)
        return f
        
admin.admin.content_types.append(DepositProsName)
###############################################################################


###############################################################################
class DepositCateg(mixins.StandardColumnsMixin, Base):
    '''
    Типи депозитів (для приватних, для корпоративу тощо)
    '''
    __tablename__ = tables.tablename_('dcategs') # fc_deposit_categs
    __descr__ = 'Типи депозитів'
    verbose_name = 'категорія депозиту'
    verbose_name_plural = 'категорії депозитів'
    
    def __str__(self):
        return self.inner_name
        
    mnemo = Column('mnemo',String(100))
    deposits = relationship(
        'Deposit',
        secondary=deposits_categs,
        lazy='dynamic',
        )

admin.admin.content_types.append(DepositCateg)
###############################################################################


###############################################################################
class DepositTermName(mixins.SmallStringSlot, Base):
    __tablename__ = tables.tablename_('dterm_names')
    # fcbank_deposit_term_names
    __descr__ = 'Назви строків депозиту (локалі)'
    verbose_name = 'термін депозиту (локаль)'
    verbose_name_plural = 'терміни депозитів (локалі)'
    
    # мовна локалізація
    lang_id = Column(Integer,ForeignKey(tables.fk_('langs')))
    # депозит
    item_id = Column(Integer,
        ForeignKey(tables.fk_('deposit_terms')))

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'item_id',
                       'lang_id'
                       )
            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(DepositTerm)
        f.lang_id.query = Q(rootsdb.Language)
        return f
        
admin.admin.content_types.append(DepositTermName)

class DepositTerm(mixins.RankedColumnsMixin, Base):
    '''
    Терміни депозитів
    '''
    __tablename__ = tables.tablename_('deposit_terms') 
    # fcbank_deposit_terms
    __descr__ = 'Терміни депозитів'
    verbose_name = 'термін депозиту'
    verbose_name_plural = 'терміни депозитів'
    
    def __str__(self):
        return str(self.value)
    
    value = Column(Numeric(precision = 4,scale=2),default = 00.00)
    names = relationship('DepositTermName',
        cascade = 'all, delete-orphan',
        passive_deletes = True,
        lazy = 'dynamic')
    tariffs = relationship('DepositTariff',lazy = 'dynamic')
    
    def localized(self, lang_id):
        try:
            loc = self.names.filter_by(lang_id=lang_id).first()
            if loc:
                return loc.content
            return self.value
        except:
            return self.value

admin.admin.content_types.append(DepositTerm)
###############################################################################


###############################################################################
class DepositRate(mixins.StandardColumnsMixin, Base):
    __tablename__ = tables.tablename_('deposit_rates') #fcbank_deposit_rates
    __descr__ = 'Відсоткові ставки депозитів'
    verbose_name = 'ставка %'
    verbose_name_plural = 'ставки %'
    
    def __str__(self):
        return str(self.value)
        
    value = Column(Numeric(precision = 7,scale=4),default = 000.0000)
    
    @classmethod
    def table(cls, **kw):
        kw['order_by'] = 'value'
        return super().table(**kw)

admin.admin.content_types.append(DepositRate)
###############################################################################


###############################################################################
class DepositEarlyClosingRate(mixins.StandardColumnsMixin, Base):
    __tablename__ = tables.tablename_('deposit_ecrates') #fc_deposit_ecrates
    __descr__ = 'Ставки дострок. розірвання'
    verbose_name = 'ставка достр. %'
    verbose_name_plural = 'ставки достр. %'
    
    def __str__(self):
        return str(self.value)
        
    value = Column(Numeric(precision = 7,scale=4),default = 000.0000)

admin.admin.content_types.append(DepositEarlyClosingRate)
###############################################################################


###############################################################################
class DepositTax(mixins.StandardColumnsMixin, Base):
    '''Ставки податку на депозит'''
    __tablename__ = tables.tablename_('deposit_taxes') #fcbank_deposit_taxes
    __descr__ = 'Ставки податку на депозит'
    verbose_name = 'ставка податку %'
    verbose_name_plural = 'ставки податку %'
    
    def __str__(self):
        return str(self.value)

    value = Column(Numeric(precision = 7,scale=4),default = 000.0000)

admin.admin.content_types.append(DepositTax)
###############################################################################


###############################################################################
class DepositMinSum(mixins.StandardColumnsMixin, Base):
    '''
    Мінімальна сума поповнення депозиту
    '''
    __tablename__ = tables.tablename_('deposit_minsums') #fc_deposit_minsums
    __descr__ = 'Мінімальні суми поповнення'
    verbose_name = 'мін. сума поповнення'
    verbose_name_plural = 'мін. суми поповнення'
    
    def __str__(self):
        return '{0} {1}'.format(str(self.value),self.currency.mncode.upper())
    
    currency_id = Column(Integer, ForeignKey(tables.fk_('currencies')))
    value = Column(Integer,default = 0)
    currency = relationship('Currency', uselist = False)

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'currency_id',
                       'currency'
                       )
            class Meta:
                model = cls
            currency = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    obj.currency = self.currency.data
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    obj.currency = self.currency.data
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.currency.query = Q(currsdb.Currency)
        return f

admin.admin.content_types.append(DepositMinSum)
###############################################################################


###############################################################################
class DepositInterestPaymentTypeName(mixins.SmallStringSlot, Base):
    __tablename__ = tables.tablename_('deposit_interestp_type_names')
    # fc_deposit_interestp_type_names
    __descr__ = 'Типи виплат відсотків по депозиту (локалі)'
    verbose_name = 'тип виплати % (локаль)'
    verbose_name_plural = 'типи виплати % (локалі)'
    
    def __str__(self):
        return self.content
    
    # мовна локалізація
    lang_id = Column(Integer,ForeignKey(tables.fk_('langs')))
    # депозит
    item_id = Column(Integer,
        ForeignKey(tables.fk_('deposit_interestp_types')))
        
    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'item_id',
                       'lang_id'
                       )
            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(DepositInterestPaymentType)
        f.lang_id.query = Q(rootsdb.Language)
        return f

admin.admin.content_types.append(DepositInterestPaymentTypeName)


class DepositInterestPaymentType(mixins.RankedColumnsMixin, Base):
    __tablename__ = tables.tablename_('deposit_interestp_types')
    #fc_deposit_interestp_types
    __descr__ = 'Типи виплат відсотків по депозиту'
    verbose_name = 'тип виплати %'
    verbose_name_plural = 'типи виплати %'
    
    def __str__(self):
        return self.inner_name
    mnemo = Column('mnemo',String(100))
    names = relationship('DepositInterestPaymentTypeName',
        cascade = 'all, delete-orphan',
        passive_deletes = True,
        lazy = 'dynamic')
    
    def localized(self, lang_id):
        try:
            loc = self.names.filter_by(lang_id=lang_id).first()
            if loc:
                return loc.content
            return self.inner_name
        except:
            return self.inner_name

admin.admin.content_types.append(DepositInterestPaymentType)
###############################################################################


###############################################################################
class DepositTitle(mixins.StringSlot, Base):
    __tablename__ = tables.tablename_('deposit_titles') # fc_deposit_titles
    __descr__ = 'Назви депозиту(локалі)'
    verbose_name = 'назва депозиту (локаль)'
    verbose_name_plural = 'назви депозиту (локалі)'
    
    def __str__(self):
        return self.content
        
    # мовна локалізація
    lang_id = Column(Integer,ForeignKey(tables.fk_('langs')))
    # депозит
    item_id = Column(Integer,ForeignKey(tables.fk_('deposits')))
    item = relationship('Deposit', uselist=False)

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'item_id',
                       'item',
                       'lang_id'
                       )
            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(Deposit)
        f.lang_id.query = Q(rootsdb.Language)
        return f

admin.admin.content_types.append(DepositTitle)
###############################################################################


###############################################################################
class DepositText(mixins.TextSlot, Base):
    __tablename__ = tables.tablename_('deposit_texts') # fc_deposit_texts
    __descr__ = 'Тексти депозиту(локалі)'
    verbose_name = 'текст депозиту (локаль)'
    verbose_name_plural = 'тексти дапозитів (локалі)'
    
    def __str__(self):
        return self.item.inner_name
        
    # мовна локалізація
    lang_id = Column(Integer,ForeignKey(tables.fk_('langs')))
    # депозит
    item_id = Column(Integer,ForeignKey(tables.fk_('deposits')))
    item = relationship('Deposit', uselist=False)

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'item_id',
                       'item',
                       'lang_id'
                       )
            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(Deposit)
        f.lang_id.query = Q(rootsdb.Language)
        return f

admin.admin.content_types.append(DepositText)
###############################################################################


###############################################################################
class DepositAnnotation(mixins.TextSlot, Base):
    __tablename__ = tables.tablename_('deposit_annots') # fc_deposit_annots
    __descr__ = 'Анотації депозиту(локалі)'
    verbose_name = 'анотація депозиту (локаль)'
    verbose_name_plural = 'анотації депозитів (локалі)'
    
    def __str__(self):
        return self.item.inner_name
        
    # мовна локалізація
    lang_id = Column(Integer,ForeignKey(tables.fk_('langs')))
    # депозит
    item_id = Column(Integer,ForeignKey(tables.fk_('deposits')))
    item = relationship('Deposit', uselist=False)

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'item_id',
                       'item',
                       'lang_id'
                       )
            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(Deposit)
        f.lang_id.query = Q(rootsdb.Language)
        return f

admin.admin.content_types.append(DepositAnnotation)
###############################################################################

###############################################################################
class DepositPromoAnnotation(mixins.TextSlot, Base):
    __tablename__ = tables.tablename_('deposit_promo_annots') 
    __descr__ = 'Анотації депозиту(локалі) для промо-віджета'
    verbose_name = 'анотація депозиту (локаль)для промо-віджета'
    verbose_name_plural = 'анотації депозитів (локалі) для промо-віджета'
    
    def __str__(self):
        return self.item.inner_name
        
    # мовна локалізація
    lang_id = Column(Integer,ForeignKey(tables.fk_('langs')))
    # депозит
    item_id = Column(Integer,ForeignKey(tables.fk_('deposits')))
    item = relationship('Deposit', uselist=False)

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'item_id',
                       'item',
                       'lang_id'
                       )
            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(Deposit)
        f.lang_id.query = Q(rootsdb.Language)
        return f

admin.admin.content_types.append(DepositPromoAnnotation)
###############################################################################


###############################################################################
class DepositUsefulInfo(mixins.TextSlot, Base):
    __tablename__ = tables.tablename_('deposit_useful_infos') 
    __descr__ = 'Корисна інформація про депозит'
    verbose_name = 'Корисна інформація про депозит (локаль)'
    verbose_name_plural = 'Корисна інформація про депозит (локалі)'
    
    def __str__(self):
        return self.item.inner_name
        
    # мовна локалізація
    lang_id = Column(Integer,ForeignKey(tables.fk_('langs')))
    # депозит
    item_id = Column(Integer,ForeignKey(tables.fk_('deposits')))
    item = relationship('Deposit', uselist=False)

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'item_id',
                       'item',
                       'lang_id'
                       )
            class Meta:
                model = cls
            item_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = \
                    [
                        c for c in cls.columns_names() \
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in 
self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.item_id.data:
                        obj.item_id = self.item_id.data.id
                    if self.lang_id.data:
                        obj.lang_id = self.lang_id.data.id
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item_id.query = Q(Deposit)
        f.lang_id.query = Q(rootsdb.Language)
        return f

admin.admin.content_types.append(DepositUsefulInfo)
###############################################################################


###############################################################################
class Deposit(mixins.RankedColumnsMixin, Base):
    __tablename__ = tables.tablename_('deposits') # fcbank_deposits
    __descr__ = 'Депозити'
    verbose_name = 'депозит'
    verbose_name_plural = 'депозити'
    
    def __str__(self):
        return self.inner_name
    
    # сайт, на якому використовується депозит
    site_id = Column(Integer,ForeignKey(tables.fk_('sites')))
    """ Node for deposit in site tree. """
    node_id = Column(Integer,ForeignKey(tables.fk_('tree_nodes')))
    mnemo = Column('mnemo',String(150))
    autoprlng = Column(Boolean, default=True)
    pension = Column(Boolean, default=False)
    icon = Column(fields.UploadFileField(
        200,
        mimetypes = admin.admin.config.imgs_mime,
        upload_to= 'images'),
        nullable = True
    )
    in_promo = Column(Boolean, default=True)
    rank_for_promo = Column(Integer, default=0)
    manual_mode = Column(Boolean, default=False)
    site = relationship('Site', uselist=False)
    node = relationship('TreeNode', backref=backref('deposit', uselist=False), uselist = False)
    calculator = relationship('DepositCalc', uselist = False)
    categs = relationship(
        'DepositCateg',
        secondary=deposits_categs,
        lazy='dynamic',
        )
    titles = relationship('DepositTitle',
        cascade = 'all, delete-orphan',
        passive_deletes = True,
        lazy = 'dynamic')
    annots = relationship('DepositAnnotation',
        cascade = 'all, delete-orphan',
        passive_deletes = True,
        lazy = 'dynamic')
    promo_annots = relationship('DepositPromoAnnotation',
        cascade = 'all, delete-orphan',
        passive_deletes = True,
        lazy = 'dynamic')
    useful_infos = relationship('DepositUsefulInfo',
        cascade = 'all, delete-orphan',
        passive_deletes = True,
        lazy = 'dynamic')
    texts = relationship('DepositText',
        cascade = 'all, delete-orphan',
        passive_deletes = True,
        lazy = 'dynamic')
    # тарифні сітки депозиту
    tariffs = relationship('DepositTariff',
        cascade = 'all, delete-orphan',
        passive_deletes = True,
        lazy = 'dynamic')
    minsums = relationship('DepositMinSum',
        secondary = deposits_minsums,
        lazy = 'dynamic')
    ip_types = relationship('DepositInterestPaymentType',
        secondary = deposits_interest_payments_types,
        lazy = 'dynamic')
        
    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'site_id',
                       'node_id',
                       'node',
                       'calculator',
                       'categs',
                       'titles',
                       'annots',
                       'texts',
                       'tariffs',
                       'minsums',
                       'ip_types'
                       )
            class Meta:
                model = cls
            site_id = QuerySelectField()
            node = QuerySelectField()
            categs = QuerySelectMultipleField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if self.site_id.data:
                        obj.site_id = self.site_id.data.id
                    obj.node = self.node.data
                    obj.categs = self.categs.data
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    if self.site_id.data:
                        obj.site_id = self.site_id.data.id
                    obj.node = self.node.data
                    obj.categs = self.categs.data
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.site_id.query = Q(rootsdb.Site)
        f.node.query = Q(rootsdb.TreeNode)\
        .filter(rootsdb.TreeNode.admin_node == False)
        f.categs.query = Q(DepositCateg).filter(DepositCateg.active == True)
        return f
    
    def title_localized(self, lang_id):
        try:
            loc = self.titles.filter_by(lang_id=lang_id).first()
            if loc:
                return loc.content
            return self.inner_name
        except:
            return self.inner_name
            
    def annot_localized(self, lang_id):
        try:
            loc = self.annots.filter_by(lang_id=lang_id).first()
            if loc:
                return loc.content
            return self.inner_name
        except:
            return self.inner_name
            
    def promo_annot_localized(self, lang_id):
        try:
            loc = self.promo_annots.filter_by(lang_id=lang_id).first()
            if loc:
                return loc.content
            return self.inner_name
        except:
            return self.inner_name
        
    def useful_info_localized(self, lang_id):
        try:
            loc = self.useful_infos.filter_by(lang_id=lang_id).first()
            if loc:
                return loc.content
            return self.inner_name
        except:
            return self.inner_name
            
    def text_localized(self, lang_id):
        try:
            loc = self.texts.filter_by(lang_id=lang_id).first()
            if loc:
                return loc.content
            return self.inner_name
        except:
            return self.inner_name
            
    def get_terms(self):
        """ Отримуємо всі діючі терміни для поточного депозиту """
        terms = []
        ids = [i.term.id for i in self.tariffs if i.active == True]
        return [i for i in Q(DepositTerm)\
        .filter(DepositTerm.id.in_(ids))\
        .order_by(desc(DepositTerm.value))\
        .distinct()]
        
    def get_amounts(self,curr_id=False):
        """
        Отримуємо активні суми для шкали калькулятора поточноо депозиту.
        Якщо вказано валюту, то отримуються суми для вказаної валюти.
        Якщо не вказано - отримуються суми для дефолтної валюти калькулятора.
        """
        if curr_id:
            return [i.value for i in self.calculator.amounts\
            .filter_by(currency_id=curr_id,active=True)\
            .order_by('value')]
        return [i.value for i in self.calculator.amounts\
        .filter_by(currency_id=self.calculator.def_curr_id,active=True)\
        .order_by('value')\
        .distinct()]
        
    def get_iptypes(self, lang_id):
        """
        Отримати усі типи виплати відсотків для поточного депозиту.
        Повертається локалізована версія назви.
        """
        ids = [i.ipt.id for i in self.tariffs if i.active == True]
        return [i.localized(lang_id) for i in DepositInterestPaymentType\
        .filter(DepositInterestPaymentType.id.in_(ids))\
        .filter(~DepositInterestPaymentType.mnemo.like('%_cap'))\
        .order_by('rank')\
        .distinct()]
        
    def get_iptypes2(self):
        """
        Отримати усі типи виплати відсотків для поточного депозиту.
        Повертається весь об’єкт.
        """
        ids = [i.ipt.id for i in self.tariffs if i.active == True]
        return [i for i in DepositInterestPaymentType\
        .filter(DepositInterestPaymentType.id.in_(ids))\
        .filter(~DepositInterestPaymentType.mnemo.like('%_cap'))\
        .order_by('rank')\
        .distinct()]
        
    def has_iptype_rank(self,item):
        """
        Отримати rank для усіх типів виплати відсотків для поточного депозиту.
        Повертається rank.
        """
        ids = [i.ipt.id for i in self.tariffs]
        if int(item) in [i.rank for i in DepositInterestPaymentType\
        .filter(DepositInterestPaymentType.id.in_(ids))\
        .distinct()]:
            return True
        return False
        
    def get_currencies(self):
        """ Отримати усі типи валют для поточного депозиту """
        ids = [i.currency.id for i in self.tariffs]
        return [i for i in Q(currsdb.Currency.id, currsdb.Currency.mncode)\
        .filter(currsdb.Currency.id.in_(ids)).order_by('rank_deptable')\
        .distinct()]
        
    def get_curr_ids(self):
        """ Отримати ідентифікатори валют """
        ids = [i.currency.id for i in self.tariffs]
        Currency = admin.db.get('currencies').model
        return [i.id for i in Currency\
        .filter(Currency.id.in_(ids)).order_by('rank_deptable')\
        .distinct()]
        
    def get_default_currency(self):
        """ Отримати дефолтну валюту для калькулятора """
        Currency = admin.db.get('currencies').model
        return Currency.query(Currency.mncode)\
        .filter(Currency.rank_deptable == 1)\
        .scalar()
        
    def has_capitalization(self):
        """ Чи має депозит капіталізацію """
        cap = DepositTariff.join(DepositInterestPaymentType)\
        .filter(DepositTariff.item_id==self.id)\
        .filter(DepositInterestPaymentType.mnemo.like('%_cap'))
        if not cap is None:
            return True
        return False

    def get_rates_table(self):
        """
        Метод, що створює стуктуру даних для наступного їх виводу у вигляді
        таблиці зі ставками депозитів.
        """
        rates_table = {}
        rows = []
        cols = []
        # колонки формуємо на основі валют депозиту
        currs = self.get_currencies()
        # рядки формуємо на основі термінів депозиту
        terms = self.get_terms()
        # заголовний рядок із мнемо-кодами валют
        rows.append([i[1] for i in currs])
        # заголовна колонка із термінами
        # cols.append([round(t.value,1) for t in terms])
        cols.append(['{0:.1f}'.format(t.value).replace('.0','') for t in terms])
        
        for term in terms:
            col = []
            for curr in currs:
                rates = self.tariffs\
                .filter_by(active=True)\
                .filter_by(currency_id=curr.id,term_id=term.id)\
                .order_by('note_marker')
                #.first()
                try:
                    """
                    We have to work on all rates for deposit term
                    (there can be more than one rate for the term)
                    """
                    r = []
                    for rate in rates:
                        s = str(round(float(str(rate.rate)) * 100, 2))
                        s += '%'
                        # if there is note marker for the rate - push it
                        if isinstance(rate.note_marker, (str,)):
                            s += rate.note_marker
                        r.append(s)
                    # collect data into the string for output
                    coldata = ' / '.join(r)
                    
                    #coldata = str(round(float(str(rate.rate)) * 100, 2))
                    #col.append(round(float(str(rate.rate)) * 100, 2))
                    col.append(coldata)
                except:
                    col.append(None)
            cols.append(col)
        
        rates_table['rows'] = rows
        rates_table['cols'] = cols
        return rates_table
        
    def get_maxrate(self):
        """
        Отримати значення максимальної ставки по депозиту
        """
        q = \
        Q(func.max(DepositRate.value).label('maxrate'))\
        .join(DepositTariff)\
        .filter(DepositTariff.item_id == self.id)\
        .filter(DepositTariff.active == True).first()
        return str(q.maxrate * 100)
admin.admin.content_types.append(Deposit)
###############################################################################


###############################################################################
'''
Клас-агрегатор даних депозиту - т.зв. тарифна сітка депозиту.
'''
class DepositTariff(mixins.StandardColumnsMixin, Base):
    __tablename__ = tables.tablename_('deposit_tariffs') #fc_deposit_tariffs
    __descr__ = 'Клас-агрегатор даних депозиту'
    verbose_name = 'тарифна модель'
    verbose_name_plural = 'тарифні моделі'
    
    def __str__(self):
        return '{0}, {1}, {2}, {3}'.format(
            self.item.inner_name,
            self.currency.mncode.upper(),
            self.term.inner_name,
            self.rate.inner_name
            )
    
    item_id = Column(Integer, ForeignKey(tables.fk_('deposits')))
    currency_id = Column(Integer, ForeignKey(tables.fk_('currencies')))
    term_id = Column(Integer, ForeignKey(tables.fk_('deposit_terms')))
    rate_id = Column(Integer, ForeignKey(tables.fk_('deposit_rates')))
    tax_id = Column(Integer, ForeignKey(tables.fk_('deposit_taxes')))
    ecrate_id = Column(Integer, ForeignKey(tables.fk_('deposit_ecrates')))
    ipt_id = Column(Integer, ForeignKey(tables.fk_('deposit_interestp_types')))
    # make a note marker (*,** etc.)
    note_marker = Column(String(10), nullable=True)
    
    item = relationship('Deposit', uselist = False)
    currency = relationship('Currency', uselist = False)
    term = relationship('DepositTerm', uselist = False)
    rate = relationship('DepositRate', uselist = False)
    tax = relationship('DepositTax', uselist = False)
    ecrate = relationship('DepositEarlyClosingRate', uselist = False)
    ipt = relationship('DepositInterestPaymentType',uselist = False)

    @classmethod
    def table(cls, **kw):
        kw['order_by'] = 'item_id, currency_id, term_id'
        return super().table(**kw)

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'item_id',
                       'currency_id',
                       'term_id',
                       'rate_id',
                       'tax_id',
                       'ecrate_id',
                       'ipt_id'
                       )
            class Meta:
                model = cls
            item = QuerySelectField()
            currency = QuerySelectField()
            term = QuerySelectField()
            rate = QuerySelectField()
            tax = QuerySelectField()
            ecrate = QuerySelectField()
            ipt = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    obj.item = self.item.data
                    obj.currency = self.currency.data
                    obj.term = self.term.data
                    obj.rate = self.rate.data
                    obj.tax = self.tax.data
                    obj.ecrate = self.ecrate.data
                    obj.ipt = self.ipt.data
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    obj.item = self.item.data
                    obj.currency = self.currency.data
                    obj.term = self.term.data
                    obj.rate = self.rate.data
                    obj.tax = self.tax.data
                    obj.ecrate = self.ecrate.data
                    obj.ipt = self.ipt.data
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item.query = Q(Deposit)
        f.currency.query = Q(currsdb.Currency)
        f.term.query = Q(DepositTerm)
        f.rate.query = Q(DepositRate).order_by('value')
        f.tax.query = Q(DepositTax)
        f.ecrate.query = Q(DepositEarlyClosingRate)
        f.ipt.query = Q(DepositInterestPaymentType)
        return f

admin.admin.content_types.append(DepositTariff)
###############################################################################


class DepositCalc(mixins.RankedColumnsMixin, Base):
    __tablename__ = tables.tablename_('depcalcs') # fc_depcalcs
    __descr__ = 'Калькулятори депозитів'
    verbose_name = 'калькулятор депозиту'
    verbose_name_plural = 'калькулятори депозитів'
    
    def __str__(self):
        return 'Калькулятор: {0}'.format(self.item.inner_name)
    
    item_id = Column(Integer, ForeignKey(tables.fk_('deposits')))
    def_curr_id = Column(Integer, ForeignKey(tables.fk_('currencies')))
    item = relationship('Deposit', uselist = False)
    def_curr = relationship('Currency', uselist = False)
    amounts = relationship(
        'DepositCalcAmount',
        secondary=calcs_amounts,
        lazy='dynamic',
        backref='calculator',
        )

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'item_id',
                       'item',
                       'def_curr_id',
                       )
            class Meta:
                model = cls
            item = QuerySelectField('Депозит')
            def_curr = QuerySelectField('Валюта калькулятора (умовч.)')

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    obj.item = self.item.data
                    obj.def_curr = self.def_curr.data
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    obj.item = self.item.data
                    obj.def_curr = self.def_curr.data
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.item.query = Q(Deposit)
        f.def_curr.query = Q(currsdb.Currency)
        return f

admin.admin.content_types.append(DepositCalc)


class DepositCalcAmount(mixins.LiteColumnsMixin, Base):
    __tablename__ = tables.tablename_('depcalc_amounts') # fc_depcalc_amounts
    __descr__ = 'Величини для шкали калькулятора "Сума депозиту"'
    verbose_name = 'сума'
    verbose_name_plural = 'суми'
    
    def __str__(self):
        return '{0} ({1})'.format(str(self.value),self.currency.mncode.upper())
    
    currency_id = Column(Integer, ForeignKey(tables.fk_('currencies')))
    value = Column(Integer,default = 0)
    currency = relationship('Currency', uselist = False)

    @classmethod
    def _form(cls, *args, **kw):
        """ Генератор інтерфейсу взаємодії з користувачем """
        class __F(cls._uiform()):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'currency_id',
                       'currency',
                       )
            class Meta:
                model = cls
            currency = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    obj.currency = self.currency.data
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    obj.currency = self.currency.data
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.currency.query = Q(currsdb.Currency)
        return f

admin.admin.content_types.append(DepositCalcAmount)
