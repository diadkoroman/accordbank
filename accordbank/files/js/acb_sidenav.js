$(function(){
    /*
     * Side navigator positioning
     * */
    var sidenav = $('#sidenav');
    var content_block = $('.content-block, #deposits-list');
    var objOffset = sidenav.offset();
    var objWidth = sidenav.innerWidth();
    var topbarH = $('.top-bar').innerHeight();
    var mainnavH = $('#main-nav').innerHeight();
    var bcrsH = $("#acb-breadcrumbs").innerHeight();
    var footerH = $('#footer').innerHeight();
    var footerOffset = $('#footer').offset();
    var avH = screen.availHeight;
    //var main_topH = (mainnavH + topbarH);
    var main_topH = (mainnavH + bcrsH);
    var sidenavH = sidenav.innerHeight();
    var content_blockH = content_block.innerHeight();
    var bodyH = $('body').innerHeight();
    var bottom_offset = avH * 0.3;
    
    
    $(window).scroll(null, function(){
        if(sidenavH < content_blockH){
            var top = $(document).scrollTop();
            //(top >= bcrsH && top < avH/2)
            if(top > 0){
                sidenav.css({
                    'position': 'fixed',
                    'top': main_topH,
                    //'padding': '20px 10px',
                    //'width': objWidth,
                    'margin-top': 0,
                });
            }
            else{
                    sidenav.css({'position': 'static'})
                };
            console.log(top);
            console.log(content_blockH);
        };
        
        /*if(top > avH/2 && top < avH-bottom_offset){
            sidenav.css({'position': 'static', 'margin-top':top})
            };
        */
        if(top >= avH*0.75){
            sidenav.fadeOut(450);
            // sidenav.css({'position': 'static', 'margin-top':main_topH})
            }else{sidenav.fadeIn(450)};
    });
    
});
