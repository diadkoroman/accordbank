$(function(){
    var newsHeaderIndex = $('.news-header-index');
    var newsHeaderIndexText = $('.news-header-index-text');
    var newsAnnounceIndex = $('.news-announce-index');
    var defaultColor = '#131313';
    var activeColor = '#B22222';
    
    newsHeaderIndex.mouseover(function(){
        $(this).find('.news-header-index-text').css({'color': activeColor});
        $(this).find('.news-announce-index').css({'color': defaultColor});
        });
    newsHeaderIndex.mouseleave(function(){
        $(this).find('.news-header-index-text').css({'color': defaultColor});
        $(this).find('.news-announce-index').css({'color': defaultColor});
        });
});
