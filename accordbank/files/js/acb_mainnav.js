$(function(){
    var mainnav_link = $('.main-nav a');
    var snWrapper = $('#subnavs-wrapper');
    var menuBlock = $('.main-nav');
    var snContainer = $('#subnav-container');
    // клік на пункті меню
    function mainnavClickHandler(link){
        //знаходимо поточний пункт
        var curr_id = link.attr('id');

        if($('.inner-submenu-wrapper[ident=' + curr_id + '] .inner-submenu-container').length > 0){
            // беремо дані з потрібного контейнера
            var html = $('.inner-submenu-wrapper[ident=' + curr_id + ']').html();
            var container_html = snContainer.html();
            // заливаємо у контейнер
            snContainer.empty().html(html);
            // показуємо(або ховаємо контейнер)
            if(html != container_html){
                snWrapper.show();
            }else{
                snWrapper.toggle();
                };
        }else{
            snContainer.empty();
            snWrapper.hide();
        };
    };
    
    function hideSnWrapper(){
        setTimeout(function(){
            if(snWrapper.is(':visible')){
                snWrapper.fadeOut();
                $('body').off('click');
            };
        }, 100);
    };
    
    //
    mainnav_link.click(function(event){
        mainnavClickHandler($(this));
        event.stopPropagation();
    });
    
    snWrapper.mouseleave(function(event){
        if(event.target != $(this) && event.target != $('#main-nav a')){
            $('body').on('click', $(this), hideSnWrapper);
        };
    });
    
    snWrapper.mouseenter(function(){
        $('body').off('click');
    });
    
});
