$(function(){
    // main banner slot
    var mbslot = $('.banner-main-bannerslot');
    // main banner object
    var mainbanner = $('.main-banner');
    // add banners
    var banner = $('.banner');
    // top navigator height
    var topbarH = $('.top-bar').innerHeight();
    // main navigator height
    var mainnavH = $('#main-nav').innerHeight();
    // screen height available
    var screenH = screen.availHeight;
    // max bannerslot height
    var mbslotMaxH = 500;
    //height for mbslot
    var mbslotAvH = Math.ceil((screenH - ((topbarH + mainnavH) * 2)) * 0.75);
    // random colors
    var colors = ['#b22222', '#e88585', '#e05a5a', '#cc6e6e', '#e05a5a', '#f8dbdb'];
    
    var colors_used = [];
    // empty banner chars
    var empties = $('.banner-char-empty');
    
    // timeout for banner engine
    /*
     * Значення для цієї змінної отримується в шаблон 
     * acb_mainbannerslot_widget із БД */
    var timeout = scrlint;
    // mainbanner queue
    var mbqueue = mainbanner.length;
    //counter for rotator
    var i = 1;
    var prev = 0;
    
    /*
     * setting banner slot height
     * */
    function setSlotHeight(){
        var mbsH = mbslot.innerHeight();
        if(mbslotAvH > mbslotMaxH){
            mbslotAvH = mbslotMaxH;
        };
        mbslot.height(mbslotAvH);
    };
    
    /*
     * main-banner images set sizing */
    function setMainBannerImagesSize(){
        mainbanner.find('img').css({'max-width':'100%', 'height':'auto'});
        };
    
    /*
     * show first mainbanner*/
    function showFirstMainBanner(){
		mainbanner.hide();
		mainbanner.eq(0).show();
	};
	
	/*
	 * hide banner in additional container if it is shown in main
	 * */
	function hideShownInMainBanners(ident){
		mainbanner.each(function(){
			if($(this).is(':visible')){
            banner.show();
            $('.banner[ident='+ ident +']').hide();
            return true;
			};
		});
	};
    
    function setColor(obj){
        var c = colors[Math.floor(Math.random()*colors.length)];
        if(colors_used.indexOf(c) != -1){
            setColor(obj);
        }else{
            obj.css({'background-color': c});
            colors_used.push(c);
        };
    };
    
    /*
     * Choose random background color for each empty banner char
     * */
     function setEmptyCharBackground(){
        if(empties.length != 0){
            var colors_used = [];
            empties.each(function(){
                setColor($(this));
            });
        };
    };
    
    /*
     * init function for bannerslot 
     * */
    function initBannerEngine(){
        if(timeout > 0 && mainbanner.length > 1){
            setInterval(function(){
                function rotate(){
                    var mbwidth = mainbanner.width();
                    mainbanner.css({'z-index':0});
                    mainbanner.hide();
                    mainbanner.eq(i).css({'z-index':10, 'left':'-' + mbwidth + 'px'});
                    // mainbanner.eq(i).fadeIn();
                    mainbanner.eq(prev).show();
                    mainbanner.eq(i).show().animate({'left':'+=' + mbwidth});
                    var ident = mainbanner.eq(i).attr('ident');
                    hideShownInMainBanners(ident);
                    i+=1;
                    prev=(i-1);
                    if(i >= mbqueue){
                        i = 0;
                        prev = mbqueue-1;
                    };
                    console.log(i);
                    console.log(prev);
                };

                rotate();

            }, timeout);
        };
    };
    
    //init part
    setSlotHeight();
    setEmptyCharBackground();
    setMainBannerImagesSize();
    showFirstMainBanner();
    hideShownInMainBanners(mainbanner.eq(0).attr('ident'));
    initBannerEngine();
});
