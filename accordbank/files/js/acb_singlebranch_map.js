/*
* Відображення карти для однієї точки 
* */
    
function initMap(){
    var markers = [];

    //створюємо мапу
    var map = new google.maps.Map(document.getElementById('map-wr-inner'), {
        center: coords[0],
        zoom: 10,
        maxZoom:15,
        styles: [{
        featureType: 'poi',
        stylers: [{ visibility: 'off' }]  // Turn off points of interest.
        }, 
        {
        featureType: 'transit.station',
        stylers: [{ visibility: 'off' }]  // Turn off bus stations, train stations, etc.
        }],
        disableDoubleClickZoom: true,
        scrollwheel: false,
    });
        
    // створюємо точки
    var pointBounds = new google.maps.LatLngBounds();

    for(i=0;i<coords.length;i++){
        var pointPos = new google.maps.LatLng(coords[i].lat,coords[i].lng);
        pointBounds.extend(pointPos);
        var marker = new google.maps.Marker({
            position: pointPos,
            map: map,
            title:''});
        // addInfoWindow(marker, not_empties[i]);
        markers.push(marker);
    };
    
    //обраховуємо ширину карти і підганяємо висоту
    var mapW = $('#map-wr-inner').innerWidth();
    $('#map-wr-outer').innerHeight(mapW * 0.7);
    $('#map-wr-inner').css({'height': 'inherit'});
    
    map.setCenter(pointBounds.getCenter(), map.fitBounds(pointBounds));
};
