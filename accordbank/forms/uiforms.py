# -*- coding: utf-8 -*-

"""
Форма для надсилання зворотного зв’язку
"""
from wtforms import fields, form
from wtforms import widgets
from wtforms import validators
from wtforms.ext.sqlalchemy.fields import QuerySelectField

from pyramid_cms.assets.forms import UIForm, ProtectedForm
from accordbank.apps.messages import dbmodels as mesgdb
from accordbank.apps.deposits import dbmodels as deposdb

# регулярка для валідації телефонного номера
teln_regex = '^\(?([0-9]{3,6})\)?([\s\-]?)([0-9]{1,3})([\s\-]?)([0-9]{1,2})([\s\-]?)([0-9]{1,2})$'
teln_regex380 = '^\+380\(?([0-9]{2,6})\)?([\s\-]?)([0-9]{1,3})([\s\-]?)([0-9]{1,2})([\s\-]?)([0-9]{1,2})$'

#################### Custom validators ########################################
def no_topic_chosen(form, field):
    """ Валідація поля з темами - повина бути обрана тема """
    if not field.data or field.data in (None, 'None', 0, '0', ''):
        raise validators.ValidationError('no_topic_chosen')
###############################################################################

class MessagesForm(UIForm):
    exclude = (
               'id',
               'created',
               'updated',
               'topic_id'
              )
    # максимальна кількість знаків у тексті повідомлення
    max_content_length = 750
    # значення, що трактується як "скоро кінець рядка"
    content_closeend =  100
    class Meta:
        model = mesgdb.Message
    sender_name = fields.StringField('sender name', [
        validators.InputRequired(message='input_required')
        ])
    sender_teln = fields.StringField('sender tel#', [
        validators.Optional(), 
        validators.Regexp(
            teln_regex,
            message='not_teln'
            )
        ])
    sender_email = fields.StringField('sender email', [
        validators.InputRequired(message='input_required'),
        validators.Email(message='not_email'),
        ])
    content = fields.TextAreaField('message text', [
        validators.InputRequired(message='input_required'),
        validators.Length(min=4, max=max_content_length, message='message is too long')
        ])
    topics = fields.SelectField(choices=((),))
    
    def validate_and_add(self):
        # add new instance
        columns = [c for c in self.Meta.model.columns_names() if not c in self.exclude]
        if self.validate():
            obj = self.Meta.model()
            for attr in columns:
                setattr(obj,attr, getattr(self, attr).data)
            if self.topics.data:
                obj.topic_id = self.topics.data
            obj.save(flush=True)
            return True
        else:
            return False


######################## Callback Form #######################################
class CallbackForm(UIForm):
    """ Форма "Замовити дзвінок" для зовнішнього сайту(нового) """
    exclude = (
               'id',
               'created',
               'updated',
               'topic_id'
              )
    class Meta:
        model = mesgdb.Message
    sender_name = fields.StringField('sender name', [
        validators.InputRequired(message='input_required')
        ])
    sender_teln = fields.StringField('sender tel#', [
        validators.InputRequired(message='input_required'), 
        validators.Regexp(
            teln_regex380,
            message='not_teln'
            )
        ])
    topics = fields.SelectField('topics', 
        [
        no_topic_chosen
        ], choices=((),))


    def add_validated(self):
        """
        Додавання УЖЕ ПРОВАЛІДОВАНИХ даних.
        !!! Використовувати тільки якщо зовні було викликано метод
        form.validate і валідація пройшла успішно.
        """
        try:
            columns = \
                [
                    c for c in self.Meta.model.columns_names() 
                    if not c in self.exclude
                ]
            obj = self.Meta.model()
            for attr in columns:
                setattr(obj,attr, getattr(self, attr).data)
            if self.topics.data:
                obj.topic_id = self.topics.data
            obj.save()
            return True
        except:
            return False
##############################################################################

######################### Question Form ######################################
class QuestionForm(UIForm):
    exclude = (
               'id',
               'created',
               'updated',
               'topic_id'
              )
    # максимальна кількість знаків у тексті повідомлення
    max_content_length = 750
    # значення, що трактується як "скоро кінець рядка"
    content_closeend =  100
    class Meta:
        model = mesgdb.Message
    topic_id = fields.HiddenField()
    sender_name = fields.StringField('sender name', [
        validators.InputRequired(message='input_required')
        ])
    sender_teln = fields.StringField('sender tel#', [
        validators.InputRequired(message='input_required'), 
        validators.Regexp(
            teln_regex380,
            message='not_teln'
            )
        ])
    sender_email = fields.StringField('sender email', [
        validators.InputRequired(message='input_required'),
        validators.Email(message='not_email'),
        ])
    content = fields.TextAreaField('message text', [
        validators.InputRequired(message='input_required'),
        validators.Length(min=4, max=max_content_length, message='message is too long')
        ])
    
    def add_validated(self):
        """
        Додавання УЖЕ ПРОВАЛІДОВАНИХ даних.
        !!! Використовувати тільки якщо зовні було викликано метод
        form.validate і валідація пройшла успішно.
        """
        try:
            columns = \
                [
                    c for c in self.Meta.model.columns_names
                    if not c in self.exclude
                ]
            obj = self.Meta.model()
            for attr in columns:
                setattr(obj,attr, getattr(self, attr).data)
            if self.topic_id.data:
                obj.topic_id = self.topic_id.data
            obj.save()
            return True
        except:
            return False
##############################################################################


class CalculatorForm(form.Form):
    currencies = fields.SelectField()
    amount = fields.StringField('deposit_amount', [validators.DataRequired(message='input_required')])
    terms = fields.SelectField(choices=[])
    iptypes = fields.SelectField(choices=[])
