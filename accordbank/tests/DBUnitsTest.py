# -*- coding: utf-8 -*-

import unittest
from pyramid import testing
from pyramid_cms.assets.db.sqlalchemy.connection import DBSession, Base, Q


class DBUnitsTest(unittest.TestCase):
    
    
    def setUp(self):
        self.request = testing.DummyRequest()
        self.curr_lang = 2
        self.host_url = 'http://127.0.0.1:8775'
        self.config = testing.setUp(request=self.request)
        from sqlalchemy import create_engine
        engine = create_engine('mysql+pymysql://acb_site:p!ssw4acbDB@localhost/acb_sitedb?charset=utf8&use_unicode=1&unix_socket=/var/run/mysql/mysql.sock')
        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
    
    
    def test_navslots(self):
        from sqlalchemy.sql.expression import asc,desc
        from pyramid_cms.apps.navs import dbmodels as navsdb
        items = navsdb.NavItem
        slots = navsdb.NavSlot
        try:
            instance = Q(items)\
                .join(slots)\
                .filter(
                    slots.mnemo == 'acb_topnav',
                    items.active == True)\
                .order_by(asc(items.rank))\
                .first()
            print(instance.localized(self.curr_lang))
        except:
            print('No instance found!')

    def test_siteitem(self):
        from pyramid_cms.apps.roots import dbmodels as rootsdb
        # host_url = 'http://127.0.0.1:8775'
        try:
            self.site = Q(rootsdb.Site)\
                .filter(rootsdb.Site.address ==self.host_url)\
                .first()
            self.site_id = self.site.id
            print(self.site.id)
        except:
            print('No site found!')

    def test_cssfiles(self):
        from pyramid_cms.apps.roots import dbmodels as rootsdb
        from pyramid_cms.apps.staticfiles import dbmodels as statsdb
        files = statsdb.CSSFile
        site = Q(rootsdb.Site)\
                .filter(rootsdb.Site.address ==self.host_url)\
                .first()
        try:
            instance = Q(files)\
                .filter(
                    files.site_id == site.id,
                    files.active == True)\
                .first()
            print(instance.site_id)
        except:
            print('No cssfiles found!')

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()
