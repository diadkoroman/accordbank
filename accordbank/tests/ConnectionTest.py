# -*- coding: utf-8 -*-
import unittest
from pyramid import testing
from pyramid_cms.assets.db.sqlalchemy.connection import DBSession, Base, Q


class ConnectionTest(unittest.TestCase):
    
    def setUp(self):
        self.config = testing.setUp()
        from sqlalchemy import create_engine
        engine = create_engine('mysql+pymysql://acb_site:p!ssw4acbDB@localhost/acb_sitedb?charset=utf8&use_unicode=1&unix_socket=/var/run/mysql/mysql.sock')
        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        
    def test_testcase(self):
        print("It works")
        

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()
