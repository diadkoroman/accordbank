# -*- coding: utf-8 -*-

"""
Додаткові класи віджетів, доповнені властивостями, необхідними
у межах проекту
"""
import re
from pyramid_cms.assets.widgets import ProjectWidget

class SearchEnabledWidget(ProjectWidget):
    
    # назва змінної GET користувацького вводу
    user_input_var = "trg"
    _raw_user_input = None
    _cleaned_user_input = None
    _forbidden_input='[><:;`~@#$%\^\+\=\/\*\.,\]\[}{]+'
    # мінімальна довжина пошукового терміну
    _min_clean_length = 2

    def clean_user_input(self):
        """
        Очищення користувацького вводу.
        _raw_user_input - НЕОБРОБЛЕНІ(!) дані
        _cleaned_user_input - дані після обробки
        
        1. якщо знаходимо змінну вводу - отримуємо її значення із GET
        і вносимо до _raw_user_input.
        
        2. якщо _raw_user_input має значення не-None, то здійснюємо обробку
        і зневадження даних.
        """

        self._raw_user_input = None
        self._cleaned_user_input = None

        # 1.
        if self.user_input_var in self.req.GET:
            self._raw_user_input = self.req.GET.get(self.user_input_var)

        # 2.
        if self._raw_user_input:
            # очищаємо від спецсимволів
            clean = \
            re.sub(self._forbidden_input, '', self._raw_user_input, re.I)
            # розбиваємо на слова
            clean = re.split("\s+", clean, re.I)
            self._cleaned_user_input = \
            [i for i in clean if len(i) > self._min_clean_length]
            
    def mark_search_res(self, sterms, data):
        """
        Ставимо позначки на елементах виводу, що відповідають пошуковому 
        запиту.
        sterms - пошукові терміни(list,tuple)
        data - дані для опрацювання
        1. аналізуємо пошуковий запит як ціле
        2. аналізуємо пошуковий запит поелементно
        """
        if sterms:
            #1
            #sterm = ' '.join(sterms)
            #data = data.replace(sterm, '<span class="label">{0}</span>'.format(sterm))
            #2
            for st in sterms:
                data = re.sub(st, '<span class="label">{0}</span>'.format(st), data, re.I)
        return data
