# -*- coding: utf-8 -*-

import datetime
import re

"""
Класи обробників для отримання даних з сайту НБУ
"""
from pyramid_cms import admin
from pyramid_cms.assets.handlers import rdhandler

class NBUDataHandler(rdhandler.RemoteSourceDataHandler):
    """
    Основний клас для отримання даних з сайту НБУ.
    Відмінності від батьківського класу полягають:
    - у наявності спеціального токена для розпізнавання параметра у
    конфігурації
    - у призначенні урла з конфіга проекту, тобто необхідності
    призначати урл при ініціалізації екземпляра не потрібно
    """

    # токен параметра
    """
    Підставляючи цей токен і поєднуючи його з inp_type отримуємо
    назву параметра у конфігурації проекту, котрий містить потрібний
    нам урл
    Напр. якщо inp_type = 'json', то при ініціалізації екзмепляра класу
    шукатиметься параметр _settings_attr_token + inp_type,
    тобто  у випадку з NBUDataHandler - 
    'nbuapi_currs_json' ('nbuapi_currs_'+'json')
    предметом пошуку в конфігурації 
    буде параметр admin.project.config.nbuapi_currs_json
    """
    _settings_attr_token = \
        getattr(admin.project.config, 'nbuapi_currs_token', None)
    def __init__(self, **kw):
        # спочатку відпрацьовує ініціалізатор батьківського класу
        super().__init__(**kw)
        try:
            self.request_url = \
                getattr(
                        admin.project.config,
                        self._settings_attr_token + self.inp_type
                        )
        except AttributeError as e:
            raise e
        


class NBUSingleCurrencyDataHandler(NBUDataHandler):
    """
    Обробник для отримання запиту стосовно окремої валюти з сайту НБУ.
    Відмінності від батьківського класу:
    - наявність спеціальних методів для модифікації параметра
     request_url
    """
    # мнемо-код валюти для внесення в УРЛ
    current_valcode = None
    # дата для внесення в УРЛ
    current_date = None
    _settings_attr_token = \
        getattr(admin.project.config, 'nbuapi_currency_token', None)
    
    def __init__(self, **kw):
        super().__init__(**kw)
        # обробляємо дані про мнемо-код валюти
        self.set_current_valcode()
        # обробляємо дані про поточну дату
        self.set_current_date()
        # модифікуємо УРЛ для отримання даних про валюту
        self.set_request_url()
        
    def get_data(self, **kw):
        self.processed_data = super().get_data(**kw)
        return self.processed_data[0] if self.processed_data else []
        
    def set_current_valcode(self):
        # обробка вхідних даних про мнемо-код валюти
        """
        Очищаємо користувацький ввід від непотрібних знаків
        """
        valcode = re.sub(r'[^a-z]+','', self.current_valcode, flags=re.I)
        try:
            self.current_valcode = valcode[:3].upper()
        except Exception as e:
            raise e

    def set_current_date(self):
        # обробка даних про поточну дату
        """
        Поточна дата встановлюється самим екземпляром класу при ініціалізації.
        Передавати її під час ініціалізації як keyword-аргумент не потрібно.
        """
        self.current_date = datetime.date.today().strftime('%Y%m%d')
        
    def set_request_url(self):
        # Модифікація УРЛ-а для запиту
        """
        Оскільки request_url представляє собою рядок для здійснення операцій
        форматування, то ми вносимо  в урл необхідні елементи:
        мнемо-код валюти
        поточну дату в необхідному форматі
        """
        self.request_url = \
            self.request_url.format(self.current_valcode, self.current_date)
