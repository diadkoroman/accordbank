# -*- coding: utf-8 -*-

"""
ТЕСТИ ДЛЯ ПЕРЕВІРКИ РОБОТИ РІЗНОМАНІТНИХ ОБРОБНИКІВ,
СТВОРЕНИХ НА БОЦІ ПРОЕКТУ
"""

import unittest

from accordbank.assets.handlers import nbu

##############################################################################
"""
Обробники даних, що отримуються з сайту НБУ
"""

class NBUDataHandlerTests(unittest.TestCase):
    """
    Тести для NBUDataHandler
    """
    def test_datahandler_creation(self):
        """
        Створення екземпляра
        """
        dh = nbu.NBUDataHandler()
        print(dh.request_url)
        
    def test_get_data(self):
        dh = nbu.NBUDataHandler()
        print(dh.get_data())
    
class NBUSingleCurrencyDataHandler(unittest.TestCase):
    """
    Тести для NBUSingleCurrencyDataHandler
    """
    def test_datahandler_creation(self):
        """
        Створення екземпляра
        """
        dh = nbu.NBUSingleCurrencyDataHandler(current_valcode='zxc')
        print(dh.request_url)
        
    def test_get_data(self):
        dh = nbu.NBUSingleCurrencyDataHandler(current_valcode='usd')
        print(dh.get_data())
##############################################################################
